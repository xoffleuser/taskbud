package com.inkling.facebook;

import org.json.JSONException;
import org.json.JSONObject;

class JsonUtil {


	static String getValue(JSONObject jsonObject, String key) {
		try {
			return jsonObject.getString(key);
		} catch (JSONException e) {
			return null;
		}
	}

	static JSONObject getJsonObject(JSONObject jsonObject, String key) {
		try {
			return jsonObject.getJSONObject(key);
		} catch (JSONException e) {
			return null;
		}
	}

	static String toString(String... values) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[ ");
		for (int i = 0; i < values.length; i++) {
			stringBuffer.append(values[i] + " ");
		}
		stringBuffer.append(" ]");
		return stringBuffer.toString();

	}

	public static void putValue(JSONObject jsonObject, String key, String error) {
		try {
			jsonObject.put(key, error);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static JSONObject createJson(String key, String value) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(key, value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

}
