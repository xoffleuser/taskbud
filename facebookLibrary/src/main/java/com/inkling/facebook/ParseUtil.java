package com.inkling.facebook;

import org.json.JSONException;
import org.json.JSONObject;

import com.inkling.fbdata.ApplicationNode;
import com.inkling.fbdata.FbRequest;
import com.inkling.fbdata.RequestType;
import com.inkling.fbdata.ScoreNode;
import com.inkling.fbdata.UserNode;
import com.inkling.fbdata.UserRequest;

class ParseUtil {

	private static final String	KEY_NAME		= "name";
	private static final String	KEY_ID			= "id";
	private static final String	KEY_NAMESPACE	= "namespace";
	private static final String	KEY_SCORE		= "score";
	private static final String	KEY_USER		= "user";

	private static final String	KEY_MESSAGE		= "message";
	private static final String	KEY_RECEIVER	= "to";
	private static final String	KEY_APPLICATION	= "application";
	private static final String	KEY_SENDER		= "from";
	private static final String	KEY_DATA		= "data";
	private static final String	KEY_TYPE		= "type";

	public static UserNode parseUser(JSONObject jsonObject) {
		String userId = JsonUtil.getValue(jsonObject, KEY_ID);
		String userName = JsonUtil.getValue(jsonObject, KEY_NAME);
		return new UserNode(userId, userName);
	}

	public static ApplicationNode parseApplicationNode(JSONObject jsonObject) {
		String appId = JsonUtil.getValue(jsonObject, KEY_ID);
		String appName = JsonUtil.getValue(jsonObject, KEY_NAME);
		String nameSpace = JsonUtil.getValue(jsonObject, KEY_NAMESPACE);
		return new ApplicationNode(appId, appName, nameSpace);
	}

	public static ScoreNode parseScoreNode(JSONObject jsonObject) {
		UserNode userNode = parseUser(jsonObject.optJSONObject(KEY_USER));
		String score = JsonUtil.getValue(jsonObject, KEY_SCORE);
		return new ScoreNode(userNode, score);
	}

	public static FbRequest parseFbRequest(JSONObject jsonObject) {
		String message = JsonUtil.getValue(jsonObject, KEY_MESSAGE);
		String data = JsonUtil.getValue(jsonObject, KEY_DATA);
		// --- Read sender
		JSONObject json = JsonUtil.getJsonObject(jsonObject, KEY_SENDER);
		UserNode sender = json == null ? null : parseUser(json);
		// --- reader receiver
		json = JsonUtil.getJsonObject(jsonObject, KEY_RECEIVER);
		UserNode receiver = json == null ? null : parseUser(json);

		json = JsonUtil.getJsonObject(jsonObject, KEY_APPLICATION);
		ApplicationNode application = json == null ? null : parseApplicationNode(json);

		String requestId = parseId(JsonUtil.getValue(jsonObject, KEY_ID));
		return new FbRequest(requestId, message, data, sender, receiver, application);
	}

	private static String parseId(String value) {
		if (value == null) {
			return null;
		} else {
			return value.split("_")[0];
		}
	}

	public static UserRequest parseUserRequest(JSONObject jsonObject) {
		FbRequest fbRequest = parseFbRequest(jsonObject);
		String data = fbRequest.getData();
		com.inkling.fbdata.RequestType requestType = null;
		if (data == null) {
			requestType = RequestType.INVITATION;
			return new UserRequest(fbRequest.getRequestId(), fbRequest.getMessage(), requestType, data, fbRequest.getSender(), fbRequest.getReceiver(), fbRequest.getApplication());
		}
		try {
			JSONObject requestObject = new JSONObject(data);
			String type = requestObject.getString(KEY_TYPE);
			if (RequestType.ASK_FOR.isEqual(type)) {
				requestType = RequestType.ASK_FOR;
			} else {
				requestType = RequestType.SEND;
			}
			data = requestObject.getString(KEY_DATA);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return new UserRequest(fbRequest.getRequestId(), fbRequest.getMessage(), requestType, data, fbRequest.getSender(), fbRequest.getReceiver(), fbRequest.getApplication());
	}

	public static String createRequestData(RequestType requestType, String data) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(KEY_TYPE, requestType);
			jsonObject.put(KEY_DATA, data);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
}
