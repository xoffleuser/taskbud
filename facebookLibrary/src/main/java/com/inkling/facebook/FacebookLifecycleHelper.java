package com.inkling.facebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.inkling.facebook.FacebookHelper.PendingAction;
import com.inkling.fbdata.RequestType;

public class FacebookLifecycleHelper implements StatusCallback, LogInCreator {

	protected static final String	REQUEST_ID				= "requestid";
	protected static final String	PAGE_NAME				= "pageName";
	protected static final String	MESSAGE					= "message";
	protected static final String	APP_ID					= "APP_ID";
	protected static final String	USER_ID					= "userid";

	private static final String		NAME					= "name";
	private static final String		IMG_URL					= "picture";
	private static final String		LINK					= "link";
	private static final String		CAPTION					= "caption";
	private static final String		DESCRIPTION				= "description";
	private static final String		SCORE					= "score";
	private static final String		Title					= "title";
	private static final String		FILTER					= "filters";
	private static final String		USER_APP				= "app_users";
	private static final String		USER_NON_APP			= "app_non_users";
	private static final String		DATA					= "data";
	private static final String		DEBUG_TAG				= "facebook";
	private static final String		NO_REQUEST_TO_CONSUME	= "No Request to Consume";

	private final UiLifecycleHelper	uiLifecycleHelper;
	private final LoginButton		loginButton;
	private final Activity			activity;
	private final FacebookHelper	facebookHelper;

	public FacebookLifecycleHelper(Activity activity) {
		this(activity, false);
	}

	public FacebookLifecycleHelper(Activity activity, boolean debug) {
		this.activity = activity;
		uiLifecycleHelper = new UiLifecycleHelper(activity, this);
		facebookHelper = new FacebookHelper(activity, this, debug);
		loginButton = new LoginButton(activity);
	}

	public void onCreate(Bundle savedInstanceState) {
		if (isUiLifeCycleHelperValid()) {
			uiLifecycleHelper.onCreate(savedInstanceState);
		}
	}

	public void onSaveInstanceState(Bundle outState) {
		if (isUiLifeCycleHelperValid()) {
			uiLifecycleHelper.onSaveInstanceState(outState);
		}
	}

	public void onResume() {
		if (isUiLifeCycleHelperValid()) {
			uiLifecycleHelper.onResume();
		}
	}

	public void onStop() {
		if (isUiLifeCycleHelperValid()) {
			uiLifecycleHelper.onStop();
		}
	}

	public void onPause() {
		if (isUiLifeCycleHelperValid()) {
			uiLifecycleHelper.onPause();
		}
	}

	public void onDestroy() {
		if (isUiLifeCycleHelperValid()) {
			uiLifecycleHelper.onDestroy();
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (isUiLifeCycleHelperValid()) {
			uiLifecycleHelper.onActivityResult(requestCode, resultCode, data);
			facebookHelper.onActivityResult(resultCode == Activity.RESULT_CANCELED, resultCode == Activity.RESULT_OK);
		}
	}

	private boolean isUiLifeCycleHelperValid() {
		return uiLifecycleHelper != null;
	}

	@Override
	public void call(Session session, SessionState state, Exception exception) {
		if (facebookHelper != null) {
			if (session != null && session.isOpened() && state == SessionState.OPENING) {
                Util.showToast(activity, "Success... ");
//				facebookHelper.handlePost();
//                logIn();
			}
		}
	}

	public void logIn() {
		if (!isUserLoggedIn()) {
            Util.showToast(activity, "Logginn... ");
			loginButton.performClick();
		}
	}

	public static boolean isUserLoggedIn() {
		Session session = Session.getActiveSession();
		if (session == null) { return false; }
		return session.isOpened();
	}

	public void postStatus(String message, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(MESSAGE, message);
		facebookHelper.handlePost(PendingAction.POST_STATUS, bundle, listener);
	}

	public void postFeed(String name, String imgUrl, String url, String caption, String description, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(NAME, name);
		bundle.putString(IMG_URL, imgUrl);
		bundle.putString(LINK, url);
		bundle.putString(CAPTION, caption);
		bundle.putString(DESCRIPTION, description);
		facebookHelper.handlePost(PendingAction.POST_FEED, bundle, listener);
	}

	public void postOnPage(String name, String msg, String imgUrl, String url, String caption, String description, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(NAME, name);
		bundle.putString(IMG_URL, imgUrl);
		bundle.putString(LINK, url);
		bundle.putString(CAPTION, caption);
		bundle.putString(DESCRIPTION, description);
		bundle.putString(MESSAGE, msg);
		facebookHelper.handlePost(PendingAction.POST_ON_PAGE, bundle, listener);
	}

	public void saveScore(int score, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(SCORE, "" + score);
		facebookHelper.handlePost(PendingAction.SAVE_SCORE, bundle, listener);
	}

	public void getMyScore(FacebookTaskCompleteListener listener) {
		facebookHelper.handlePost(PendingAction.GET_MY_SCORE, null, listener);
	}

	public void getAllScore(String appId, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(APP_ID, appId);
		facebookHelper.handlePost(PendingAction.GET_APP_SCORES, bundle, listener);
	}

	public void inviteFriend(String title, String msg, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(Title, title);
		bundle.putString(MESSAGE, msg);
		bundle.putString(DATA, ParseUtil.createRequestData(RequestType.INVITATION, msg));
		bundle.putString(FILTER, USER_NON_APP);
		facebookHelper.handlePost(PendingAction.INVITE_FRIEND, bundle, listener);
	}

	/**
	 * Read all the request from the facebook(include invitation, request and askfor).
	 * 
	 * @param listener
	 */
	public void readRequests(FacebookTaskCompleteListener listener) {
		facebookHelper.handlePost(PendingAction.READ_REQUEST, null, listener);
	}

	/**
	 * Delete the request from the facebook user's request bucket.(NEED to do that if want to consume else it keep on coming).
	 * 
	 * @param reqId
	 * @param listener
	 */
	public void consumeRequest(String reqId, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(REQUEST_ID, reqId);
		facebookHelper.handlePost(PendingAction.DELETE_REQUEST, bundle, listener);
	}

	private void sendRequestToUser(RequestType requestType, String title, String msg, String data, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(Title, title);
		bundle.putString(MESSAGE, msg);
		bundle.putString(FILTER, USER_APP);
		bundle.putString(DATA, ParseUtil.createRequestData(requestType, data));
		facebookHelper.handlePost(requestType == RequestType.SEND ? PendingAction.SEND_REQUEST : PendingAction.SEND_HELP, bundle, listener);
	}

	/**
	 * Send a REQUEST to a user.
	 * 
	 * @param title
	 * @param msg
	 * @param data
	 * @param listener
	 */
	public void sendRequest(String title, String msg, String data, FacebookTaskCompleteListener listener) {
		sendRequestToUser(RequestType.SEND, title, msg, data, listener);
	}

	public void sendRequestToUser(String userId, String title, String msg, String data, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString("to", userId);
		bundle.putString(Title, title);
		bundle.putString(MESSAGE, msg);
		bundle.putString(FILTER, USER_APP);
		bundle.putString(DATA, ParseUtil.createRequestData(RequestType.SEND, data));
		facebookHelper.handlePost(PendingAction.SEND_REQUEST_TO_USER, bundle, listener);
	}

	/**
	 * Sends a ASK FOR REQUEST to user.
	 * 
	 * @param title
	 * @param msg
	 * @param data
	 * @param listener
	 */
	public void sendAskFor(String title, String msg, String data, FacebookTaskCompleteListener listener) {
		sendRequestToUser(RequestType.ASK_FOR, title, msg, data, listener);
	}

	public void replySendRequest(String userId, String title, String msg, String data, FacebookTaskCompleteListener listener) {
		sendRequestToUser(userId, RequestType.REPLY_SEND, title, msg, data, listener);
	}

	public void replyAskfor(String userId, String title, String msg, String data, FacebookTaskCompleteListener listener) {
		sendRequestToUser(userId, RequestType.REPLY_ASK_FOR, title, msg, data, listener);
	}

	private void sendRequestToUser(String userId, RequestType requestType, String title, String msg, String data, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(Title, title);
		bundle.putString(MESSAGE, msg);
		bundle.putString(FILTER, USER_APP);
		bundle.putString(DATA, ParseUtil.createRequestData(requestType, data));
		bundle.putString("to", userId);
		facebookHelper.handlePost(requestType == RequestType.REPLY_SEND ? PendingAction.SEND_REPLY_REQUEST : PendingAction.SEND_REPLY_ASKFOR, bundle, listener);
	}

	public void consumeAllRequest(String[] ids, FacebookTaskCompleteListener listener) {
		if (ids.length == 0) {
			Log.e(DEBUG_TAG, "Can't delete null request with size 0");
			Util.showToast(activity, NO_REQUEST_TO_CONSUME);
			return;
		}
		Bundle bundle = new Bundle();
		bundle.putStringArray(REQUEST_ID, ids);
		facebookHelper.handlePost(PendingAction.DELETE_ALL_REQUEST, bundle, listener);
	}

	public UiLifecycleHelper getUiHelper() {
		return uiLifecycleHelper;
	}

	private void postTaggedStatus(String string, String[] friendId, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString("tags", getFriendTagged(friendId));
		bundle.putString("place", "155021662189");
		facebookHelper.handlePost(PendingAction.POST_FEED, bundle, listener);
	}

	private void postTaggedFeed(String[] friendId, String name, String msg, String imgUrl, String url, String caption, String description, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString(NAME, name);
		bundle.putString(IMG_URL, imgUrl);
		bundle.putString(LINK, url);
		bundle.putString(CAPTION, caption);
		bundle.putString(DESCRIPTION, description);
		bundle.putString(MESSAGE, msg);
		bundle.putString("tags", getFriendTagged(friendId));
		bundle.putString("place", "155021662189");
		facebookHelper.handlePost(PendingAction.POST_FEED, bundle, listener);
	}

	private String getFriendTagged(String[] userId) {
		StringBuffer tags = new StringBuffer();
		for (int i = 0; i < userId.length; i++) {
			tags.append(userId[i]);
			if (i != userId.length - 1) {
				tags.append(",");
			}
		}
		return tags.toString();
	}

	private void likePage(FacebookTaskCompleteListener listener) {
		facebookHelper.handlePost(PendingAction.LIKE_PAGE, null, listener);
	}

	public void getUserInfo(FacebookTaskCompleteListener listener) {
        logIn();
//		Bundle bundle = new Bundle();
//		facebookHelper.handlePost(PendingAction.GET_USER_INFO, bundle, listener);
	}

	public void postAchivement(String url, FacebookTaskCompleteListener listener) {
		Bundle bundle = new Bundle();
		bundle.putString("achievement", url);
		facebookHelper.handlePost(PendingAction.POST_ACHIVEMENT, bundle, listener);
	}

}
