package com.inkling.facebook;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.facebook.FacebookRequestError;
import com.facebook.Response;
import com.inkling.fbdata.FbRequest;
import com.inkling.fbdata.ScoreNode;
import com.inkling.fbdata.UserNode;
import com.inkling.fbdata.UserRequest;

public class ResponseUtil {

	private static final String	KEY_DATA	= "data";
	private static final String	KEY_ERROR	= "error";

	public static String getError(Response response) {
		return response.getError() == null ? null : response.getError().toString();
	}

	public static String getError(JSONObject jsonObject) {
		return JsonUtil.getValue(jsonObject, KEY_ERROR);
	}

	public static List<ScoreNode> parseScore(Response response) {
		FacebookRequestError error = response.getError();
		if (error != null) { return null; }
		JSONObject json = response.getGraphObject().getInnerJSONObject();
		List<ScoreNode> listScoreNodes = parseScore(json);
		return listScoreNodes;
	}

	private static List<ScoreNode> parseScore(JSONObject json) {
		try {
			List<ScoreNode> list = new ArrayList<ScoreNode>();
			JSONArray array = json.getJSONArray(KEY_DATA);
			for (int i = 0; i < array.length(); i++) {
				JSONObject object = array.getJSONObject(i);
				ScoreNode scoreNode = ParseUtil.parseScoreNode(object);
				list.add(scoreNode);
			}
			return list;
		} catch (JSONException e) {
			Log.d("Json", "Exception occured " + e);
		}
		return null;
	}

	public static List<UserRequest> getUserRequest(Response response) {
		FacebookRequestError error = response.getError();
		if (error != null) { return null; }
		JSONObject json = response.getGraphObject().getInnerJSONObject();
		List<UserRequest> list = getUserRequest(json);
		return list;
	}

	private static List<UserRequest> getUserRequest(JSONObject json) {
		try {
			List<UserRequest> list = new ArrayList<UserRequest>();
			JSONArray array = json.getJSONArray(KEY_DATA);
			for (int i = 0; i < array.length(); i++) {
				JSONObject object = array.getJSONObject(i);
				UserRequest request = ParseUtil.parseUserRequest(object);
				list.add(request);
			}
			return list;
		} catch (JSONException e) {
			Log.d("Json", "Exception occured " + e);
		}
		return null;
	}

	public static List<FbRequest> parseRequest(Response response) {
		FacebookRequestError error = response.getError();
		if (error != null) { return null; }
		JSONObject json = response.getGraphObject().getInnerJSONObject();
		List<FbRequest> list = parseRequest(json);
		return list;
	}

	private static List<FbRequest> parseRequest(JSONObject json) {
		try {
			List<FbRequest> list = new ArrayList<FbRequest>();
			JSONArray array = json.getJSONArray(KEY_DATA);
			for (int i = 0; i < array.length(); i++) {
				JSONObject object = array.getJSONObject(i);
				FbRequest request = ParseUtil.parseFbRequest(object);
				list.add(request);
			}
			return list;
		} catch (JSONException e) {
			Log.d("Json", "Exception occured " + e);
		}
		return null;
	}

	/**
	 * Call getError() to check whether there is a error or not.<br>
	 * <b>Work for sendInvitation, sendRequest, sendHelp </b>
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static List<String> getFriendIdList(JSONObject jsonObject) {
		@SuppressWarnings("rawtypes")
		Iterator keys = jsonObject.keys();
		ArrayList<String> list = new ArrayList<String>();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (key.startsWith("to[")) {
				list.add(JsonUtil.getValue(jsonObject, key));
			}
		}
		return list;
	}

	public static List<UserNode> getTaggableFriend(Response response) {
		FacebookRequestError error = response.getError();
		if (error != null) { return null; }
		JSONObject json = response.getGraphObject().getInnerJSONObject();
		return parseUsers(json);
	}

	private static List<UserNode> parseUsers(JSONObject json) {
		List<UserNode> userNodes = new ArrayList<UserNode>();
		try {
			JSONArray jsonArray = json.getJSONArray("data");
			for (int i = 0; i < jsonArray.length(); i++) {
				userNodes.add(ParseUtil.parseUser(jsonArray.getJSONObject(i)));
			}
		} catch (Exception e) {
		}
		return userNodes;
	}

	public static UserNode parseUser(JSONObject jsonObject) {
		return ParseUtil.parseUser(jsonObject);
	}

}
