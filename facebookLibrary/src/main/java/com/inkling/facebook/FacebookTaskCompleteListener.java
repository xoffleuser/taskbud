package com.inkling.facebook;

import org.json.JSONObject;

import com.facebook.Response;

public interface FacebookTaskCompleteListener {

	/** Thinking about json string
	 * @param taskString */
	void onComplete(Response response, JSONObject jsonObject);

}
