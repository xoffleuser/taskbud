package com.inkling.facebook;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookException;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.Request.GraphUserCallback;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

/**
 * Implementation of different Facebook calls.
 * 
 */
class FacebookHelper implements Callback {

	private static enum Action {
		LOG_IN, ASKING_PERMISSIONS, NONE_OR_POSTING
	}

	private static final String	PageUrl					= "http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fteaminkling&width=200&height=590&colorscheme=dark&show_faces=true&header=true&stream=true&show_border=true&appId=282500488587882";
	private static final String	DEBUG_TAG				= "facebook";

	/** Page-id for team inkling. */
	private static final String	PAGE_ID					= "500355613408750";
	private static final String	ME_FEED					= "/me/feed";

	private static final String	KEY_PENDING_ACTION		= "KeyPendingAction";
	private static final String	KEY_BUNDLE_DATE			= "KeyBundleData";

	/** Required for almost-all operations. */
	private static final String	PERMISSION_PUBLISH		= "publish_actions";

	private static final String	PREMISSION_USER_FRIENDS	= "user_friends";

	private static final String	JSON_KEY_ERROR			= "error";
	private static final String	JSON_KEY_NAME			= "name";
	private static final String	JSON_KEY_ID				= "id";

	/**
	 * Different action that are possible via FacebookHelper
	 * 
	 * @author Gaurav
	 */
	protected enum PendingAction {
		NONE, POST_STATUS, POST_FEED, POST_ON_PAGE, SAVE_SCORE, GET_MY_SCORE, GET_APP_SCORES, INVITE_FRIEND, READ_REQUEST, DELETE_REQUEST, SEND_REQUEST, SEND_REQUEST_TO_USER, SEND_HELP, SEND_REPLY_REQUEST, SEND_REPLY_ASKFOR, DELETE_ALL_REQUEST, LIKE_PAGE, GET_USER_INFO, POST_ACHIVEMENT
	}

	/** Denote the currentAction going on. */
	private PendingAction						pendingAction;

	/** Values required to do pending action */
	private Bundle								bundle;

	/** Context is just for toasts. */
	private Context								context;
	/** if active then post all start-action and post-result under facebook tag. */
	private boolean								debug;

	/** Static listener so don't call it parallel. */
	private static FacebookTaskCompleteListener	listener;

	/** Once login has been attempted */
	@SuppressWarnings("unused")
	private boolean								triedLoginOnce	= false;

	@SuppressWarnings("unused")
	private Action								action			= Action.NONE_OR_POSTING;

	private LogInCreator						logIn;

	public FacebookHelper(Context context, LogInCreator logIn, boolean debug) {
		this.context = context;
		this.logIn = logIn;
		this.debug = debug;
		pendingAction = PendingAction.NONE;
		bundle = new Bundle();
	}

	/**
	 * Restore the pending action & data for action if the activity is recreated. Initializing the pending action.
	 * 
	 * @param savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			pendingAction = PendingAction.valueOf(savedInstanceState.getString(KEY_PENDING_ACTION));
			bundle = savedInstanceState.getBundle(KEY_BUNDLE_DATE);
			handlePost(pendingAction, bundle, listener);
		}
	}

	/**
	 * Save the pending action and data required for it.
	 * 
	 * @param outState
	 */
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(KEY_PENDING_ACTION, pendingAction.toString());
		outState.putBundle(KEY_BUNDLE_DATE, bundle);
	}

	/**
	 * Check all the condition i.e
	 * 
	 * <pre>
	 * 1. If no action is pending.
	 * 2. is Session valid
	 * 3. Has permission for particular action.
	 * 4. AtLas perform action.
	 * </pre>
	 * 
	 * @param pendingAction
	 * @param bundle
	 * @param listener
	 */
	public void handlePost(PendingAction pendingAction, Bundle bundle, FacebookTaskCompleteListener listener) {
		// started new request
		this.triedLoginOnce = false;
		this.pendingAction = pendingAction;
		this.bundle = bundle;
		FacebookHelper.listener = listener;
		handlePost();
	}

	void handlePost() {
		if (pendingAction == null || pendingAction == PendingAction.NONE) {
			debug("Err: No Action pending still calling.");
			return;
		} else if (!isSessionValid()) {
			if (listener != null) {
				complete(null, JsonUtil.createJson(JSON_KEY_ERROR, "In Valid Session."));
			}
			debug("Err: calling but not valid session.");
			triedLoginOnce = true;
			logIn.logIn();
			return;
		} else if (!hasPermission(pendingAction)) {
			requestForPermission(pendingAction);
		} else {
			startAction();
			performAction();
		}
	}

	/**
	 * <b>Note: Should call isValidSessionBefore</b><br>
	 * Check for particular permission for a particular task/action.
	 * 
	 * @param pendingAction
	 */
	private void requestForPermission(PendingAction pendingAction) {
		List<String> permission = getRequiredPermissions();
		debug(" Did n't permisssion: Request 4 it.");
		NewPermissionsRequest newPermissionsRequest = new NewPermissionsRequest((Activity) context, permission);
		newPermissionsRequest.setCallback(new StatusCallback() {

			// TODO req permission callback
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (debug) {
					if (exception != null) {
						debug(exception.toString());
						Util.showToast(context, exception.toString());
					} else {
						debug("no exception : " + session.isPermissionGranted(PERMISSION_PUBLISH) + "  " + state);
						Util.showToast(context, "no exception : " + session.isPermissionGranted(PERMISSION_PUBLISH) + "  " + state);
					}
				}
			}
		});
		Session.getActiveSession().requestNewPublishPermissions(newPermissionsRequest);
	}

	/** Check pending action and perform particular task. */
	private void performAction() {
		switch (pendingAction) {
		case POST_STATUS:
			postStatus(bundle);
			break;
		case POST_FEED:
			postFeed(bundle);
			break;
		case POST_ON_PAGE:
			postOnPage(bundle);
			break;
		case SAVE_SCORE:
			saveScore(bundle);
			break;
		case GET_MY_SCORE:
			getMyScore();
			break;
		case GET_APP_SCORES:
			getAppScores();
			break;
		case INVITE_FRIEND:
			inviteFriends();
			break;
		case READ_REQUEST:
			readRequest();
			break;
		case DELETE_REQUEST:
			deleteRequest();
			break;
		case SEND_REQUEST:
			sendRequest();
			break;
		case SEND_REQUEST_TO_USER:
			sendRequestToUser();
			break;
		case SEND_HELP:
			sendHelp();
			break;
		case SEND_REPLY_REQUEST:
			sendReplyRequest();
			break;

		case SEND_REPLY_ASKFOR:
			sendReplyAskfor();
			break;
		case DELETE_ALL_REQUEST:
			deleteAllRequest();
			break;
		case LIKE_PAGE:
			likePage();
			break;
		case GET_USER_INFO:
			getUserInfo();
			break;
		case POST_ACHIVEMENT:
			postAchivement();
			break;

		case NONE:
			break;
		}
		setPendingAction(PendingAction.NONE);
	}

	private boolean isSessionValid() {
		if (Session.getActiveSession() == null) { return false; }
		return Session.getActiveSession().isOpened();
	}

	/**
	 * @param pendingAction
	 * @return true if session has necessary permission to do this task.
	 */
	private boolean hasPermission(PendingAction pendingAction) {
		List<String> permission = getRequiredPermissions();
		for (String string : permission) {
			debug("Permission req: " + string);
		}
		if (permission == null || permission.size() == 0) { return true; }
		for (int i = 0; i < permission.size(); i++) {
			if (!Session.getActiveSession().isPermissionGranted(permission.get(i))) { return false; }
		}
		return true;
	}

	private void setPendingAction(PendingAction pendingAction) {
		this.pendingAction = pendingAction;
	}

	/**
	 * Just required a string message as a status provide within bundle.<br>
	 * Can run on any thread but LOGIN should be provided if user is not logged in the call it on ui thread. <br>
	 * Rest Url: \/me/feed
	 * 
	 * @param bundle
	 *            is not required.
	 */
	private void postStatus(Bundle bundle) {
		Request request = Request.newStatusUpdateRequest(Session.getActiveSession(), bundle.getString(FacebookLifecycleHelper.MESSAGE), this);
		request.executeAsync();
	}

	/**
	 * Bundle with name, msg, image-url, link, caption, description.<br>
	 * Can run on any thread but LOGIN should be provided if user is not logged in the call it on ui thread.
	 * 
	 * @param bundle
	 *            data to be posted as feed.
	 */
	private void postFeed(Bundle bundle) {
		Request request = new Request(Session.getActiveSession(), ME_FEED, bundle, HttpMethod.POST, this);
		request.executeAsync();
	}

	private void postOnPage(Bundle bundle) {
		Request postOnPageRequest = new Request(Session.getActiveSession(), PAGE_ID + "/feed/", bundle, HttpMethod.POST, this);
		postOnPageRequest.executeAsync();
	}

	private void saveScore(final Bundle bundle) {
		Request saveScore = new Request(Session.getActiveSession(), "/me/scores", bundle, HttpMethod.POST, this);
		saveScore.executeAsync();
	}

	private void getUserInfo() {
		Request newMeRequest = Request.newMeRequest(Session.getActiveSession(), new GraphUserCallback() {

			@Override
			public void onCompleted(GraphUser user, Response response) {
				// Error found
				debug("User Info=> " + user + "   && Response=>" + response);
				if (response == null || response.getError() != null || user == null) {
					complete(response, null);
					return;
				}

				JSONObject json = JsonUtil.createJson(JSON_KEY_ERROR, response.getError() == null ? null : response.getError().toString());
				JsonUtil.putValue(json, JSON_KEY_NAME, user.getName());
				JsonUtil.putValue(json, JSON_KEY_ID, user.getId());
				complete(response, json);
			}
		});
		newMeRequest.executeAsync();
	}

	private void getMyScore() {
		Request request = new Request(Session.getActiveSession(), "/me/scores", null, HttpMethod.GET, this);
		request.executeAsync();
	}

	private void getAppScores() {
		Request request = new Request(Session.getActiveSession(), bundle.getString(FacebookLifecycleHelper.APP_ID) + "/scores", null, HttpMethod.GET, this);
		request.executeAsync();
	}

	private void inviteFriends() {
		showSendPopup();
	}

	private void readRequest() {
		Request request = new Request(Session.getActiveSession(), "/me/apprequests", null, HttpMethod.GET, this);
		request.executeAsync();
	}

	private void deleteRequest() {
		Request deleteRequest = new Request(Session.getActiveSession(), "/" + bundle.getString(FacebookLifecycleHelper.REQUEST_ID), null, HttpMethod.DELETE, this);
		deleteRequest.executeAsync();
	}

	private void sendRequest() {
		showSendPopup();
	}

	private void sendRequestToUser() {
		String str = "/" + bundle.getString(FacebookLifecycleHelper.USER_ID) + "/apprequests";
		Log.i(DEBUG_TAG, str);
		showSendPopup();
	}

	private void sendHelp() {
		showSendPopup();
	}

	private void sendReplyRequest() {
		showSendPopup();
	}

	private void sendReplyAskfor() {
		showSendPopup();
	}

	private void deleteAllRequest() {
		String[] ids = bundle.getStringArray(FacebookLifecycleHelper.REQUEST_ID);
		Request[] deleteRequest = new Request[ids.length];
		for (int i = 0; i < ids.length; i++) {
			deleteRequest[i] = new Request(Session.getActiveSession(), "/" + ids[i], null, HttpMethod.DELETE, null);
		}
		RequestBatch requestBatch = new RequestBatch(deleteRequest);
		// TODO call back
		requestBatch.addCallback(new RequestBatch.Callback() {

			@Override
			public void onBatchCompleted(RequestBatch batch) {
				JSONObject jsonObject = new JSONObject();
				JsonUtil.putValue(jsonObject, JSON_KEY_ERROR, null);
				complete(null, jsonObject);
			}
		});
		requestBatch.executeAsync();
	}

	/** Call it on Ui Thread TODO like */
	public void likePage() {
		WebDialog webDialog = new WebDialog(context, PageUrl);
		webDialog.show();
	}

	private void showSendPopup() {
		WebDialog requestsDialog = (new WebDialog.RequestsDialogBuilder(context, Session.getActiveSession(), bundle)).setOnCompleteListener(new OnCompleteListener() {

			@Override
			public void onComplete(Bundle values, FacebookException error) {
				try {
					final JSONObject jsonObject = new JSONObject();
					String errorMsg = error == null ? null : error.toString();
					jsonObject.put(JSON_KEY_ERROR, errorMsg);
					final String requestId = values.getString("request");
					if (requestId != null) {
						jsonObject.put("completed", true);
					} else {
						jsonObject.put("completed", false);
					}
					if (values != null) {
						Iterator<String> iterator = values.keySet().iterator();
						while (iterator.hasNext()) {
							String key = iterator.next();
							jsonObject.put(key, values.get(key));
						}
					}
					complete(null, jsonObject);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).build();
		requestsDialog.show();
	}

	private void postAchivement() {
		Request request = new Request(Session.getActiveSession(), "/me/achievements", bundle, HttpMethod.POST, this);
		request.executeAsync();
	}

	public void onActivityResult(boolean isRequestCancelled, boolean isSuccess) {
		debug("Should cancel request : " + (isRequestCancelled) + "  , Required isSuccess:      " + isSuccess);
		if (isRequestCancelled || !isSuccess) {
			if (listener != null) {
				complete(null, JsonUtil.createJson(JSON_KEY_ERROR, "Request cancelled"));
				pendingAction = PendingAction.NONE;
			}
			return;
		}
		debug("Cont.. again");
//		handlePost(pendingAction, bundle, listener);
	}

	private List<String> getRequiredPermissions() {
		List<String> permission = new ArrayList<String>();
		switch (pendingAction) {

		case GET_USER_INFO:
		case INVITE_FRIEND:
			// No permission needed;
//			break;

		// me/feed
		case POST_STATUS:
		case POST_FEED:
			permission.add(PERMISSION_PUBLISH);
//			break;

		// Score Api
		// link for more :https://developers.facebook.com/docs/games/scores/

		case SAVE_SCORE:
			// create or update requires publish permission
			permission.add(PERMISSION_PUBLISH);
//			break;
		case GET_MY_SCORE:
			// No permission required to read score for this app for this player.
//			break;
		case GET_APP_SCORES:
			// user_friend permission required for this. [This is always granted 1 of 3 basic permission]
//			permission.add(PREMISSION_USER_FRIENDS);
//			break;
		// Delete also requires publish_actions permission

		// Check Permission
		case POST_ON_PAGE:
		case POST_ACHIVEMENT:
			permission.add(PERMISSION_PUBLISH);
//			break;

		// Check Permission
		case READ_REQUEST:
		case SEND_REQUEST:
		case SEND_REQUEST_TO_USER:
		case SEND_REPLY_ASKFOR:
		case SEND_REPLY_REQUEST:
		case SEND_HELP:
			// Send request look for permissions
		case DELETE_REQUEST:
		case DELETE_ALL_REQUEST:
			// look for delete permissions
			permission.add(PERMISSION_PUBLISH);
		case LIKE_PAGE:
		case NONE:
			break;

		}
		return permission;
	}

	private void debug(String msg) {
		if (debug) {
			Util.showToast(context, "FbHelper: " + msg);
			Log.i(DEBUG_TAG, "FbHelper: " + msg);
		}
	}

	/** Show toast and log message about action started and info provided. */
	private void startAction() {
		if (debug) {
			Util.showToast(context, "Start Action: " + pendingAction);
			Log.i(DEBUG_TAG, "Start Action:  " + pendingAction);
			Log.i(DEBUG_TAG, "Start Data:  " + bundle);
		}
	}

	/**
	 * Show toast and log message about what action end and response returned.
	 * 
	 * @param response
	 */
	private void endAction(Response response) {
		if (debug) {
			Util.showToast(context, "On Complete : " + response);
			Log.i(DEBUG_TAG, "On Complete : ");
			if (response != null) {
				Log.i(DEBUG_TAG, "Completed data : " + response.toString());
			} else {
				Log.i(DEBUG_TAG, "Completed data : other response obtained");
			}
		}
	}

	/** Call the TODO static listener. @param response */
	private void callListener(Response response, JSONObject jsonString) {
		if (listener != null) {
			listener.onComplete(response, jsonString);
		}
	}

	@Override
	public void onCompleted(Response response) {
		complete(response, null);
	}

	private void complete(Response response, JSONObject jsonObject) {
		endAction(response);
		callListener(response, jsonObject);
	}
}
