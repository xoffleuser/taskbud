-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-sdk
ADDED from AndroidManifest.xml:7:5
MERGED from TaskTracker1:facebookLibrary:unspecified:7:5
MERGED from TaskTracker1:facebookSDK:unspecified:20:5
MERGED from com.android.support:support-v4:22.1.1:20:5
MERGED from com.android.support:cardview-v7:22.1.1:20:5
MERGED from com.android.support:recyclerview-v7:22.1.1:20:5
MERGED from com.android.support:support-v4:22.1.1:20:5
MERGED from com.android.support:appcompat-v7:22.1.1:20:5
MERGED from com.android.support:support-v4:22.1.1:20:5
MERGED from com.android.support:support-v4:22.1.1:20:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:9:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:8:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:11:5
MERGED from TaskTracker1:facebookLibrary:unspecified:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
uses-permission#android.permission.RECEIVE_BOOT_COMPLETED
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:22
uses-permission#com.google.android.c2dm.permission.RECEIVE
ADDED from AndroidManifest.xml:18:5
	android:name
		ADDED from AndroidManifest.xml:18:22
uses-permission#com.xoffle.tasktracker.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:19:5
	android:name
		ADDED from AndroidManifest.xml:19:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:20:5
	android:name
		ADDED from AndroidManifest.xml:20:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:21:5
	android:name
		ADDED from AndroidManifest.xml:21:22
uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:22
permission#com.xoffle.tasktracker.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:24:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:26:9
	android:name
		ADDED from AndroidManifest.xml:25:9
application
ADDED from AndroidManifest.xml:28:5
MERGED from TaskTracker1:facebookLibrary:unspecified:13:5
MERGED from TaskTracker1:facebookSDK:unspecified:24:5
MERGED from com.android.support:support-v4:22.1.1:22:5
MERGED from com.android.support:cardview-v7:22.1.1:22:5
MERGED from com.android.support:recyclerview-v7:22.1.1:22:5
MERGED from com.android.support:support-v4:22.1.1:22:5
MERGED from com.android.support:appcompat-v7:22.1.1:22:5
MERGED from com.android.support:support-v4:22.1.1:22:5
MERGED from com.android.support:support-v4:22.1.1:22:5
	android:label
		ADDED from AndroidManifest.xml:32:9
	android:allowBackup
		ADDED from AndroidManifest.xml:30:9
	android:icon
		ADDED from AndroidManifest.xml:31:9
	android:theme
		ADDED from AndroidManifest.xml:33:9
	android:name
		ADDED from AndroidManifest.xml:29:9
activity#com.xoffle.tasktracker.userauth.UserauthActivity
ADDED from AndroidManifest.xml:34:9
	android:label
		ADDED from AndroidManifest.xml:36:13
	android:theme
		ADDED from AndroidManifest.xml:37:13
	android:name
		ADDED from AndroidManifest.xml:35:13
activity#com.xoffle.tasktracker.splash.SplashActivity
ADDED from AndroidManifest.xml:44:9
	android:theme
		ADDED from AndroidManifest.xml:46:13
	android:name
		ADDED from AndroidManifest.xml:45:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:47:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:48:17
	android:name
		ADDED from AndroidManifest.xml:48:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:50:17
	android:name
		ADDED from AndroidManifest.xml:50:27
activity#com.xoffle.tasktracker.homescreen.MainActivity
ADDED from AndroidManifest.xml:53:9
	android:label
		ADDED from AndroidManifest.xml:55:13
	android:theme
		ADDED from AndroidManifest.xml:56:13
	android:name
		ADDED from AndroidManifest.xml:54:13
activity#com.xoffle.tasktracker.group.creategroup.CreateGroupActivity
ADDED from AndroidManifest.xml:58:9
	android:label
		ADDED from AndroidManifest.xml:60:13
	android:name
		ADDED from AndroidManifest.xml:59:13
activity#com.xoffle.tasktracker.group.creategroup.AddFriendsToGroupActivity
ADDED from AndroidManifest.xml:62:9
	android:label
		ADDED from AndroidManifest.xml:64:13
	android:name
		ADDED from AndroidManifest.xml:63:13
activity#com.xoffle.tasktracker.group.creategroup.SelectFriendsActivity
ADDED from AndroidManifest.xml:66:9
	android:label
		ADDED from AndroidManifest.xml:68:13
	android:name
		ADDED from AndroidManifest.xml:67:13
	android:launchMode
		ADDED from AndroidManifest.xml:69:13
intent-filter#android.intent.action.SEARCH
ADDED from AndroidManifest.xml:70:13
action#android.intent.action.SEARCH
ADDED from AndroidManifest.xml:71:17
	android:name
		ADDED from AndroidManifest.xml:71:25
meta-data#android.app.default_searchable
ADDED from AndroidManifest.xml:74:13
	android:name
		ADDED from AndroidManifest.xml:75:17
	android:value
		ADDED from AndroidManifest.xml:76:17
meta-data#android.app.searchable
ADDED from AndroidManifest.xml:77:13
	android:resource
		ADDED from AndroidManifest.xml:79:17
	android:name
		ADDED from AndroidManifest.xml:78:17
activity#com.facebook.LoginActivity
ADDED from AndroidManifest.xml:81:9
	android:label
		ADDED from AndroidManifest.xml:83:13
	android:theme
		ADDED from AndroidManifest.xml:84:13
	android:name
		ADDED from AndroidManifest.xml:82:13
meta-data#com.facebook.sdk.ApplicationId
ADDED from AndroidManifest.xml:86:9
	android:name
		ADDED from AndroidManifest.xml:87:13
	android:value
		ADDED from AndroidManifest.xml:88:13
service#com.xoffle.tasktracker.homescreen.DataFetchService
ADDED from AndroidManifest.xml:90:9
	android:name
		ADDED from AndroidManifest.xml:90:18
service#com.parse.PushService
ADDED from AndroidManifest.xml:92:9
	android:name
		ADDED from AndroidManifest.xml:92:18
receiver#com.parse.GcmBroadcastReceiver
ADDED from AndroidManifest.xml:94:9
	android:permission
		ADDED from AndroidManifest.xml:96:13
	android:name
		ADDED from AndroidManifest.xml:95:13
intent-filter#com.google.android.c2dm.intent.RECEIVE+com.google.android.c2dm.intent.REGISTRATION+com.xoffle.tasktracker
ADDED from AndroidManifest.xml:97:13
action#com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:98:17
	android:name
		ADDED from AndroidManifest.xml:98:25
action#com.google.android.c2dm.intent.REGISTRATION
ADDED from AndroidManifest.xml:99:17
	android:name
		ADDED from AndroidManifest.xml:99:25
category#com.xoffle.tasktracker
ADDED from AndroidManifest.xml:101:17
	android:name
		ADDED from AndroidManifest.xml:101:27
receiver#com.xoffle.tasktracker.pushnotification.GCMReceiver
ADDED from AndroidManifest.xml:104:9
	android:exported
		ADDED from AndroidManifest.xml:106:13
	android:name
		ADDED from AndroidManifest.xml:105:13
intent-filter#com.parse.push.intent.DELETE+com.parse.push.intent.OPEN+com.parse.push.intent.RECEIVE
ADDED from AndroidManifest.xml:107:13
action#com.parse.push.intent.RECEIVE
ADDED from AndroidManifest.xml:108:17
	android:name
		ADDED from AndroidManifest.xml:108:25
action#com.parse.push.intent.DELETE
ADDED from AndroidManifest.xml:109:17
	android:name
		ADDED from AndroidManifest.xml:109:25
action#com.parse.push.intent.OPEN
ADDED from AndroidManifest.xml:110:17
	android:name
		ADDED from AndroidManifest.xml:110:25
meta-data#com.parse.push.notification_icon
ADDED from AndroidManifest.xml:114:9
	android:resource
		ADDED from AndroidManifest.xml:116:13
	android:name
		ADDED from AndroidManifest.xml:115:13
activity#com.xoffle.tasktracker.setting.SettingsActivity
ADDED from AndroidManifest.xml:118:9
	android:label
		ADDED from AndroidManifest.xml:120:13
	android:name
		ADDED from AndroidManifest.xml:119:13
activity#android.support.v7.widget.TestActivity
ADDED from com.android.support:recyclerview-v7:22.1.1:23:9
	android:label
		ADDED from com.android.support:recyclerview-v7:22.1.1:25:13
	android:name
		ADDED from com.android.support:recyclerview-v7:22.1.1:24:13
