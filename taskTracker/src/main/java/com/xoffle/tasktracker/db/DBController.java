package com.xoffle.tasktracker.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.inkling.leaderboard.AppGroup;
import com.inkling.leaderboard.AppTaskBuds;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.SeenBy;
import com.inkling.leaderboard.Task;
import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.group.GroupMember;
import com.xoffle.tasktracker.group.OnGroupLoadListener;
import com.xoffle.tasktracker.json.JsonUtils;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.task.OnTaskEventListener;
import com.xoffle.tasktracker.util.Debug;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBController {

    private static DBController instance;
    private DBHelper dbHelper;
    private Context context;

    private DBController(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
    }

    public static DBController getInstance(Context context) {
        return instance == null ? instance = new DBController(context) : instance;
    }

    public Cursor loadAllFriends() {
        return dbHelper.getTaskBudList();
    }

    public void loadAllData(OnGroupLoadListener onGroupLoadListener) {

        Cursor cursor = dbHelper.getGroups();
        if (cursor != null && cursor.getCount() != 0) {
            Debug.debug("cursor count all = " + cursor.getCount());
            cursor.moveToFirst();

            int count = 0;
            do {
                Group group = new Group(context, cursor);
                Debug.debug("group = " + (++count) + " = " + group.getName() + ", isMyGroup = " + group.isMyGroup());

                ArrayList<GroupMember> groupMembers = getGroupMembers(group.getGroupId(), group.getAdminId());
                group.setGroupMembers(groupMembers);

                onGroupLoadListener.onGroupLoaded(group);

            } while (cursor.moveToNext());
        }
        // dataLoader.execute(new AsynInfo(GROUP_INFO, null));
    }

    public ArrayList<GroupMember> getGroupMembers(String groupId, String adminId) {
        Cursor cursor = dbHelper.getGroupMembersId(groupId);

        ArrayList<GroupMember> members = new ArrayList<>();
        do {
            if (cursor != null && cursor.getCount() != 0) {
                cursor.moveToFirst();

                do {
                    String tbId = cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID));
//                    Cursor taskBudCursor = dbHelper.getTaskBudCursor(tbId);

                    GroupMember memberBean = CursorFactory.createGroupMemberBean(tbId, context, adminId);

                    members.add(memberBean);
                } while (cursor.moveToNext());
            }

        } while (cursor.moveToNext());

        return members;
    }

    public TaskBudBean getTaskBud(String memberId) {
        return CursorFactory.createTaskBud(context, memberId);
    }

    public void createGroup(AppGroup appGroup) {
//        Debug.debug("DBController createGroup");
//        dbHelper.createGroup(appGroup);
        createMyGroup(appGroup);
    }

    public void createMyGroup(AppGroup appGroup) {
        ContentValues values = new ContentValues();

        values.put(DBConstants.GROUP_ID, appGroup.getObjectId());
        values.put(DBConstants.NAME, appGroup.getGroupName());
        values.put(DBConstants.ADMIN_ID, appGroup.getAdminId());
        values.put(DBConstants.CREATOR_ID, appGroup.getCreatorId());
        values.put(DBConstants.STATUS, appGroup.getGroupStatus());
        values.put(DBConstants.STATUS_UPDATE_ON, appGroup.getStatusUpdateOn());
        values.put(DBConstants.CREATED_AT, appGroup.getCreatedAt().getTime());
        values.put(DBConstants.UPDATED_AT, appGroup.getUpdatedAt().getTime());

        values.put(DBConstants.IMAGE_OBJECT_ID, appGroup.getImageObjectId());

        if (appGroup.isMyGroup()) {
            values.put(DBConstants.IS_MY_GROUP, 1);
        } else {
            values.put(DBConstants.IS_MY_GROUP, 0);
        }
        dbHelper.createGroup(values);
    }

    public void createTask(Task task) {

        Debug.debug("DBController create task");

        ContentValues values = new ContentValues();
        if (task.getObjectId() == null) {

            values.put(DBConstants.TASK_ID, "");
        } else {
            values.put(DBConstants.TASK_ID, task.getObjectId());
        }

        if (task.getAckCnfID() != null) {
            values.put(DBConstants.TASK_ACK_CNF_ID, task.getAckCnfID());
        }

        if (task.getAckTime() != 0) {
            values.put(DBConstants.TASK_ACK_TIME, task.getAckTime());
        }

        if (task.getCnfTime() != 0) {
            values.put(DBConstants.TASK_CNF_TIME, task.getCnfTime());
        }

        if (task.isAckd()) {
            values.put(DBConstants.TASK_IS_ACKNOWLEDGED, task.isAckd() == true ? 1 : 0);
        }

        if (task.isCnfd()) {
            values.put(DBConstants.TASK_IS_CONFIRMED, task.isCnfd() == true ? 1 : 0);
        }

        if (task.isDeleted()) {
            values.put(DBConstants.TASK_IS_DELETED, task.isDeleted() == true ? 1 : 0);
        }

        if (task.getSubject() != null) {
            values.put(DBConstants.TASK_SUBJECT, task.getSubject());
        }

        if (task.getDescription() != null) {
            values.put(DBConstants.TASK_DESCRIPTION, task.getDescription());
        }

        if (task.getRemark() != null) {
            values.put(DBConstants.TASK_REMARK, task.getRemark());
        }

        if (task.getTaskCreaterId() != null) {
            values.put(DBConstants.TASK_CREATOR_ID, task.getTaskCreaterId());
        }

        if (task.getCreatedAt() != null) {
            values.put(DBConstants.CREATED_AT, task.getCreatedAt().getTime());
        }

        if (task.getUpdatedAt() != null) {
            values.put(DBConstants.UPDATED_AT, task.getUpdatedAt().getTime());
        }

        if (task.getExpireDate() != 0) {
            values.put(DBConstants.TASK_EXPIRE_DATE, task.getExpireDate());
        }

        if (task.getGroupObjectId() != null) {
            values.put(DBConstants.GROUP_ID, task.getGroupObjectId());
        }

//        for (int i = memberIds.size() - 1; i >= 0; i--) {
//            insertSeenInfo(task.getObjectId(), memberIds.get(i), DBConstants.TASK_NOT_DELIVERED);
//        }

        insertSeenInfo(task.getObjectId(), task.getSeenByList());

        dbHelper.createTask(values);
    }
    // private ArrayList<String> loadGroupMembers(String gId) {
    // Cursor cursor = dbHelper.getGroupMembersId(gId);
    //
    // ArrayList<String> mList = new ArrayList<>();
    //
    // if (cursor != null) {
    // cursor.moveToFirst();
    //
    // do {
    // mList.add(cursor.getString(1));
    // } while (cursor.moveToNext());
    // }
    // return mList;
    // }

    public void addFriends(String id, String name) {
        try {
            dbHelper.insertNewTaskBud(id, name);
        } catch (Exception e) {
        }
    }

    /**
     * load group having gId and gCount
     * @param gId
     * @return cursor
     */
    public Cursor loadGroup(String gId) {
        return dbHelper.getGroup(gId);
    }

    /**
     * return memberId/fbId and name of all members of particular group
     * @param gId
     * @return cursor
     */
    public Cursor getGroupMembersId(String gId) {
        return dbHelper.getGroupMembersId(gId);
    }

    public void addGroupMember(String groupId, String userId) {
        dbHelper.addGroupMember(groupId, userId);
    }

    /**
     * load all task of given group from local sqlite db just to initialize list view
     * @param onTaskEventListener
     * @param groupId
     */
    public void loadAllTaskOfGroup(OnTaskEventListener onTaskEventListener, String groupId) {
        Cursor cursor = dbHelper.getGroupTasks(groupId);
        Debug.debug("Task Group id = " + cursor.getCount());
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                Debug.debug(" task's group id = " + cursor.getString(cursor.getColumnIndex(DBConstants.GROUP_ID)));
                onTaskLoaded(onTaskEventListener, cursor);

            } while (cursor.moveToNext());
        }
    }

    private void onTaskLoaded(OnTaskEventListener onTaskEventListener, Cursor cursor) {
        Task task = new Task();

        getTask(task, cursor);

        onTaskEventListener.onTaskLoaded(task);
    }

    /**
     * set the property of cursor to task object
     * @param task
     * @param cursor
     */
    private void getTask(Task task, Cursor cursor) {

        task.setObjectId(cursor.getString(cursor.getColumnIndex(DBConstants.TASK_ID)));
        task.setGroupObjectId(cursor.getString(cursor.getColumnIndex(DBConstants.GROUP_ID)));

        task.setAckCnfID(cursor.getString(cursor.getColumnIndex(DBConstants.TASK_ACK_CNF_ID)));
        task.setAckTime(cursor.getInt(cursor.getColumnIndex(DBConstants.TASK_ACK_TIME)));
        task.setCnfTime(cursor.getInt(cursor.getColumnIndex(DBConstants.TASK_CNF_TIME)));
        task.setAckd(cursor.getInt(cursor.getColumnIndex(DBConstants.TASK_IS_ACKNOWLEDGED)) == 1 ? true : false);
        task.setCnfd(cursor.getInt(cursor.getColumnIndex(DBConstants.TASK_IS_CONFIRMED)) == 1 ? true : false);
        task.setDeleted(cursor.getInt(cursor.getColumnIndex(DBConstants.TASK_IS_DELETED)) == 1 ? true : false);

        task.setSubject(cursor.getString(cursor.getColumnIndex(DBConstants.TASK_SUBJECT)));
        task.setDescription(cursor.getString(cursor.getColumnIndex(DBConstants.TASK_DESCRIPTION)));
        task.setRemark(cursor.getString(cursor.getColumnIndex(DBConstants.TASK_REMARK)));

        task.setTaskCreaterId(cursor.getString(cursor.getColumnIndex(DBConstants.TASK_CREATOR_ID)));
        task.setCreatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DBConstants.CREATED_AT))));
        task.setUpdatedAt(new Date(cursor.getLong(cursor.getColumnIndex(DBConstants.UPDATED_AT))));

        task.setExpireDate(cursor.getLong(cursor.getColumnIndex(DBConstants.TASK_EXPIRE_DATE)));
    }

    /**
     * update on SQLite db on acknowledged button clicked
     * @param groupId
     * @param task
     */
    public void updateTask(String groupId, Task task) {
        dbHelper.updateTask(groupId, task);
    }

    public Cursor getGroupTask(String taskId) {
        return dbHelper.getGroupTask(taskId);
    }

    public Cursor getTaskBudCursor(String memberId) {
        return dbHelper.getTaskBud(memberId);
    }

    public void deleteTask(String taskId) {
        dbHelper.deleteTask(taskId);
    }

    public void insertSeenInfo(String taskId, String seenBy, int status) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.TASK_ID, taskId);
        values.put(DBConstants.USER_ID, seenBy);
        values.put(DBConstants.STATUS, status);
        values.put(DBConstants.TASK_SEEN_INFO_WHEN, System.currentTimeMillis());

        dbHelper.insertSeenInfo(values);
    }

    public void insertSeenInfo(String taskId, SeenBy seenBy) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.TASK_ID, taskId);
        values.put(DBConstants.USER_ID, seenBy.getUserId());
        values.put(DBConstants.STATUS, seenBy.getStatus());
        values.put(DBConstants.TASK_SEEN_INFO_WHEN, seenBy.getWhen());

        dbHelper.insertSeenInfo(values);
    }

    public void insertSeenInfo(String taskId, ArrayList<String> list) {
        for (String string : list) {
            SeenBy seenBy = JsonUtils.stringToSeenBy(string);
            insertSeenInfo(taskId, seenBy);
        }
    }

    public int updateSeenInfo(String taskId, ArrayList<String> seenByList) {

        int i = seenByList.size() - 1;
        for (; i >= 0; i--) {

            SeenBy seenBy = JsonUtils.stringToSeenBy(seenByList.get(i));

            ContentValues values = new ContentValues();
            values.put(DBConstants.STATUS, seenBy.getStatus());
            values.put(DBConstants.TASK_SEEN_INFO_WHEN, seenBy.getWhen());

            int num = dbHelper.updateSeenInfo(taskId, seenBy.getUserId(), values);
            if (num == -1) {
                insertSeenInfo(taskId, seenBy);
            }
        }
        return i;
    }

    public int updateSeenInfo(String taskId, String seenBy, int status) {

        ContentValues values = new ContentValues();
        values.put(DBConstants.STATUS, status);
        values.put(DBConstants.TASK_SEEN_INFO_WHEN, System.currentTimeMillis());
        return dbHelper.updateSeenInfo(taskId, seenBy, values);
    }

    public void createTask(String groupId, Task task) {
        Cursor cursor = getGroupMembersId(groupId);
        ArrayList<String> memberIds = getGroupMembersId(cursor);

        createTask(task);
    }

    private ArrayList<String> getGroupMembersId(Cursor cursor) {
        if (cursor != null && cursor.getCount() > 0) {

            ArrayList<String> memberIds = new ArrayList<>();

            cursor.moveToFirst();
            do {
                memberIds.add(cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID)));
            } while (cursor.moveToNext());

            return memberIds;
        }
        return null;
    }

    public int updateSeenInfo(String taskId, SeenBy seenBy) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.STATUS, seenBy.getStatus());
        values.put(DBConstants.TASK_SEEN_INFO_WHEN, seenBy.getWhen());
        return dbHelper.updateSeenInfo(taskId, seenBy.getUserId(), values);
    }

    public Cursor getGroupTasks(String groupId) {
        return dbHelper.getGroupTasks(groupId);
    }

    public Cursor getTaskSeenInfoForMe(String taskId, String seenById) {
        return getTaskSeenInfoFor(taskId, seenById);
    }

    public Cursor getTaskSeenInfoFor(String taskId, String seenById) {
        return dbHelper.getTaskSeenInfoFor(taskId, seenById);
    }

    public String getUsername(String userId) {
        Cursor cursor = dbHelper.getTaskBud(userId);

        String userName = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            userName = cursor.getString(cursor.getColumnIndex(DBConstants.NAME));
        }

        return userName;
    }

    public String getDisplayName(String userId) {
        Cursor cursor = dbHelper.getTaskBud(userId);

        String userName = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            userName = cursor.getString(cursor.getColumnIndex(DBConstants.DISPLAY_NAME));
        }

        return userName;
    }

    public Cursor getGroupInfo(String groupId) {
        return dbHelper.getGroup(groupId);
    }

    public void updateDB() {
        dbHelper.updateDB();
    }

    public void updateGroupUpdateTime(String groupId) {
        dbHelper.updateGroupUpdateTime(groupId);
    }

    public void updateGroup(Group group) {
        if (shuldDelete(group)) {
            dbHelper.deleteGroup(group.getGroupId());
        } else {
            dbHelper.updateGroup(group);
        }
    }

    private boolean shuldDelete(Group group) {
        String adminId = DataStorage.getInstance(context).getUserId();
        if (group.getMemberIds().contains(adminId)) {
            return false;
        }
        return true;
    }

    public void getGroup(OnGroupLoadListener onGroupLoadListener) {

        Cursor cursor = dbHelper.getGroups();
        if (cursor != null && cursor.getCount() != 0) {
            Debug.debug("cursor count all = " + cursor.getCount());
            cursor.moveToFirst();

            do {
                Group group = new Group(context, cursor);

                ArrayList<GroupMember> groupMembers = getGroupMembers(group.getGroupId(), group.getAdminId());
                group.setGroupMembers(groupMembers);

                onGroupLoadListener.onGroupLoaded(group);

            } while (cursor.moveToNext());
        }
        // dataLoader.execute(new AsynInfo(GROUP_INFO, null));
    }

    public void updateTaskBud(TaskBudBean taskBudBean) {
        ContentValues values = new ContentValues();
        setTaskBudValues(values, taskBudBean);

        dbHelper.updateTaskBud(taskBudBean.getMemberId(), values);
    }

    private void setTaskBudValues(ContentValues values, TaskBudBean taskBudBean) {

        System.out.println("taskBudBean.getDisplayName() = " + taskBudBean.getDisplayName());
        if (taskBudBean.getDisplayName() != null) {
            values.put(DBConstants.DISPLAY_NAME, taskBudBean.getDisplayName());
        }
        if (taskBudBean.getUserName() != null) {
            values.put(DBConstants.NAME, taskBudBean.getUserName());
        }
        if (taskBudBean.getImageId() != null) {
            values.put(DBConstants.IMAGE_OBJECT_ID, taskBudBean.getImageId());
        }
        if (taskBudBean.getStatus() != null) {
            values.put(DBConstants.STATUS, taskBudBean.getStatus());
            Debug.debug("setTaskBudValues() status = " + taskBudBean.getStatus());
        }
        if (taskBudBean.getMemberId() != null) {
            values.put(DBConstants.TASKBUD_ID, taskBudBean.getMemberId());
        }
    }

    public void updateTaskBudFromParse(String id) {
        LeaderBoard.loadAppMember(id, new DataFetchListener<AppTaskBuds>() {
            @Override
            public void onFetchComplete(AppTaskBuds appTaskBud, boolean isLoaded) {
                if (appTaskBud != null) {
                    updateTaskBud(new TaskBudBean(appTaskBud));
                }
            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> objects) {

            }
        });
    }

    public void deleteLocalDb() {
        dbHelper.deleteDatabase();
    }

    public Cursor getAllTaskBuds() {
        return dbHelper.getTaskBudList();
    }
}