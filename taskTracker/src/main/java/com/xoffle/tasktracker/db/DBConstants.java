package com.xoffle.tasktracker.db;

public interface DBConstants {

    int VERSION = 1;

    public final int ID_LIMIT = 30;
    public final int NAME_LIMIT = 30;
    public final int STATUS_LIMIT = 100;

    public int TASK_NOT_DELIVERED = 0;
    public int TASK_DELIVERED = 1;
    public int TASK_SEEN = 2;

    String IMAGE_OBJECT_ID = "imageId";

    String DB_NAME = "DB_TASK_TRACKER";

    String CREATED_AT = "createAt";
    String UPDATED_AT = "updateAt";
    String DISPLAY_NAME = "displayName";

    /**
     * group table info
     */
    String TABLE_MY_GROUPS = "myGroup";
    String GROUP_ID = "id";
    String NAME = "name";
    String ADMIN_ID = "adminId";
    String CREATOR_ID = "creatorId";
    String STATUS = "status";
    String STATUS_UPDATE_ON = "statusUpadateOn";
    String IS_MY_GROUP = "IS_MY_GROUP";

    String CREATE_TABLE_MY_GROUPS = "CREATE TABLE " + TABLE_MY_GROUPS + " (" +
            GROUP_ID + " varchar(30), " +
            NAME + " varchar(30)," +
            ADMIN_ID + " varchar(30)," +
            CREATOR_ID + " varchar(30)," +
            STATUS + " varchar(150)," +
            STATUS_UPDATE_ON + " int(30)," +
            CREATED_AT + " int(30)," +
            UPDATED_AT + " int(30)," +
            IMAGE_OBJECT_ID + " varchar(30)," +
            IS_MY_GROUP + " int(1)," +
            "primary key("+ GROUP_ID+"));";

    /**
     * group table info
     */

    String TABLE_TASK_BUDS = "taskBuds";
    String TASKBUD_ID = "tbId";//facebook Id

    String CREATE_TABLE_TASK_BUDS = "CREATE TABLE " + TABLE_TASK_BUDS + " (" +
            TASKBUD_ID + " varchar("+ID_LIMIT+"), " +
            NAME + " varchar("+NAME_LIMIT+"), " +
            DISPLAY_NAME + " varchar("+NAME_LIMIT+"), " +
            STATUS + " varchar("+STATUS_LIMIT+"), " +
            IMAGE_OBJECT_ID + " varchar("+ID_LIMIT+"), " +
            "primary key(" + TASKBUD_ID + "));";


    /**
     * group table info
     */
    String TABLE_GROUP_MEMBERS = "groupMember";

    String CREATE_TABLE_GROUP_MEMBERS = "CREATE TABLE " + TABLE_GROUP_MEMBERS + " (" +
            GROUP_ID + " varchar(30), " +
            TASKBUD_ID + " varchar(30), " +
            "primary key("+ GROUP_ID+ ","+ TASKBUD_ID+"));";

    /**
     * group table info
     */
    String TABLE_TASKS = "tasks";
    String TASK_ID = "taskId";
    String TASK_ACK_CNF_ID = "ackCnfId";
    String TASK_ACK_TIME = "ackTime";
    String TASK_CNF_TIME = "cnfTime";
    String TASK_IS_ACKNOWLEDGED = "isAcknowledged";
    String TASK_IS_CONFIRMED = "is_confirmed";
    String TASK_IS_DELETED = "is_deleted";
    String TASK_SUBJECT = "subject";
    String TASK_DESCRIPTION = "description";
    String TASK_REMARK = "remark";
    String TASK_CREATOR_ID = "creatorId";
    String TASK_EXPIRE_DATE = "expDate";
//    String TASK_GROUP_OBJECT_ID = "groupObjectId";

    String CREATE_TABLE_TASK = "CREATE TABLE " + TABLE_TASKS + " (" +
            TASK_ID + " varchar(30)," +
            GROUP_ID + " varchar(30)," +
            TASK_ACK_CNF_ID + " varchar(30)," +
            TASK_ACK_TIME + " int(30)," +
            TASK_CNF_TIME + " int(30)," +
            TASK_IS_ACKNOWLEDGED + " inr(1)," +
            TASK_IS_CONFIRMED + "  int(1)," +
            TASK_IS_DELETED + " int(1)," +
            TASK_SUBJECT + " varchar(150)," +
            TASK_DESCRIPTION + " varchar(600)," +
            TASK_REMARK + " varchar(200)," +
            TASK_CREATOR_ID + " varchar(30)," +
            CREATED_AT + " int(30)," +
            UPDATED_AT + " int(30)," +
            TASK_EXPIRE_DATE + " int(30)," +
            "primary key("+ TASK_ID+"));";
//            TASK_GROUP_OBJECT_ID + " varchar(30));";

    /**
     * Table user to store date related to buds who seen the created task.
     */
    String TABLE_TASK_SEEN_INFO = "task_seen_info";
    String TASK_SEEN_INFO_WHEN = "updateTime";
    String USER_ID = "userId";

    String CREATE_TABLE_TASK_SEEN_INFO = "CREATE TABLE " + TABLE_TASK_SEEN_INFO + " (" +
            TASK_ID + " varchar(30), " +
            STATUS + " varchar(150), " +
            TASK_SEEN_INFO_WHEN + " int(30), " +
            USER_ID + " varchar(30));";
}