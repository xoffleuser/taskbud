package com.xoffle.tasktracker.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.inkling.leaderboard.AppGroup;
import com.inkling.leaderboard.Task;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.util.Debug;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {

        super(context, DBConstants.DB_NAME, null, DBConstants.VERSION);
        System.out.println("DBHelper.DBHelper()");

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    private void createTables(SQLiteDatabase db) {
        db.execSQL(DBConstants.CREATE_TABLE_MY_GROUPS);
        db.execSQL(DBConstants.CREATE_TABLE_TASK_BUDS);
        db.execSQL(DBConstants.CREATE_TABLE_GROUP_MEMBERS);
        db.execSQL(DBConstants.CREATE_TABLE_TASK);
        db.execSQL(DBConstants.CREATE_TABLE_TASK_SEEN_INFO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("DROP TABLE IF_EXIST " + DBConstants.TABLE_MY_GROUPS);
//        db.execSQL("DROP TABLE IF_EXIST " + DBConstants.TABLE_TASK_BUDS);
//        db.execSQL("DROP TABLE IF_EXIST " + DBConstants.TABLE_GROUP_MEMBERS);
//        db.execSQL("DROP TABLE IF_EXIST " + DBConstants.TABLE_TASKS);
//        db.execSQL("DROP TABLE IF_EXIST " + DBConstants.TABLE_TASK_SEEN_INFO);
    }

    public void createGroup(AppGroup appGroup) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            String imageId = appGroup.getImageObjectId();
            if (imageId == null) {
                imageId = "";
            }

            db.execSQL("INSERT INTO " + DBConstants.TABLE_MY_GROUPS +
                    " values('" + appGroup.getObjectId() + "'," +
                    " '" + appGroup.getGroupName() + "'," +
                    " '" + appGroup.getAdminId() + "'," +
                    " '" + appGroup.getCreatorId() + "'," +
                    " '" + appGroup.getGroupStatus() + "'," +
                    appGroup.getStatusUpdateOn() + "," +
                    System.currentTimeMillis() + "," +
                    System.currentTimeMillis() + "," +
                    " '" + imageId + "'," +
                    " " + 0 + ")");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createGroup(ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.insertOrThrow(DBConstants.TABLE_MY_GROUPS, null, values);
            Debug.debug("create Group()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addGroupMember(String groupId, String tbId) {

        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL("INSERT INTO " + DBConstants.TABLE_GROUP_MEMBERS + " values('" + groupId + "', '" + tbId + "');");
        } catch (SQLException e) {
        }
    }

    public void createTask(ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();

        Debug.debug("DBHelper create task");

        try {
            db.insert(DBConstants.TABLE_TASKS, null, values);

            Debug.debug("data inserted successfully");
        } catch (SQLException e) {
            Debug.debug("exception occurred");
        }
    }

    public Cursor getGroups() {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + DBConstants.TABLE_MY_GROUPS + " ORDER BY " + DBConstants.IS_MY_GROUP + " DESC, " + DBConstants.UPDATED_AT + " DESC";

        return db.rawQuery(query, null);
    }

//    public Cursor getGroup(String groupId) {
//        SQLiteDatabase db = getReadableDatabase();
//        String query = "SELECT * FROM " + DBConstants.TABLE_MY_GROUPS + " ORDER BY " + DBConstants.IS_MY_GROUP + " DESC, " + DBConstants.UPDATED_AT + " DESC";
//
//        return db.rawQuery(query, null);
//    }

    public Cursor getGroupMembersId(String groupId) {
        SQLiteDatabase db = getReadableDatabase();

        String selection = DBConstants.GROUP_ID + " = ?";
        String[] args = {groupId};
        return db.query(DBConstants.TABLE_GROUP_MEMBERS, null, selection, args, null, null, null);
    }

    /**
     * return all tasks of group
     * @param groupId
     * @return cursor containing all task of this group(groupId,groupCount)
     */
    public Cursor getGroupTasks(String groupId) {
        SQLiteDatabase db = getReadableDatabase();
        String selection = DBConstants.GROUP_ID + " = ?";
        String[] args = {groupId};

        //asc for assending order
        String orderBy = DBConstants.UPDATED_AT + " DESC";

        return db.query(DBConstants.TABLE_TASKS, null, selection, args, null, null, orderBy, "");
//        return db.query(DBConstants.TABLE_TASKS, null, null, null, null, null, null, null);
    }

    /**
     * @return return all fb friends having this app in there device
     */
    public Cursor getTaskBudList() {
        SQLiteDatabase db = getReadableDatabase();
        return db.query(DBConstants.TABLE_TASK_BUDS, null, null, null, null, null, null);
    }

    public void insertNewTaskBud(String id, String name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBConstants.TASKBUD_ID, id);
        values.put(DBConstants.NAME, name);
        try {
            db.insert(DBConstants.TABLE_TASK_BUDS, null, values);
        } catch (SQLException e) {
        }
    }

    /**
     * @param gId
     * @return
     */
    public Cursor getGroup(String gId) {
        SQLiteDatabase db = getReadableDatabase();
        String selection = DBConstants.GROUP_ID + " = ?";
        String selectionArgs[] = {gId};
        return db.query(DBConstants.TABLE_MY_GROUPS, null, selection, selectionArgs, null, null, null);
    }

    public void updateTask(String groupId, Task task) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        assignValueOnTaskUpdate(task, values);

        String whereClause = DBConstants.GROUP_ID + " = ? and " + DBConstants.TASK_ID + " = ?";
        String whereArgs[] = {groupId, task.getObjectId()};

        try {
            db.update(DBConstants.TABLE_TASKS, values, whereClause, whereArgs);
        } catch (SQLException e) {
        }
    }

    private void assignValueOnTaskUpdate(Task task, ContentValues values) {
        values.put(DBConstants.TASK_IS_ACKNOWLEDGED, task.isAckd() == true ? 1 : 0);
        values.put(DBConstants.TASK_IS_CONFIRMED, task.isCnfd() == true ? 1 : 0);

        values.put(DBConstants.TASK_ACK_TIME, task.getAckTime());
        values.put(DBConstants.TASK_CNF_TIME, task.getCnfTime());

        if (task.getAckCnfID() != null) {
            values.put(DBConstants.TASK_ACK_CNF_ID, task.getAckCnfID());
        }

        if (task.getDescription() != null) {
            values.put(DBConstants.TASK_DESCRIPTION, task.getDescription());
        }
        if (task.getRemark() != null) {
            values.put(DBConstants.TASK_REMARK, task.getRemark());
        }

        values.put(DBConstants.TASK_IS_DELETED, task.isDeleted() == true ? 1 : 0);

        if (task.getSubject() != null) {
            values.put(DBConstants.TASK_SUBJECT, task.getSubject());
        }

        if (task.getTaskCreaterId() != null) {
            values.put(DBConstants.TASK_CREATOR_ID, task.getTaskCreaterId());
        }

        if (task.getExpireDate() != 0) {
            values.put(DBConstants.TASK_EXPIRE_DATE, task.getExpireDate());
        }

        values.put(DBConstants.UPDATED_AT, System.currentTimeMillis());
    }

    public Cursor getGroupTask(String taskId) {
        SQLiteDatabase db = getReadableDatabase();
        String selection = DBConstants.TASK_ID + " = ?";
        String selectionArgs[] = {taskId};
        return db.query(DBConstants.TABLE_TASKS, null, selection, selectionArgs, null, null, null);
    }

    public Cursor getTaskBud(String tbId) {
        SQLiteDatabase db = getReadableDatabase();

        String selection = DBConstants.TASKBUD_ID + " =?";
        String[] args = {tbId};
        return db.query(DBConstants.TABLE_TASK_BUDS, null, selection, args, null, null, null);
    }

    public void deleteTask(String taskId) {
        SQLiteDatabase db = getWritableDatabase();

        String whereClause = DBConstants.TASK_ID + " = ?";
        String[] whereArgs = {taskId};
        db.delete(DBConstants.TABLE_TASKS, whereClause, whereArgs);
    }

    public void insertSeenInfo(ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.insert(DBConstants.TABLE_TASK_SEEN_INFO, null, values);
        } catch (Exception e) {
        }
    }

    public int updateSeenInfo(String taskId, String seenBy, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            String whereClause = DBConstants.GROUP_ID + " = ? and " +
                    DBConstants.TASK_ID + " = ? and " +
                    DBConstants.USER_ID + " =?";
            String[] whereArgs = {taskId, seenBy};
            return db.update(DBConstants.TABLE_TASK_SEEN_INFO, values, whereClause, whereArgs);
        } catch (Exception e) {
            return -1;
        }
    }

    public Cursor getTaskSeenInfoFor(String taskId, String seenById) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            String whereClause = DBConstants.TASK_ID + " = ? and " +
                    DBConstants.USER_ID + " =?";
            String[] whereArgs = {taskId, seenById};

            return db.query(DBConstants.TABLE_TASK_SEEN_INFO, null, whereClause, whereArgs, null, null, null);
        } catch (Exception e) {
            return null;
        }
    }

    public void updateDB() {
        try {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL("ALTER TABLE " + DBConstants.TABLE_MY_GROUPS + " ADD COLUMN " + DBConstants.IS_MY_GROUP + " int(1)");
        } catch (Exception e) {
        }
    }

    public int updateGroupUpdateTime(String groupId) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put(DBConstants.UPDATED_AT, System.currentTimeMillis());
            String whereClause = DBConstants.GROUP_ID + " = ?";
            String[] whereArgs = {groupId};
            return db.update(DBConstants.TABLE_MY_GROUPS, values, whereClause, whereArgs);
        } catch (Exception e) {
            return -1;
        }
    }

    public int updateGroup(Group group) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            updateGroupMembers(group.getGroupId(), group.getMemberIds());

            ContentValues values = new ContentValues();
            updateGroupData(values, group);

            String whereClause = DBConstants.GROUP_ID + " = ?";
            String[] whereArgs = {group.getGroupId()};
            return db.update(DBConstants.TABLE_MY_GROUPS, values, whereClause, whereArgs);
        } catch (Exception e) {
            return -1;
        }
    }

    private void updateGroupMembers(String groupId, ArrayList<String> memberIds) {
        /**delete previous member of group*/
        deleteAllGroupMembers(groupId);

        /**add all members again*/
        if (memberIds != null) {
            for (int i = memberIds.size() - 1; i >= 0; i--) {
                addGroupMember(groupId, memberIds.get(i));
            }
        }
    }

    private void deleteAllGroupMembers(String groupId) {
        SQLiteDatabase db = getWritableDatabase();
        String where = DBConstants.GROUP_ID + "=?";
        String[] whereArgs = {groupId};
        db.delete(DBConstants.TABLE_GROUP_MEMBERS, where, whereArgs);
    }

    private void updateGroupData(ContentValues values, Group group) {

        values.put(DBConstants.GROUP_ID, group.getGroupId());
        values.put(DBConstants.NAME, group.getName());
        values.put(DBConstants.ADMIN_ID, group.getAdminId());
        values.put(DBConstants.CREATOR_ID, group.getCreatorId());
        values.put(DBConstants.STATUS, group.getStatus());
        values.put(DBConstants.STATUS_UPDATE_ON, group.getStatusUpdateOn());
        values.put(DBConstants.IS_MY_GROUP, group.isMyGroup());
        values.put(DBConstants.UPDATED_AT, System.currentTimeMillis());

        values.put(DBConstants.IMAGE_OBJECT_ID, group.getImageObjectId());
    }

    public void deleteGroup(String groupId) {
        SQLiteDatabase db = getWritableDatabase();

        String whereClause = DBConstants.GROUP_ID + "=?";
        String[] whereArgs = {groupId};
        db.delete(DBConstants.TABLE_MY_GROUPS, whereClause, whereArgs);

        deleteAllGroupMembers(groupId);
    }

    public int updateTaskBud(String memberId, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            String whereClause = DBConstants.TASKBUD_ID + " = ?";
            String[] whereArgs = {memberId};
            return db.update(DBConstants.TABLE_TASK_BUDS, values, whereClause, whereArgs);
        } catch (Exception e) {
            Debug.debug("setError Occured...");
            e.printStackTrace();
            return -1;
        }
    }

    public void deleteDatabase() {
        try {
//            SQLiteDatabase db = getWritableDatabase();
//            db.execSQL("DROP TABLE " + DBConstants.TABLE_MY_GROUPS);
//            db.execSQL("DROP TABLE " + DBConstants.TABLE_TASK_BUDS);
//            db.execSQL("DROP TABLE " + DBConstants.TABLE_GROUP_MEMBERS);
//            db.execSQL("DROP TABLE " + DBConstants.TABLE_TASKS);
//            db.execSQL("DROP TABLE " + DBConstants.TABLE_TASK_SEEN_INFO);
//
//            db.execSQL("DROP DATABASE "+DBConstants.DB_NAME);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}