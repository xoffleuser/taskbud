package com.xoffle.tasktracker.db;

import android.content.Context;
import android.database.Cursor;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.group.GroupMember;
import com.xoffle.tasktracker.model.DataStorage;

/**
 * Created by ravi on 4/8/15.
 */
public class CursorFactory {
    public static GroupMember createGroupMemberBean(String taskBudId, Context context, String adminId) {

//        Cursor cursor = DBController.getInstance(context).getTaskBudCursor(taskBudId);
//        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
//
//            String id = cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID));
//            String userName = cursor.getString(cursor.getColumnIndex(DBConstants.NAME));
////            String displayName = cursor.getString(cursor.getColumnIndex(DBConstants.DISPLAY_NAME));
////            String status = cursor.getString(cursor.getColumnIndex(DBConstants.STATUS));
////            String imageId = cursor.getString(cursor.getColumnIndex(DBConstants.IMAGE_OBJECT_ID));
//
////            TaskBudBean taskBudBean = new TaskBudBean(id, userName);
        TaskBudBean taskBudBean = createTaskBud(context, taskBudId);

        if (taskBudBean != null) {
            GroupMember memberBean = new GroupMember(taskBudBean);
//            GroupMember memberBean = new GroupMember(id, userName);

//            memberBean.setMemberId(id);

//            memberBean.setUserName(displayName);
//
//            memberBean.setImageId(imageId);
//
//            memberBean.setStatus(status);

            String myId = DataStorage.getInstance(context).getUserId();

            memberBean.setMe(myId.equals(memberBean.getMemberId()));

            memberBean.setAdmin(adminId.equals(memberBean.getMemberId()));

            if (memberBean.isMe()) {
                memberBean.setDisplayName(context.getString(R.string.you));
            }
//            else {
////                memberBean.setDisplayName(memberBean.getUserName());
//            }

            return memberBean;

        }
        return null;
    }

    public static TaskBudBean createTaskBud(Context context, String taskBudId) {

        Cursor cursor = DBController.getInstance(context).getTaskBudCursor(taskBudId);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            String id = getString(cursor,cursor.getColumnIndex(DBConstants.TASKBUD_ID));
            String userName = getString(cursor,cursor.getColumnIndex(DBConstants.NAME));
            String displayName = getString(cursor,cursor.getColumnIndex(DBConstants.DISPLAY_NAME));
            String status = getString(cursor,cursor.getColumnIndex(DBConstants.STATUS));
            String imageId = getString(cursor,cursor.getColumnIndex(DBConstants.IMAGE_OBJECT_ID));

            TaskBudBean taskBudBean = new TaskBudBean(id, displayName);
            taskBudBean.setUserName(userName);
            taskBudBean.setStatus(status);
            taskBudBean.setImageId(imageId);
            return taskBudBean;
        }
        return null;
    }

    private static String getString(Cursor cursor, int index) {
        return index < 0 ? null : cursor.getString(index);
    }
}