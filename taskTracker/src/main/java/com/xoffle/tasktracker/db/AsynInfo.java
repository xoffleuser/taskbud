package com.xoffle.tasktracker.db;

public class AsynInfo {

	public AsynInfo(int type, String gId) {
		this.type = type;
		this.gId = gId;
	}

	private int type;
	private String gId;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getgId() {
		return gId;
	}

	public void setgId(String gId) {
		this.gId = gId;
	}

}
