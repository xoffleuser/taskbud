package com.xoffle.tasktracker.userauth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.fb.FacebookButton;
import com.xoffle.tasktracker.fb.FacebookUtil;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.model.Constant;

public class UserauthActivity extends ActionBarActivity {

    public static final String IS_LOGGIN_FIRST_TIME = "isLoggingFirstTime";
    private static final String TAG = "USER_AUTH_TAG";
    private FacebookUtil facebookUtil;
    private FacebookButton fbButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userauth);

        facebookUtil = FacebookUtil.getInstance(this);
        fbButton = new FacebookButton(this, facebookUtil);

        logout();

        checkAuthentication();
    }

    private void logout() {
        if (getIntent() != null) {
            boolean isLogout = getIntent().getBooleanExtra(Constant.SHOULD_LOGOUT, false);
            if (isLogout) {
                fbButton.logout();
                FacebookUtil.destroy();
                facebookUtil = FacebookUtil.getInstance(this);
                fbButton = new FacebookButton(this, facebookUtil);
            }
        }
    }

    private void checkAuthentication() {
//        if (fbButton.isLoggedIn()) {
//            Intent intent = new Intent(UserauthActivity.this, MainActivity.class);
//            finish();
//            startActivity(intent);
//        } else {
        addSignInFragment();
//        }
    }

    void facebookSignIn() {
        Intent intent = new Intent(UserauthActivity.this, MainActivity.class);
        intent.putExtra(IS_LOGGIN_FIRST_TIME, true);
        finish();
        startActivity(intent);
    }

    private void addSignInFragment() {
        SignInFragment fragment = new SignInFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.userauth_activity, fragment).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userauth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookUtil.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        facebookUtil.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        facebookUtil.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        facebookUtil.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        facebookUtil.onDestroy();
        super.onDestroy();
    }

    public FacebookButton getFacebookButton() {
        return fbButton;
    }
}
