package com.xoffle.tasktracker.userauth;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.fb.FacebookButton;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;

/**
 * Created by ravi on 26/4/15.
 */
public class SignInFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sign_in_fragment, container, false);

        ImageView facebookviewLogin = (ImageView) view.findViewById(R.id.signInFraagmentFacebookLogin);
        facebookviewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        return view;
    }

    void login() {

        if (!Utils.isNetworkAvailble(getActivity())) {
            Utils.showToast(getActivity(), getString(R.string.no_internet_connection));
            return;
        }

        FacebookButton fbButton = ((UserauthActivity) getActivity()).getFacebookButton();
        fbButton.login(new FacebookButton.OnLogginToFb() {

            @Override
            public void onLoggin() {
                Debug.debug("Main Activity logged in successfully");

                ((UserauthActivity) getActivity()).facebookSignIn();
            }
        });

    }
}
