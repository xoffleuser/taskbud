package com.xoffle.tasktracker.user;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.image.ParseImageHandler;
import com.xoffle.tasktracker.listeners.OnImageUploadListener;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.utilfragments.ImageChangeFragment;
import com.xoffle.tasktracker.utilfragments.ImageChangeListener;
import com.xoffle.tasktracker.utilfragments.TextChangeFragment;
import com.xoffle.tasktracker.utilfragments.TextChangeListener;
import com.xoffle.tasktracker.view.MyFragment;

/**
 * Created by ravi on 13/8/15.
 */
public class UserDetailFragment extends MyFragment implements View.OnClickListener {

    private static final String TASK_BUD = "TASK_BUD";
    public static boolean isActive;
    private boolean isMe;
    private ImageView profilePicImageView;
    private TextView displayNameTextView;
    private TextView usernameTextView;
    private TextView statusTextView;
    private TaskBudBean taskBudBean;

    public static UserDetailFragment newInstance(TaskBudBean taskBudBean) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(TASK_BUD, taskBudBean);

        UserDetailFragment fragment = new UserDetailFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_detail_fragment, container, false);

        taskBudBean = (TaskBudBean) getArguments().getSerializable(TASK_BUD);
        isMe = taskBudBean.getMemberId().equals(DataStorage.getInstance(getActivity()).getUserId());

        setToolbar(view);

        initViewsAndSetListener(view);

        setData();

        return view;
    }

    private void setData() {
        ImageUtil.setImageFromStorage(profilePicImageView, taskBudBean.getImageId());

        displayNameTextView.setText(taskBudBean.getDisplayName());
        usernameTextView.setText(taskBudBean.getUserName());

        statusTextView.setText(taskBudBean.getStatus());
    }

    private void initViewsAndSetListener(View view) {

        profilePicImageView = (ImageView) view.findViewById(R.id.userDetailFragmentProfillePic);
        ImageView profilePicEditImageView = (ImageView) view.findViewById(R.id.userDetailFragmentProfilePicEditIv);
        if (isMe) {
            setClickListener(profilePicEditImageView);
        } else {
            profilePicEditImageView.setVisibility(View.GONE);
        }

        displayNameTextView = (TextView) view.findViewById(R.id.userDetailFragmentDisplayName);
        if (isMe) {
            displayNameTextView.setOnClickListener(this);
        }

        usernameTextView = (TextView) view.findViewById(R.id.userDetailFragmentUsername);

        View statusView = view.findViewById(R.id.userDetailFragmentStatusView);
        statusTextView = (TextView) statusView.findViewById(R.id.statusViewStatus);
        ImageView statusChangeImageView = (ImageView) statusView.findViewById(R.id.statusViewEditIv);

        if (isMe) {
            setClickListener(statusChangeImageView);
        } else {
            statusChangeImageView.setVisibility(View.GONE);
        }
    }

    private void setClickListener(ImageView view) {
        view.setVisibility(View.VISIBLE);
        view.setOnClickListener(this);
    }

    private void setToolbar(View view) {
        initMiddleTextToolbar(view, R.id.userDetailFragmentMiddleTextToolbar, getActivity().getString(R.string.my_profile));
    }

    @Override
    public void onResume() {
        super.onResume();

        isActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();

        isActive = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.userDetailFragmentProfilePicEditIv:
                openChangeImageFragment();
                break;

            case R.id.userDetailFragmentDisplayName:
                openNameChangeFragment();
                break;

            case R.id.statusViewEditIv:
                openStatusChangeFragment();
                break;
        }
    }

    private void openChangeImageFragment() {
        Utils.showToast(getActivity(), "Open Change Image Fragment");
        final ImageChangeFragment fragment = ImageChangeFragment.newInstance(taskBudBean.getImageId());
        ((MainActivity) getActivity()).addFragment(fragment, true);
        fragment.setImageChangeListener(new ImageChangeListener() {
            @Override
            public void onImageChange(String imageName) {
                fragment.showLoadingDialog();

                updateImageToDb(fragment, imageName);
            }
        });
    }

    private void updateImageToDb(final ImageChangeFragment fragment, String imageName) {
        ParseImageHandler.saveUserImage(getActivity(), taskBudBean.getImageId(), new OnImageUploadListener() {
            @Override
            public void onImageUpload(Bitmap bitmap, String imageObjectId) {
                Debug.debug("updateImageToDb bitmap = " + bitmap);
                if (bitmap != null) {
                    fragment.dismissLoadingDialog();

                    taskBudBean.setImageId(imageObjectId);
                    LeaderBoard.updateAppTaskBud(taskBudBean,null);
                    DBController.getInstance(getActivity()).updateTaskBud(taskBudBean);

                    profilePicImageView.setImageBitmap(bitmap);
                }
            }
        });
    }

    private void openStatusChangeFragment() {
        final TextChangeFragment fragment = TextChangeFragment.newInstance("Status", taskBudBean.getStatus(), 100);
        ((MainActivity) getActivity()).addFragment(fragment, true);

        fragment.setTextChangeListener(new TextChangeListener() {
            @Override
            public void onTextUpdate(String status) {
                taskBudBean.setStatus(status);
                statusTextView.setText(status);
                DBController.getInstance(getActivity()).updateTaskBud(taskBudBean);
                fragment.hideLoadingDialog();
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    private void openNameChangeFragment() {
        final TextChangeFragment fragment = TextChangeFragment.newInstance("Display Name", taskBudBean.getDisplayName(), 40);
        ((MainActivity) getActivity()).addFragment(fragment, true);

        fragment.setTextChangeListener(new TextChangeListener() {
            @Override
            public void onTextUpdate(String displayName) {
                taskBudBean.setDisplayName(displayName);
                displayNameTextView.setText(displayName);

                DBController.getInstance(getActivity()).updateTaskBud(taskBudBean);
                fragment.hideLoadingDialog();
                getFragmentManager().popBackStackImmediate();
            }
        });
    }
}
