package com.xoffle.tasktracker.beans;


import com.xoffle.tasktracker.util.Utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by ravi on 10/5/15.
 */
public class BeanParser {
    public static void parse(String info, SeenInfoBean seenInfoBean) {
        if (Utils.isNull(info))
            return;

        String[] split = info.split("#");

        try {
            String id = split[0];
            seenInfoBean.setSeenById(id);

            int status = Integer.parseInt(split[1].trim());
            seenInfoBean.setStatus(status);

            long date = Long.parseLong(split[2].trim());
            seenInfoBean.setUpdateDate(date);
        } catch (Exception e) {
        }
    }
}
