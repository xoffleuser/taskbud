package com.xoffle.tasktracker.beans;

/**
 * Created by ravi on 10/5/15.
 */
public class SeenInfoBean {

    private String seenById;
    private long updateDate;
    private int status;

    public SeenInfoBean(String seenById, int status, long updateTime) {
        this.seenById = seenById;
        this.updateDate = updateTime;
        this.status = status;
    }

    public SeenInfoBean() {
    }

    public String getSeenById() {
        return seenById;
    }

    public void setSeenById(String seenById) {
        this.seenById = seenById;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return seenById + "#" + status + "#" + updateDate;
    }
}
