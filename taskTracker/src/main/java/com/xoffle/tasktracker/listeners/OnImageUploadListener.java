package com.xoffle.tasktracker.listeners;

import android.graphics.Bitmap;

/**
 * Created by ravi on 23/7/15.
 */
public interface OnImageUploadListener {
    void onImageUpload(Bitmap bitmap, String objectId);
}
