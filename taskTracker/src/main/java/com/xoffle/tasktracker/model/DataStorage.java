package com.xoffle.tasktracker.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashSet;

public class DataStorage {

    private static final String NAME = "com.xoffle.tasktracker.data_storage";
    private static final String USER_ID = "USER_ID";
    private static final String DISPLAY_NAME = "DISPLAY_NAME";
    private static final String GROUP_COUNT = "GROUP_COUNT";
    private static final String APP_REVIEW = "APP_REVIEW";
    private static final String TASK_ID = "TASK_COUNT";
    private static final String UPDATE_MSG = "UPDATE_MSG";
    private static final String UPDATE_MSG_COUNTER = "UPDATE_MSG_COUNTER";
    private static final String USER_NAME = "USER_NAME";
    private static final String MY_GROUP = "MY_GROUP";
    private static final String IS_UPDATE_AVAILABLE = "IS_UPDATE_AVAILABLE" + 1;
    private static final String IMAGE_UPDATES = "IMAGE_UPDATES";
    private static DataStorage instance;
    private SharedPreferences preferences;

    private DataStorage(Context context) {
        preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public static DataStorage getInstance(Context context) {
        return instance == null ? instance = new DataStorage(context) : instance;
    }

    public String getUserId() {
        return preferences.getString(USER_ID, null);
    }

    public void setUserId(String userId) {
        Editor edit = preferences.edit();
        edit.putString(USER_ID, userId);
        edit.commit();
    }

    public String getDisplayName() {
        return preferences.getString(DISPLAY_NAME, null);
    }

    public void setDisplayName(String userName) {
        Editor edit = preferences.edit();
        edit.putString(DISPLAY_NAME, userName);
        edit.commit();
    }

    public String getUsername() {
        return preferences.getString(USER_NAME, null);
    }

    public void setUsername(String username) {
        preferences.edit().putString(USER_NAME, username).commit();
    }

    public void appReviewed() {
        Editor edit = preferences.edit();
        edit.putBoolean(APP_REVIEW, true);
        edit.commit();
    }

    public void addUpdateMsg(String message) {
        Editor edit = preferences.edit();
        edit.putString(UPDATE_MSG + getUpdateMsgCounter(), message);
        System.out.print(" ------------,,,,,,,,,,,,,,,,,,,,,," + getUpdateMsgCounter() + " msg = " + message);
        edit.commit();
    }

    /**
     * @return int counter value
     */
    public int getUpdateMsgCounter() {
        return preferences.getInt(UPDATE_MSG_COUNTER, 0);
    }

    /**
     * counter is user for storing new updates
     * @param counter
     */
    public void setUpdateMsgCounter(int counter) {
        Editor edit = preferences.edit();
        edit.putInt(UPDATE_MSG_COUNTER, counter);
        edit.commit();
    }

    public String getUpdateMessage(int currentCounter) {
        return preferences.getString(UPDATE_MSG + currentCounter, null);
    }

    public boolean isMyGroupCreated() {
        return preferences.getBoolean(MY_GROUP, false);
    }

    public void setIsMyGroupCreated(boolean status) {
        preferences.edit().putBoolean(MY_GROUP, status).commit();
    }

    public boolean isUpdateAvailable() {
        return preferences.getBoolean(IS_UPDATE_AVAILABLE, true);
    }

    public void setIsUpdateAvailable(boolean status) {
        preferences.edit().putBoolean(IS_UPDATE_AVAILABLE, status).commit();
    }

    public void setImageUpdates(HashSet<String> set) {
        preferences.edit().putStringSet(IMAGE_UPDATES, set).commit();
    }

    public java.util.Set<String> getImageUpdates() {
        return preferences.getStringSet(IMAGE_UPDATES, null);
    }
}