package com.xoffle.tasktracker.model;

import org.json.JSONObject;

public class Update {

    private int counter;
    private JSONObject json;

    public Update(int counter,JSONObject json) {
        this.counter = counter;
        this.json = json;
    }

    public int getCounter() {
        return counter;
    }

    public JSONObject getJson() {
        return json;
    }
}