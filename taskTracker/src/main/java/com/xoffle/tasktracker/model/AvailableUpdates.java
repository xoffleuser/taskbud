package com.xoffle.tasktracker.model;

public class AvailableUpdates {

	public static final int NEW_GROUP = 20;
	public static final int DELETE_GROUP = 21;
	public static final int UPDATE_GROUP_ICON = 221;
	public static final int UPDATE_GROUP_NAME = 222;
	public static final int UPDATE_GROUP_ADD_MEM = 223;
	public static final int UPDATE_GROUP_REMOVE_MEM = 224;

	public static final int NEW_TASK = 10;
	public static final int DELETE_TASK = 11;
	public static final int UPDATE_TASK_EDIT_TEXT = 121;
	public static final int UPDATE_TASK_CONFORM = 122;
	public static final int UPDATE_TASK_ACK = 123;
	public static final int UPDATE_TASK_ADD_REMARK = 124;
	public static final int UPDATE_TASK_REMOVE = 125;
	public static final int UPDATE_TASK_EXPIRE = 126;

	public void addUpdate() {
		
	}
}
