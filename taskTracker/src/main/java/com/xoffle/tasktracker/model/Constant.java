package com.xoffle.tasktracker.model;

import android.os.Environment;

/**
 * Created by ravi on 4/5/15.
 */
public interface Constant {
    String UPDATE_MSG = "updateMessage";
    String UPDATE_REMARK = "updateRemark";
    String EXISTING_USER = "ExistingUser";
    String TITLE = "title";
    String SHOULD_LOGOUT = "should_logout";

    public interface TaskConstant {
        int FILTER_ACKNOWLEDGED_ONLY = 1;
        int FILTER_CONFIRMED = 2;
        int FILTER_CNFD_AND_ACKD = 3;
        int FILTER_ACTIVE = 5;
        int FILTER_NO_ACTIVITY = 7;
        int FILTER_ALL = 8;

        int SHOW_RENCET = 4;
        int SHOW_CREATE_AT = 6;
    }

    public interface ImageC{
        String IMAGE_DIR = "/my_images";
        String GROUP_IMG_NAME = "group_icon";
        String IMAGE_NAME = "image_name";
        String IMAGE_PATH = Environment.getExternalStorageDirectory().toString()+IMAGE_DIR;
        String IMAGE_EXT = ".png";
    }

    public interface EditTextC{
        int DRAWABLE_LEFT = 0;
        int DRAWABLE_TOP = 1;
        int DRAWABLE_RIGHT = 2;
        int DRAWABLE_BOTTOM = 3;
    }
}
