package com.xoffle.tasktracker.json;

import android.content.Context;

import com.inkling.leaderboard.SeenBy;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ravi on 19/4/15.
 */
public class JsonParser {

    public static void parseAppUpdates(Context context, OnParseUpdateListener listener) {
        DataStorage store = DataStorage.getInstance(context);
        int counter = store.getUpdateMsgCounter();

        for (int i = 0; i < counter; i++) {
            String msg = store.getUpdateMessage(i);
            Debug.debug("update " + i + " = " + msg);

            if (!Utils.isNull(msg)) {
                parseUpdate(context, listener, msg);
            }
        }

        store.setUpdateMsgCounter(0);
    }

    private static void parseUpdate(Context context, OnParseUpdateListener listener, String msg) {
        Debug.debug("msg = " + msg);
        try {
            JSONObject json = new JSONObject(msg);
            int type = json.getInt(JsonCreater.TYPE);
            Debug.debug("type = " + type);
            switch (type) {

                case JsonCreater.TYPE_TASK_CREATED:
                case JsonCreater.TYPE_TASK_UPDATED:
                case JsonCreater.TYPE_TASK_DELETED:

                    parseMessage(context, listener, json);

                    break;
                default:
                    break;
            }
        } catch (JSONException e) {
        }
    }

    private static void parseMessage(Context context, OnParseUpdateListener listener, JSONObject json) throws JSONException {
        String groupId = json.getString(JsonCreater.GROUP_ID);
        String taskId = json.getString(JsonCreater.TASK_ID);

        updateGroupUpdateCounter(listener, groupId);
    }

    public static void parseAppUpdateFromNotification(Context context, String msg, OnParseUpdateListener listener) {

        Debug.debug("update = " + msg);

        if (!Utils.isNull(msg)) {
            parseAppsUpdate(context, listener, msg);
        }
    }

    private static void parseAppsUpdate(Context context, OnParseUpdateListener listener, String msg) {
        Debug.debug("msg = " + msg);
        try {
            JSONObject json = new JSONObject(msg);
            int type = json.getInt(JsonCreater.TYPE);
            Debug.debug("type = " + type);
            switch (type) {
                case JsonCreater.TYPE_GROUND_CREATED:
                    parseOnGroupCreated(context, listener, json);
                    break;
                case JsonCreater.TYPE_GROUP_UPDATED:
                    parseOnGroupUpdate(context,listener,json);
                    break;
                case JsonCreater.TYPE_TASK_CREATED:
                    parseOnTaskCreated(context, listener, json);
                    break;
                case JsonCreater.TYPE_TASK_UPDATED:
                    parseOnTaskUpdated(context, listener, json);
                    break;
                case JsonCreater.TYPE_TASK_DELETED:
                    parseOnGroupTaskDelete(context, json);
                    break;
                case JsonCreater.TYPE_TASK_USER_INFO_UPDATE:
                    parseOnTaskSeenInfoChange(context, json);
                    break;
                default:
                    break;
            }
        } catch (JSONException e) {
        }
    }

    private static void parseOnGroupUpdate(Context context, OnParseUpdateListener listener, JSONObject json) throws JSONException {
        String groupId = json.getString(JsonCreater.GROUP_ID);

        BackendHandler.getInstance(context).onGroupUpdate(groupId);
    }

    private static void parseOnTaskSeenInfoChange(Context context, JSONObject json) throws JSONException {
        String groupId = json.getString(JsonCreater.GROUP_ID);
        String taskId = json.getString(JsonCreater.TASK_ID);
        long updateTime = json.getLong(JsonCreater.TASK_DELIVER_TIME);
        String seenById = json.getString(JsonCreater.TASK_SEEN_BY_ID);
        int status = json.getInt(JsonCreater.TASK_SEEN_STATUS);
        SeenBy seenBy = new SeenBy(seenById, status, updateTime);

        System.out.println("parse on task info change groupId = " + groupId + ",tId = " + taskId);

        BackendHandler.getInstance(context).updateTaskSeenInfo(taskId, seenBy);
    }

    private static void parseOnGroupTaskDelete(Context context, JSONObject json) throws JSONException {
        String taskId = json.getString(JsonCreater.TASK_ID);

        BackendHandler.getInstance(context).deleteTaskFromLocalDb(taskId);
    }

    private static void parseOnTaskUpdated(Context context, OnParseUpdateListener listener, JSONObject json) throws JSONException {
        String groupId = json.getString(JsonCreater.GROUP_ID);
        String taskId = json.getString(JsonCreater.TASK_ID);

        updateGroupUpdateCounter(listener, groupId);
        BackendHandler.getInstance(context).onTaskUpdate(taskId);
    }

    private static void parseOnTaskCreated(Context context, OnParseUpdateListener listener, JSONObject json) throws JSONException {

        String groupId = json.getString(JsonCreater.GROUP_ID);
        String taskId = json.getString(JsonCreater.TASK_ID);

        System.out.println("gId = " + groupId + ", tId = " + taskId);
        updateGroupUpdateCounter(listener, groupId);
        BackendHandler.getInstance(context).onNewTaskCreate(groupId, taskId);
    }

    /**
     * update counter on group to show new new updates.
     * @param listener
     * @param groupId
     */
    private static void updateGroupUpdateCounter(OnParseUpdateListener listener, String groupId) {
        if (listener != null)
            listener.updateGroupUpdates(groupId);
    }

    private static void parseOnGroupCreated(Context context, OnParseUpdateListener listener, JSONObject json) throws JSONException {
        String groupId = json.getString(JsonCreater.GROUP_ID);

        BackendHandler.getInstance(context).onNewGroupCreate(listener, groupId);
    }

    public interface OnParseUpdateListener {

        void onGroupMemberAddedToGroup();

        void onGroupLoaded(Group group);

        void updateGroupUpdates(String groupId);
    }
}