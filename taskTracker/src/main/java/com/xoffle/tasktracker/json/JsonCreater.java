package com.xoffle.tasktracker.json;

import com.inkling.leaderboard.AppGroup;
import com.xoffle.tasktracker.model.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ravi on 11/4/15.
 */
public class JsonCreater {

    public static final String TYPE = "type";

    public static final String ALERT = "alert";
    public static final String GROUP_ID = "groupId";
    public static final String TASK_ID = "taskCreatersId";
    public static final String TASK_SEEN_STATUS = "task_seen_status";
    public static final String TASK_DELIVER_TIME = "task_deliver_time";
    public static final String TASK_SEEN_BY_ID = "task_seen_by_id";

    public static final int TYPE_TASK_CREATED = 1;
    public static final int TYPE_GROUND_CREATED = 2;
    public static final int TYPE_TASK_UPDATED = 3;
    public static final int TYPE_GROUP_UPDATED = 4;
    public static final int TYPE_TASK_DELETED = 5;
    public static final int TYPE_TASK_USER_INFO_UPDATE = 6;

    public static JSONObject createJsonOnTaskCreate(String groupId, String taskId, String msg) {
        JSONObject json = new JSONObject();
        try {
            json.put(ALERT, msg);
            json.put(TYPE, TYPE_TASK_CREATED);
            json.put(GROUP_ID, groupId);
            json.put(TASK_ID, taskId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static JSONObject createJsonForGroupCreate(AppGroup appGroup) {
        JSONObject json = new JSONObject();
        try {
            json.put(ALERT, "You are added in " + appGroup.getGroupName() + " group by " + DataStorage.getInstance(null).getDisplayName());
            json.put(GROUP_ID, appGroup.getObjectId());
            json.put(TYPE, TYPE_GROUND_CREATED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static JSONObject createJsonForTaskUpdate(String groupId, String taskId, boolean showAlert) {
        JSONObject json = new JSONObject();
        try {
            if (showAlert) {
                json.put(ALERT, "Task is updated by " + DataStorage.getInstance(null).getDisplayName());
            }
            json.put(GROUP_ID, groupId);
            json.put(TASK_ID, taskId);
            json.put(TYPE, TYPE_TASK_UPDATED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static JSONObject createJsonForTaskDelete(String taskId) {
        JSONObject json = new JSONObject();
        try {
            json.put(ALERT, "Task is deleted by " + DataStorage.getInstance(null).getDisplayName());
            json.put(TASK_ID, taskId);
            json.put(TYPE, TYPE_TASK_DELETED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static JSONObject createJsonForUpdate(String groupId, boolean shouldNotify) {
        JSONObject json = new JSONObject();
        try {
            if (shouldNotify) {
                json.put(ALERT, "Group is updated by " + DataStorage.getInstance(null).getDisplayName());
            }

            json.put(GROUP_ID, groupId);
            json.put(TYPE, TYPE_GROUP_UPDATED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

//    public static JSONObject createJsonForTaskUserInfoUpdate(String groupId, String taskId, int status, String seenById) {
//        JSONObject json = new JSONObject();
//        try {
//            json.put(GROUP_ID, groupId);
//            json.put(TASK_SEEN_BY_ID, seenById);
//            json.put(TYPE, TYPE_TASK_USER_INFO_UPDATE);
//            json.put(TASK_SEEN_STATUS, status);
//            json.put(TASK_DELIVER_TIME, System.currentTimeMillis());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return json;
//    }
}