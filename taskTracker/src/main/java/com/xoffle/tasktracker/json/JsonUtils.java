package com.xoffle.tasktracker.json;

import com.inkling.leaderboard.SeenBy;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ravi on 9/6/15.
 */
public class JsonUtils {

    public static final String USER_ID = "user_id";
    public static final String STATUS = "status";
    public static final String WHEN = "when";

    public static String seenByToJsonString(SeenBy seenBy) {
        JSONObject jsonObject  = new JSONObject();
        try {
        jsonObject.put(USER_ID,seenBy.getUserId());
        jsonObject.put(STATUS,seenBy.getStatus());
            jsonObject.put(WHEN,seenBy.getWhen());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public static SeenBy stringToSeenBy(String text)
    {
        SeenBy seenBy = new SeenBy();
        try {
            JSONObject jsonObject  =new JSONObject(text);
            seenBy.setUserId(jsonObject.getString(USER_ID));
            seenBy.setStatus(jsonObject.getInt(STATUS));
            seenBy.setWhen(jsonObject.getLong(WHEN));

            return seenBy;
        } catch (JSONException e) {
            return null;
        }
    }
}