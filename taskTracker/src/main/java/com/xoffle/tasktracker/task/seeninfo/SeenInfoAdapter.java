package com.xoffle.tasktracker.task.seeninfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.beans.SeenInfoBean;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.util.DateUtil;

/**
 * Created by ravi on 12/5/15.
 */
public class SeenInfoAdapter extends ArrayAdapter<SeenInfoBean> {
    private final LayoutInflater inflater;
    private final Context context;

    public SeenInfoAdapter(Context context, int resource) {
        super(context, resource);

        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.seen_info_view, parent, false);
        }

        SeenInfoBean seenInfoBean = getItem(position);

        ImageView userImage = (ImageView) convertView.findViewById(R.id.seenInfoViewUserImage);
        TextView userNameTv = (TextView) convertView.findViewById(R.id.seenInfoViewUsersName);
        TextView dateTv = (TextView) convertView.findViewById(R.id.seenInfoViewDate);

        setData(userImage, userNameTv, dateTv, seenInfoBean);

        return convertView;
    }

    private void setData(ImageView userImage, TextView userNameTV, TextView dateTV, SeenInfoBean seenInfoBean) {
        setImage(userImage);
        setUsername(seenInfoBean.getSeenById(), userNameTV);
        setDate(seenInfoBean.getUpdateDate(), dateTV);
    }

    private void setDate(long updateDate, TextView dateTV) {
        String dateFormat = DateUtil.format(updateDate, DateUtil.FORMAT_1);
        dateTV.setText(dateFormat);
    }

    private void setUsername(String seenById, TextView userNameTextView) {
        String userName = BackendHandler.getInstance(context).getUsername(seenById);
        if (userName != null)
            userNameTextView.setText(userName);
    }

    private void setImage(ImageView userImage) {

    }
}
