package com.xoffle.tasktracker.task;

/**
 * Created by ravi on 10/6/15.
 */
public interface TaskCreateUpdateListener {
    public void onTaskCreateButtonClicked(String subject, String description);

    void onTaskUpdateButtonClicked(String subject, String description);
}
