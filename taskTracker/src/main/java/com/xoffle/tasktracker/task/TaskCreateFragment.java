package com.xoffle.tasktracker.task;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.view.MyFragment;

/**
 * Created by ravi on 9/6/15.
 */
public class TaskCreateFragment extends MyFragment implements View.OnClickListener {

    private static final String IS_IN_EDIT_MODE = "IS_IN_EDIT_MODE";
    private static final String SUBJECT = "SUBJECT";
    private static final String DESCRIPTION = "DESCRIPTION";

    private TaskCreateUpdateListener taskCreateListener;
    private EditText subjectEt;
    private EditText descriptionEt;
    private Button createButton;
    private Button cancelButton;
    private boolean isInEditMode;

    public static TaskCreateFragment newInstance() {
        TaskCreateFragment taskCreateFragment = new TaskCreateFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_IN_EDIT_MODE, false);
        taskCreateFragment.setArguments(bundle);
        return taskCreateFragment;
    }

    public static TaskCreateFragment newInstance(String subject, String description) {
        TaskCreateFragment taskCreateFragment = new TaskCreateFragment();

        Bundle bundle = new Bundle();

        bundle.putString(SUBJECT, subject);
        bundle.putString(DESCRIPTION, description);
        bundle.putBoolean(IS_IN_EDIT_MODE, true);
        taskCreateFragment.setArguments(bundle);

        return taskCreateFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        isInEditMode = bundle.getBoolean(IS_IN_EDIT_MODE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_task_fragment, container, false);

        initToolbar(view, R.id.createTaskFragmentToolbarSimple, "Create Task");

        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        findViewByIds(view);

        if (isInEditMode) {
            setTaskText();
        }

        setClickListener();

        return view;
    }

    private void setTaskText() {
        Bundle bundle = getArguments();
        String subject = bundle.getString(SUBJECT);
        String description = bundle.getString(DESCRIPTION);
        subjectEt.setText(subject);

        subjectEt.setOnTouchListener(null);

        if (!Utils.isNull(description)) {
            descriptionEt.setText(description);
        }

        createButton.setText(getActivity().getString(R.string.update));

        toolbar.setTitle("Edit Task");
    }

    private void setClickListener() {
        createButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
    }

    private void findViewByIds(View view) {
        subjectEt = (EditText) view.findViewById(R.id.createTaskFragmentSubjectEt);
        descriptionEt = (EditText) view.findViewById(R.id.createTaskFragmentEtDescription);

        createButton = (Button) view.findViewById(R.id.createTaskFragmentButtonCreate);
        cancelButton = (Button) view.findViewById(R.id.createTaskFragmentButtonCancel);
    }

    public void setTaskCreateListener(TaskCreateUpdateListener taskCreateListener) {
        this.taskCreateListener = taskCreateListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createTaskFragmentButtonCreate:
                createUpdateTask();
                break;
            case R.id.createTaskFragmentButtonCancel:
                cancelTask();
                break;
            default:
        }
    }

    private void cancelTask() {
        getFragmentManager().popBackStackImmediate();
    }

    private void createUpdateTask() {
        String subject = subjectEt.getText().toString();
        String description = descriptionEt.getText().toString();

        if (Utils.isNull(subject)) {
            Toast.makeText(getActivity(), "Subject could not be null", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isInEditMode) {
            taskCreateListener.onTaskUpdateButtonClicked(subject, description);
        } else {
            taskCreateListener.onTaskCreateButtonClicked(subject, description);
        }
        getFragmentManager().popBackStackImmediate();
    }
}