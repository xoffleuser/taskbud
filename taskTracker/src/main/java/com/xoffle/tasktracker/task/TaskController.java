package com.xoffle.tasktracker.task;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.inkling.leaderboard.AppGroupTask;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.SeenBy;
import com.inkling.leaderboard.Task;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.controller.Constructor;
import com.xoffle.tasktracker.custom.LoadingDialog;
import com.xoffle.tasktracker.db.DBConstants;
import com.xoffle.tasktracker.dialog.InputDialogFragment;
import com.xoffle.tasktracker.group.GroupController;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.json.JsonUtils;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Utils;

import java.util.Date;
import java.util.List;

/**
 * Created by ravi on 19/4/15.
 */
public class TaskController implements TaskCreateUpdateListener, OnTaskUpdateListener {
    public static final int UPDATE_MESSAGE = 1;
    public static final int UPDATE_REMARK = 2;
    public static final int TASK_REMOVE = 3;
    public static final int TASK_DELETE = 4;
    private final Fragment fragment;
    private OnTaskEventListener taskEventListener;
    private Activity activity;
    private LoadingDialog loadingDialog;
    private Task task;

    public TaskController(Fragment fragment) {
        this.fragment = fragment;
        this.activity = fragment.getActivity();
        loadingDialog = new LoadingDialog();
    }

    public static boolean isTaskAckdBy(Task task, String userId) {
        return userId.equals(task.getAckCnfID());
    }

    public static boolean isTaskAckdByMe(Task task) {
        return isTaskAckdBy(task, DataStorage.getInstance(null).getUserId());
    }

    /**
     * remove the task from local database not from server
     * @param task
     */
    public void removeTask(Task task) {
        BackendHandler.getInstance(activity).removeTask(task);
    }

    /**
     * generate a popup for creating task
     */
    public void createNewTask() {

        TaskCreateFragment taskFragment = TaskCreateFragment.newInstance();
        taskFragment.setTaskCreateListener(this);
        ((MainActivity) activity).addFragment(taskFragment, true);
    }

    /**
     * create new task through alert popup
     */
    public void createTask(String subject, String description) {
        loadingDialog.show(fragment.getFragmentManager());

        DataStorage store = DataStorage.getInstance(null);
        final String groupId = GroupController.getInstance().getCurrentGroup().getGroupId();

        SeenBy seenBy = new SeenBy(store.getUserId(), DBConstants.TASK_SEEN, System.currentTimeMillis());

        BackendHandler.getInstance(activity).createTask(new Task(groupId, store.getUserId(), subject, description, System.currentTimeMillis(), JsonUtils.seenByToJsonString(seenBy)), new Constructor.OnTaskCreateListener() {
            @Override
            public void onTaskCreate(Task task) {

                if (loadingDialog != null && loadingDialog.isVisible()) {
                    loadingDialog.dismiss();
                }

                if (task == null) {
                    Utils.showToast(activity, "Task not created, error occurred.");
                } else {
                    BackendHandler.getInstance(activity).updateGroupUpdateTime(groupId);
                    taskEventListener.onTaskLoaded(task);
                }
            }
        });
    }

    public void editTask(final Task task, final int updateType, FragmentManager fragmentManager, final OnTaskEventListener onTaskEventListener) {

        if (updateType == TaskController.UPDATE_MESSAGE) {
            this.task = task;
            TaskCreateFragment taskCreateFragment = TaskCreateFragment.newInstance(task.getSubject(), task.getDescription());
            ((MainActivity) activity).addFragment(taskCreateFragment, true);
            taskCreateFragment.setTaskCreateListener(this);
            return;
        }

        final InputDialogFragment dialog = InputDialogFragment.newInstance(0);

        Bundle bundle = new Bundle();

        String title = "Title";
        switch (updateType) {
            case UPDATE_MESSAGE:
                title = "Edit message";
                bundle.putString(Constant.UPDATE_MSG, task.getDescription());
                break;
            case UPDATE_REMARK:
                title = "Edit remark";
                bundle.putString(Constant.UPDATE_REMARK, task.getRemark());
                break;
        }

        dialog.setArguments(bundle);

        dialog.init(title, "Edit", "Cancel", "");
        dialog.setDialogInputListener(new InputDialogFragment.OnDialogInputListener() {
            @Override
            public void onButton1Click(String txt) {
                editTask(task, txt, updateType, onTaskEventListener);
                dialog.dismiss();
            }

            @Override
            public void onButton2Click(String txt) {
                dialog.dismiss();
            }
        });

        dialog.showDialog(fragmentManager);
//        dialog.setMessage(task.msg);
    }

    private void editTask(Task task, String txt, int updateType, OnTaskEventListener onTaskEventListener) {
        switch (updateType) {
            case UPDATE_MESSAGE:
                task.setDescription(txt);
                break;
            case UPDATE_REMARK:
                task.setRemark(txt);
        }
        BackendHandler.getInstance(activity).handleBackendOnTaskUpdate(task, this);

        onTaskEventListener.refreshTaskList();
    }

    public void deleteTask(Task task) {
        BackendHandler.getInstance(activity).deleteTask(task, this);
    }

    public boolean isTaskConfirmed(Task task) {
        return BackendHandler.getInstance(activity).isTaskConfirmed(task);
    }

    @Override
    public void onTaskCreateButtonClicked(String subject, String description) {
        createTask(subject, description);
    }

    @Override
    public void onTaskUpdateButtonClicked(String subject, String description) {
        loadingDialog.show(fragment.getFragmentManager());
        task.setSubject(subject);
        task.setDescription(description);

        BackendHandler.getInstance(activity).handleBackendOnTaskUpdate(task, this);
    }

    public void setOnTaskEventListener(OnTaskEventListener taskEventListener) {
        this.taskEventListener = taskEventListener;
    }

    @Override
    public void onTaskDelete(Task deletedTask) {
        taskEventListener.removeTask(deletedTask);
        BackendHandler.getInstance(activity).updateGroupUpdateTime(GroupController.getInstance().getCurrentGroup().getGroupId());
    }

    @Override
    public void onTaskUpdate(Task updatedTask) {
        Utils.dismissDialog(loadingDialog);
//        if (loadingDialog != null && loadingDialog.isVisible()) {
//            loadingDialog.dismiss();
//        }

        if (updatedTask == null) {
            return;
        }

        BackendHandler.getInstance(activity).updateGroupUpdateTime(GroupController.getInstance().getCurrentGroup().getGroupId());
        taskEventListener.refreshTaskList();
        Utils.showToast(activity, activity.getString(R.string.task_update_msg));
    }

    public void acknowledgeTask(String objectId, final OnTaskEventListener onTaskEventListener) {
        if (!Utils.isNetworkAvailble(activity)) {
            Utils.showToast(activity, activity.getString(R.string.no_internet_connection));
            return;
        }

        Utils.showLoadingDialog(loadingDialog, fragment.getFragmentManager());

        LeaderBoard.loadGroupTask(objectId, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask object, boolean isLoaded) {

                if (object != null) {
                    acknowledge(new Task(object), onTaskEventListener);
                } else {
                    Utils.dismissDialog(loadingDialog);
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    private void acknowledge(Task task, OnTaskEventListener onTaskEventListener) {
        if (!task.isAckd()) {
            task.setAckd(true);
            task.setAckCnfID(DataStorage.getInstance(activity).getUserId());
            task.setAckTime(System.currentTimeMillis());
            task.setUpdatedAt(new Date(System.currentTimeMillis()));

            onTaskEventListener.refreshTaskList();
            BackendHandler.getInstance(activity).handleBackendOnTaskUpdate(task, this);
        } else {
            if (isTaskConfirmed(task)) {
                Utils.showToast(activity, "Can not remove task acknowledge, Because Task is already Confirmed..");
                Utils.dismissDialog(loadingDialog);
                return;
            } else {
                if (isTaskAckdByMe(task)) {
                    task.setAckd(false);
                    task.setAckCnfID("");

                    onTaskEventListener.refreshTaskList();
                    BackendHandler.getInstance(activity).handleBackendOnTaskUpdate(task, this);
                } else {
                    Utils.showToast(activity, activity.getString(R.string.ack_reset_error_msg));
                    Utils.dismissDialog(loadingDialog);
                }
            }
        }
    }

    public void confirmTask(String objectId, final OnTaskEventListener onTaskEventListener) {
        if (!Utils.isNetworkAvailble(activity)) {
            Utils.showToast(activity, activity.getString(R.string.no_internet_connection));
            return;
        }

        Utils.showLoadingDialog(loadingDialog, fragment.getFragmentManager());

        LeaderBoard.loadGroupTask(objectId, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask object, boolean isLoaded) {

                if (object != null) {
                    confirm(new Task(object), onTaskEventListener);
                } else {
                    Utils.dismissDialog(loadingDialog);
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    private void confirm(Task task, OnTaskEventListener onTaskEventListener) {
        if (task.isCnfd()) {
            Utils.showToast(activity, activity.getString(R.string.already_confirmed));
            Utils.dismissDialog(loadingDialog);
            return;
        }

        if (isAcknowledgedByMe(task)) {
            task.setCnfd(true);
            task.setAckCnfID(DataStorage.getInstance(activity).getUserId());

            onTaskEventListener.refreshTaskList();
            BackendHandler.getInstance(activity).handleBackendOnTaskUpdate(task, this);
        } else {
            Utils.showToast(activity, activity.getString(R.string.confirm_error_msg));
            Utils.dismissDialog(loadingDialog);
        }
    }

    private boolean isAcknowledgedByMe(Task task) {
        return DataStorage.getInstance(null).getUserId().equals(task.getAckCnfID());
    }
}