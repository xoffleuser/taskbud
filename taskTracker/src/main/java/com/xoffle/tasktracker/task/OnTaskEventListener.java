package com.xoffle.tasktracker.task;

import com.inkling.leaderboard.Task;

/**
 * Created by ravi on 16/4/15.
 */
public interface OnTaskEventListener {
    void onAckButtonClick(Task task);
    void onConfirmButtonClick(Task task);

    void onTaskLoaded(Task task);

    void removeTask(Task deletedTask);

    void refreshTaskList();
}
