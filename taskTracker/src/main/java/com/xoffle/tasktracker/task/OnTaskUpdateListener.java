package com.xoffle.tasktracker.task;

import com.inkling.leaderboard.Task;

/**
 * Created by ravi on 11/6/15.
 */
public interface OnTaskUpdateListener {

    void onTaskDelete(Task deletedTask);

    void onTaskUpdate(Task updatedTask);
}