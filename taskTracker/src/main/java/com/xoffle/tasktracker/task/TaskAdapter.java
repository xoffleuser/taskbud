package com.xoffle.tasktracker.task;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inkling.leaderboard.Task;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.util.DateUtil;
import com.xoffle.tasktracker.util.Utils;

import java.util.ArrayList;

/**
 * Created by ravi on 9/4/15.
 */
public class TaskAdapter extends ArrayAdapter<Task> {

    private final LayoutInflater inflater;
    private final OnTaskEventListener onTaskEventListener;
    ArrayList<Task> allTask = new ArrayList<>();
    private int filterBy;
    private int sortBy;

    public TaskAdapter(Context context, int resource, OnTaskEventListener onTaskEventListener) {
        super(context, resource);
        inflater = LayoutInflater.from(context);

        this.onTaskEventListener = onTaskEventListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Task taskView = getItem(position);

        TaskHolder taskHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.task_view, parent, false);
            taskHolder = new TaskHolder(getContext(), convertView, taskView, onTaskEventListener);
            convertView.setTag(taskHolder);

        } else {
            taskHolder = (TaskHolder) convertView.getTag();
            taskHolder.setData(getContext(), taskView, onTaskEventListener);
        }

        return convertView;
    }

    public void addTask(Task task) {
        add(task);

        allTask.add(task);
    }

    public void removeTask(String objectId) {
        for (int i = getCount() - 1; i >= 0; i--) {
            if (getItem(i).getObjectId().equals(objectId)) {
                remove(getItem(i));
                break;
            }
        }
    }

    public void sort(int sortBy) {
        this.sortBy = sortBy;
        sort(new TaskComparator(sortBy));
        notifyDataSetChanged();
    }

    public void filterBy(int filterBy) {
        this.filterBy = filterBy;

        filterData();

        notifyDataSetChanged();
    }

    private void filterData() {

        clear();

        if (filterBy == Constant.TaskConstant.FILTER_ALL) {
            addAll(allTask);
            return;
        }

        int size = allTask.size();
        for (int i = 0; i < size; i++) {

            Task task = allTask.get(i);
            switch (filterBy) {
                case Constant.TaskConstant.FILTER_CNFD_AND_ACKD:
                    if (task.isAckd() || task.isCnfd()) {
                        add(task);
                    }
                    break;
                case Constant.TaskConstant.FILTER_ACTIVE:
                    if (!task.isCnfd()) {
                        add(task);
                    }
                    break;
                case Constant.TaskConstant.FILTER_CONFIRMED:
                    if (task.isCnfd()) {
                        add(task);
                    }
                    break;
                case Constant.TaskConstant.FILTER_ACKNOWLEDGED_ONLY:
                    if (task.isAckd() && !task.isCnfd()) {
                        add(task);
                    }
                    break;
                case Constant.TaskConstant.FILTER_NO_ACTIVITY:
                    if (!task.isAckd()) {
                        add(task);
                    }
                    break;
            }
        }
    }

    public int getFilterBy() {
        return filterBy;
    }

    public int getSortBy() {
        return sortBy;
    }

    private static class TaskHolder implements View.OnLongClickListener {
        private Button ackButton;
        private Button confirmButton;

        private TextView taskTitle;
        private TextView dateTextView;
        private TextView taskUpdaterTextView;
        private TextView taskMsgTextView;
        private TextView remarkTextView;
        private TextView taskCreatedByTextView;

        private Task task;
        private OnTaskEventListener taskEventListener;

        public TaskHolder(Context context, View convertView, Task task, OnTaskEventListener taskEventListener) {
            findViewByIds(convertView);
            setData(context, task, taskEventListener);
        }

        private void setData(Context context, Task task, OnTaskEventListener taskEventListener) {
            this.task = task;
            this.taskEventListener = taskEventListener;


            String cnfAckBy = null;
            if (task.getAckCnfID() != null) {
                cnfAckBy = BackendHandler.getInstance(context).getUsername(task.getAckCnfID());
            }

            String createdBy = BackendHandler.getInstance(context).getDisplayName(task.getTaskCreaterId());

            dateTextView.setText(DateUtil.format(task.getCreatedAt(), DateUtil.FORMAT_1));

            taskTitle.setText(task.getSubject());
            taskCreatedByTextView.setText("- " + createdBy);

            if (task.isAckd()) {
                taskUpdaterTextView.setVisibility(View.VISIBLE);
                taskUpdaterTextView.setText(context.getString(R.string.ackd_by) + cnfAckBy + context.getString(R.string.on) + DateUtil.format(task.getUpdatedAt(), DateUtil.FORMAT_1));
                taskUpdaterTextView.setTextColor(context.getResources().getColor(R.color.grey));
                ackButton.setText(context.getString(R.string.acknowledged));
                ackButton.setTextColor(context.getResources().getColor(R.color.blue1));
            } else {
                taskUpdaterTextView.setText("");
                taskUpdaterTextView.setVisibility(View.GONE);
                ackButton.setTextColor(context.getResources().getColor(R.color.grey));
                ackButton.setText(context.getString(R.string.acknowledge));
            }
            if (task.isCnfd()) {
                taskUpdaterTextView.setVisibility(View.VISIBLE);
                taskUpdaterTextView.setText(context.getString(R.string.confirmed_by) + cnfAckBy + context.getString(R.string.on) + DateUtil.format(task.getUpdatedAt(), DateUtil.FORMAT_1));
                taskUpdaterTextView.setTextColor(context.getResources().getColor(R.color.grey));
                confirmButton.setText(context.getString(R.string.confirmed));
                confirmButton.setTextColor(context.getResources().getColor(R.color.blue1));
            } else {
                confirmButton.setTextColor(context.getResources().getColor(R.color.grey));
                confirmButton.setText(context.getString(R.string.confirm));
            }

            if (!Utils.isNull(task.getDescription())) {
//            taskMsgTextView.setVisibility(View.VISIBLE);
                taskMsgTextView.setText(task.getDescription());
            } else {
                taskMsgTextView.setText(context.getString(R.string.no_description));
//            taskMsgTextView.setVisibility(View.GONE);
            }

            if (!Utils.isNull(task.getRemark())) {
                remarkTextView.setVisibility(View.VISIBLE);
                remarkTextView.setText(Html.fromHtml("<b>Remark</b> : " + task.getRemark()));
            } else {
                remarkTextView.setVisibility(View.GONE);
            }

            setListener();
        }

        private void setListener() {
            ackButton.setOnLongClickListener(this);
            confirmButton.setOnLongClickListener(this);
        }

        private void findViewByIds(View convertView) {
            LinearLayout lLayout = (LinearLayout) convertView.findViewById(R.id.task_view_ll);

            ackButton = (Button) lLayout.findViewById(R.id.task_view_ll_button_ack);
            confirmButton = (Button) lLayout.findViewById(R.id.task_view_ll_button_confirm);

            taskTitle = ((TextView) convertView.findViewById(R.id.taskViewTaskTitle));
            dateTextView = ((TextView) convertView.findViewById(R.id.task_view_date));
            taskUpdaterTextView = ((TextView) convertView.findViewById(R.id.task_view_acknowledgedby));
            taskMsgTextView = ((TextView) convertView.findViewById(R.id.task_view_text_view_msg));
            remarkTextView = ((TextView) convertView.findViewById(R.id.task_view_text_view_remark));

            taskCreatedByTextView = (TextView) convertView.findViewById(R.id.taskViewCreatedBy);
        }

        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()) {
                case R.id.task_view_ll_button_ack:
                    taskEventListener.onAckButtonClick(task);
                    return true;

                case R.id.task_view_ll_button_confirm:
                    taskEventListener.onConfirmButtonClick(task);
                    return true;
            }

            return false;
        }
    }

    /**
     * Created by ravi on 21/6/15.
     */
    class TaskComparator implements java.util.Comparator<Task> {

        private final int orderBy;

        public TaskComparator(int orderBy) {
            this.orderBy = orderBy;
        }

        @Override
        public int compare(Task lhs, Task rhs) {

            switch (orderBy) {
                case Constant.TaskConstant.SHOW_RENCET:
                    return lhs.getUpdatedAt().getTime() > rhs.getUpdatedAt().getTime() ? -1 : 1;

                case Constant.TaskConstant.SHOW_CREATE_AT:
                    return lhs.getCreatedAt().getTime() > rhs.getCreatedAt().getTime() ? -1 : 1;

                default:
                    return lhs.getCreatedAt().getTime() > rhs.getCreatedAt().getTime() ? -1 : 1;
            }
        }
    }
}