package com.xoffle.tasktracker.task.seeninfo;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkling.leaderboard.AppGroupTask;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.SeenBy;
import com.inkling.leaderboard.Task;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.db.DBConstants;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.group.GroupController;
import com.xoffle.tasktracker.group.GroupMember;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravi on 12/5/15.
 */
public class SeenInfoDialog extends DialogFragment {

    private static final String TAG = "SeenInfoFragment";
    private static final String TASK_KEY = "TASK_KEY";
    private Group currentGroup = null;
//    private Task task = null;

    private TextView readByHeaderTextView;
    private TextView readByFooterTextView;
    private TextView deliveredToHeaderTextView;
    private TextView deliveredToFooterTextView;
    private LinearLayout readByLinearLayout;
    private LinearLayout deliveredToLinearLayout;

    public static SeenInfoDialog newInstance(Task task) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(TASK_KEY, task);

        SeenInfoDialog seenInfoDialog = new SeenInfoDialog();
        seenInfoDialog.setArguments(bundle);

        return seenInfoDialog;
    }

//    public SeenInfoDialog(Task task) {
//        currentGroup = GroupController.getInstance().getCurrentGroup();
//        this.task = task;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.MyTheme);

        currentGroup = GroupController.getInstance().getCurrentGroup();
//        if (getArguments() != null) {
//            Task task = (Task) getArguments().getSerializable(TASK_KEY);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_seen_info, container, false);

        TextView creatersNameTv = (TextView) view.findViewById(R.id.seenInfoFragmentTextViewTaskCreater);
        TextView dateTv = (TextView) view.findViewById(R.id.seenInfoFragmentTextViewDate);
        TextView messageTv = (TextView) view.findViewById(R.id.seenInfoFragmentTextViewTaskMessage);
        TextView remarkTv = (TextView) view.findViewById(R.id.seenInfoFragmentTextViewRemark);

        Task task = (Task) getArguments().getSerializable(TASK_KEY);

        creatersNameTv.setText(BackendHandler.getInstance(getActivity()).getDisplayName(task.getTaskCreaterId()));
        dateTv.setText(DateUtil.format(task.getCreatedAt(), DateUtil.FORMAT_1));
        messageTv.setText(Html.fromHtml("<b><i>Task</i></b> : " + task.getDescription()));
        if (task.getRemark() != null) {
            remarkTv.setText(Html.fromHtml("<b><i>Remark</i></b> : " + task.getRemark()));
        }

//        setAdapter(view, inflater);

        setItems(view, inflater, task);

        loadSeenInfo(task.getObjectId(), inflater);

        return view;
    }

    private void loadSeenInfo(String taskId, final LayoutInflater inflater) {
        LeaderBoard.loadGroupTask(taskId, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask appTask, boolean isLoaded) {
                if (appTask != null) {
                    resetLayout(new Task(appTask), inflater);
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    private void resetLayout(Task task, LayoutInflater inflater) {
        removeLayoutViews();
        DBController.getInstance(getActivity()).updateSeenInfo(task.getObjectId(), task.getSeenByList());
        setLayoutData(inflater, task);
    }

    private void removeLayoutViews() {
        readByLinearLayout.removeAllViews();
        deliveredToLinearLayout.removeAllViews();
    }

    private void setItems(View view, LayoutInflater inflater, Task task) {

        findAllViewByIds(view);

        readByHeaderTextView.setText(getActivity().getText(R.string.read_by));
        deliveredToHeaderTextView.setText(getActivity().getText(R.string.delivered_to));

        setLayoutData(inflater, task);

    }

    private void setLayoutData(LayoutInflater inflater, Task task) {
        ArrayList<GroupMember> groupMembers = BackendHandler.getInstance(getActivity()).getGroupMembers(currentGroup.getGroupId(), currentGroup.getAdminId());

        String userId = DataStorage.getInstance(getActivity()).getUserId();
        int readBy = 0, deliveredTo = 0;
        int total = groupMembers.size();
//        int total = 14;
//        int status = 1;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 2, 0, 0);

        for (int i = total - 1; i >= 0; i--) {

            String memberId = groupMembers.get(i).getMemberId();
//            String memberId = "123";
//            status = 3 - status;

            if (!userId.equals(memberId)) {
                SeenBy seenBy = BackendHandler.getInstance(getActivity()).getSeenInfoFor(task.getObjectId(), memberId);
//                seenInfoBean = new SeenInfoBean("id", status, System.currentTimeMillis());
                if (seenBy == null) {
                    continue;
                }
                if (seenBy.getStatus() == DBConstants.TASK_DELIVERED) {
                    deliveredTo++;
                    addView(deliveredToLinearLayout, seenBy, inflater, layoutParams);
//                    deliveredToAdapter.add(seenInfoBean);
                } else if (seenBy.getStatus() == DBConstants.TASK_SEEN) {
                    readBy++;
                    deliveredTo++;
                    addView(readByLinearLayout, seenBy, inflater, layoutParams);
//                    readByAdapter.add(seenInfoBean);
                }
            }
        }
        int readRemaining = total - readBy - 1;
        int deliverRemaining = total - deliveredTo - 1;

        if (readRemaining > 0) {
            readByFooterTextView.setText(readRemaining + " Remaining");
            readByFooterTextView.setVisibility(View.VISIBLE);
        } else {
            readByFooterTextView.setVisibility(View.GONE);
        }

        if (deliverRemaining > 0) {
            deliveredToFooterTextView.setText(deliverRemaining + " Remaining");
            deliveredToFooterTextView.setVisibility(View.VISIBLE);
        } else {
            deliveredToFooterTextView.setVisibility(View.GONE);
        }
    }

    private void addView(LinearLayout readByLinearLayout, SeenBy seenBy, LayoutInflater inflater, LinearLayout.LayoutParams layoutParams) {
        RelativeLayout viewLayout = (RelativeLayout) inflater.inflate(R.layout.seen_info_view, null, false);
        TextView dateTextView = (TextView) viewLayout.findViewById(R.id.seenInfoViewDate);
        TextView usernameTextView = (TextView) viewLayout.findViewById(R.id.seenInfoViewUsersName);
        ImageView profilePic = (ImageView) viewLayout.findViewById(R.id.seenInfoViewUserImage);

        setData(profilePic, usernameTextView, dateTextView, seenBy);

        readByLinearLayout.addView(viewLayout, layoutParams);

    }

    private void setData(ImageView userImage, TextView userNameTV, TextView dateTV, SeenBy seenBy) {
        setImage(userImage);
        setUsername(seenBy.getUserId(), userNameTV);
        setDate(seenBy.getWhen(), dateTV);
    }

    private void setDate(long updateDate, TextView dateTV) {
        String dateFormat = DateUtil.format(updateDate, DateUtil.FORMAT_1);
        dateTV.setText(dateFormat);
    }

    private void setUsername(String seenById, TextView userNameTextView) {
        String userName = BackendHandler.getInstance(getActivity()).getUsername(seenById);
        if (userName != null) {
            userNameTextView.setText(userName);
        }
    }

    private void setImage(ImageView userImage) {

    }

    private void findAllViewByIds(View view) {
        RelativeLayout readByHeader = (RelativeLayout) view.findViewById(R.id.readByHeader);
        readByHeaderTextView = (TextView) readByHeader.findViewById(R.id.seenInfoHeaderTextViewTitle);
        readByFooterTextView = (TextView) view.findViewById(R.id.seenByFooter);

        RelativeLayout deliveredToHeader = (RelativeLayout) view.findViewById(R.id.deliveredToHeader);
        deliveredToHeaderTextView = (TextView) deliveredToHeader.findViewById(R.id.seenInfoHeaderTextViewTitle);
        deliveredToFooterTextView = (TextView) view.findViewById(R.id.deliveredToFooter);

        readByLinearLayout = (LinearLayout) view.findViewById(R.id.linearLayoutReadByViews);
        deliveredToLinearLayout = (LinearLayout) view.findViewById(R.id.linearLayoutDeliveredToViews);
    }

    public void show(FragmentManager fragmentManager) {
        Fragment preFragment = fragmentManager.findFragmentByTag(TAG);
        if (preFragment != null) {
            fragmentManager.beginTransaction().remove(preFragment);
        }

        show(fragmentManager, TAG);
    }
}