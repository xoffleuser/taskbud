package com.xoffle.tasktracker.task;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.inkling.leaderboard.AppGroupTask;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.Task;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.custom.CustomActionProvider;
import com.xoffle.tasktracker.custom.TaskFilterActionProviderListener;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.group.GroupController;
import com.xoffle.tasktracker.group.GroupDetailFragment;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.task.seeninfo.SeenInfoDialog;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.view.MyFragment;

import java.util.Date;
import java.util.List;

/**
 * Created by ravi on 17/5/15.
 */
public class TaskListFragment extends MyFragment implements OnTaskEventListener, OnTaskUpdateListener {

    private TaskController taskController;
    private TaskAdapter taskAdapter;
    private CustomActionProvider mCustomActionProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_task, container, false);

        setToolbar(view);

        taskController = new TaskController(this);
        taskController.setOnTaskEventListener(this);

        initializeListView(view);

        updateSeenByInfo();

        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (!hidden) {
            updateToolbarIcon();
            if (BackendHandler.getInstance(getActivity()).getGroup(GroupController.getInstance().getCurrentGroup().getGroupId()) == null) {
                getFragmentManager().popBackStackImmediate();
            }

            titleTv.setText(GroupController.getInstance().getCurrentGroup().getName());

            if (menu != null) {
                menu.clear();
                menuInflater.inflate(R.menu.menu_task, menu);
//                shareIntent();
                setCustomActionProvider(menu);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                ((MainActivity)getActivity()).openDrawerLayout();
                return true;

            case R.id.action_new_task:
                createNewTask();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void createNewTask() {
        if (!Utils.isNetworkAvailble(getActivity())) {
            Utils.showToast(getActivity(), getString(R.string.no_internet_connection));
            return;
        }

        taskController.createNewTask();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_task, menu);

        this.menu = menu;
        this.menuInflater = inflater;

        setCustomActionProvider(menu);
//        shareIntent();
    }

    private void setCustomActionProvider(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_create);

        mCustomActionProvider = (CustomActionProvider) MenuItemCompat.getActionProvider(item);

        mCustomActionProvider.setItemClickListener(new TaskFilterActionProviderListener() {
            @Override
            public void onTaskFilter(int filterBy, boolean isChecked) {
                if (isChecked) {
                    taskAdapter.filterBy(filterBy);
                }
//                System.out.println("filter By = " + filterBy);
            }

            @Override
            public void onTaskSortBy(int sortBy, boolean isChecked) {
                if (isChecked) {
                    taskAdapter.sort(sortBy);
                }
//                System.out.println("sort by = " + sortBy);
            }

            @Override
            public TaskAdapter getTaskAdapter() {
                return taskAdapter;
            }
        });
    }

//    private void shareIntent() {
//        if (mShareActionProvider != null) {
//            Intent intent = new Intent();
//            intent.setAction(Intent.ACTION_SEND);
//            intent.putExtra(Intent.EXTRA_TEXT, "my share text");
//            intent.setType("text/plain");
//
//            mShareActionProvider.setShareIntent(intent);
//            mShareActionProvider.setShareHistoryFileName(null);
//        }
//    }

    private void setToolbar(View view) {
        Group group = GroupController.getInstance().getCurrentGroup();
        initGroupToolbar(view, R.id.taskListFragmentGroupToolbar, group.getName(), group.getMembersText());

        if (logoIv != null) {
            ImageUtil.setImageFromStorage(logoIv, group.getImageObjectId());
        }

        enableHomeButton(R.drawable.ic_list_white);
    }

    private void updateToolbarIcon() {
        Group group = GroupController.getInstance().getCurrentGroup();
        if (logoIv != null) {
            ImageUtil.setImageFromStorage(logoIv, group.getImageObjectId());
        }
    }

    @Override
    public void onTextLayoutClick() {
        ((MainActivity) getActivity()).addFragment(new GroupDetailFragment(), true);
    }

    @Override
    public void onLogoClick() {
        getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    private void updateSeenByInfo() {
        BackendHandler.getInstance(getActivity()).updateSeenByInfoForThisGroup(GroupController.getInstance().getCurrentGroup());
    }

    private void initializeListView(View view) {
        ListView listView = (ListView) view.findViewById(R.id.task_activity_listView);
        registerForContextMenu(listView);

        taskAdapter = new TaskAdapter(getActivity().getApplicationContext(), R.id.task_activity_listView, this);

        listView.setAdapter(taskAdapter);

        Group group = GroupController.getInstance().getCurrentGroup();
        Debug.debug("Group id = " + group.getGroupId());
        DBController.getInstance(getActivity()).loadAllTaskOfGroup(this, group.getGroupId());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.task_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

//        Debug.debug("onContextMenuSelected itemId = " + item.getItemId());
        switch (item.getItemId()) {

            case R.id.task_menu_edit:
//                editTask(item, TaskController.UPDATE_MESSAGE);
                editTask(item, TaskController.UPDATE_MESSAGE);
                return true;
            case R.id.task_menu_remark:
                editTask(item, TaskController.UPDATE_REMARK);
                return true;
            case R.id.task_menu_delete:
                Debug.debug("onContextMenuSelected = " + item.getItemId());
                deleteTask(item);
                return true;
            case R.id.task_menu_remove:
                removeTask(item);
                return true;
            case R.id.task_menu_seen_info:
                openSeenInfo(item);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
//
//    private void editTask(MenuItem item) {
//        if (!Utils.isNetworkAvailble(getActivity())) {
//            Utils.showToast(getActivity(), getString(R.string.no_internet_connection));
//            return;
//        }
//
//        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//        Task task = taskAdapter.getItem(info.position);
//        Debug.debug("Edit task " + info.position);
//
////        ((MainActivity)getActivity()).addFragment(TaskCreateFragment.newInstance(task.getSubject(),task.getDescription()),true);
//    }

    private void editTask(MenuItem item, int updateType) {
        if (!Utils.isNetworkAvailble(getActivity())) {
            Utils.showToast(getActivity(), getString(R.string.no_internet_connection));
            return;
        }

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Debug.debug("Edit task " + info.position);
        Task task = taskAdapter.getItem(info.position);
        taskController.editTask(task, updateType, getFragmentManager(), this);
    }


    private void deleteTask(MenuItem item) {
        if (!Utils.isNetworkAvailble(getActivity())) {
            Utils.showToast(getActivity(), getString(R.string.no_internet_connection));
            return;
        }

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Task task = taskAdapter.getItem(info.position);
        taskController.deleteTask(task);
    }

    private void removeTask(MenuItem item) {
        if (!Utils.isNetworkAvailble(getActivity())) {
            Utils.showToast(getActivity(), getString(R.string.no_internet_connection));
            return;
        }

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Task task = taskAdapter.getItem(info.position);

        taskController.removeTask(task);
        removeTask(task);
    }

    private void openSeenInfo(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Task task = taskAdapter.getItem(info.position);

        SeenInfoDialog seenInfoDialog = SeenInfoDialog.newInstance(task);
        seenInfoDialog.show(getFragmentManager());
//        addFragment(new SeenInfoFragment(task));

    }

    @Override
    public void onAckButtonClick(Task task) {
        taskController.acknowledgeTask(task.getObjectId(),this);
    }

    @Override
    public void onConfirmButtonClick(Task task) {
        taskController.confirmTask(task.getObjectId(),this);
    }

    private void showAlertPopup() {
        Utils.showToast(getActivity(), "Invalid Operation");
    }

    @Override
    public void onTaskLoaded(Task task) {
        taskAdapter.addTask(task);
    }

    @Override
    public void removeTask(Task task) {
        taskAdapter.removeTask(task.getObjectId());
    }

    @Override
    public void refreshTaskList() {
        taskAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTaskDelete(Task deletedTask) {

    }

    @Override
    public void onTaskUpdate(Task updatedTask) {

    }
}