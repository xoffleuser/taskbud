package com.xoffle.tasktracker.image;

import android.content.Context;
import android.graphics.Bitmap;

import com.inkling.leaderboard.AppImages;
import com.inkling.leaderboard.ParseImageListener;
import com.xoffle.tasktracker.image.ParseImageHandler;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by ravi on 25/7/15.
 */
public class ImageController {
    public void downloadAllUpdatedImages(Context context) {
        DataStorage store = DataStorage.getInstance(context);

        HashSet set = (HashSet) store.getImageUpdates();

        Debug.debug("ImageController.download all Images set = "+set);

        if (set == null) {
            return;
        }

        Debug.debug("ImageDownload ---");
        Iterator<String> iterator = set.iterator();

        while (iterator.hasNext()) {
            String imageName = iterator.next();
            Debug.debug("ImageKey = "+imageName);
            downloadImage(set, imageName, context);
        }
    }

    private void downloadImage(final HashSet set, final String imageName, final Context context) {
//        Debug.debug("downloadImage() imageName = "+imageName);
        ParseImageHandler.downloadImage(imageName, new ParseImageListener() {
            @Override
            public void onImageSaveOrUpdate(AppImages appImages) {

            }

            @Override
            public void onImageDownload(byte[] bytes) {

                Debug.debug("downloadImage bytes = "+bytes+" imageName = "+imageName);
                set.remove(imageName);
                DataStorage.getInstance(context).setImageUpdates(set);

                Bitmap bitmap = ImageUtil.convertToBitmap(bytes);
                if (bitmap != null) {
                    ImageUtil.saveImage(context, bitmap, imageName);
                    Debug.debug("imageDownload() Bitmap = "+bitmap);
                }
            }
        });
    }
}