package com.xoffle.tasktracker.image;

import android.content.Context;
import android.graphics.Bitmap;

import com.inkling.leaderboard.AppImages;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.ParseImageListener;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.listeners.OnImageUploadListener;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;

/**
 * Created by ravi on 21/7/15.
 */
public class ParseImageHandler {

    public static void saveGroupImage(final Context context, final Group group, final OnImageUploadListener onImageUploadListener) {
        final Bitmap bitmap = getGroupBitmap();

        byte[] bytes = ImageUtil.convertToBytes(bitmap);

        Bitmap byteBitmap = ImageUtil.convertToBitmap(bytes);
        Debug.debug("byteBitmap = " + byteBitmap);

        LeaderBoard.saveOrUpdateImage(group.getImageObjectId(),bytes, new ParseImageListener() {

            @Override
            public void onImageSaveOrUpdate(AppImages appImages) {
                Debug.debug("Image is successfully uploaded to parse = ");
                group.setImageId(appImages.getObjectId());

                ImageUtil.saveImage(context, bitmap, appImages.getObjectId());

                onImageUploadListener.onImageUpload(bitmap, appImages.getObjectId());

                saveImageIdToGroup(context, group, appImages.getObjectId());

            }

            @Override
            public void onImageDownload(byte[] bytes) {

            }
        });
    }

    public static void saveUserImage(final Context context, String imageId, final OnImageUploadListener onImageUploadListener) {
        final Bitmap bitmap = ImageUtil.loadImageFromStorage(Constant.ImageC.GROUP_IMG_NAME);

        byte[] bytes = ImageUtil.convertToBytes(bitmap);

        Bitmap byteBitmap = ImageUtil.convertToBitmap(bytes);
//        Debug.debug("byteBitmap = " + byteBitmap);

        LeaderBoard.saveOrUpdateImage(imageId,bytes, new ParseImageListener() {

            @Override
            public void onImageSaveOrUpdate(AppImages appImages) {
//                Debug.debug("Image is successfully uploaded to parse = ");
//                group.setImageId(appImages.getObjectId());

                ImageUtil.saveImage(context, bitmap, appImages.getObjectId());

                onImageUploadListener.onImageUpload(bitmap,appImages.getObjectId());

//                saveImageIdToGroup(context, group, appImages.getObjectId());

            }

            @Override
            public void onImageDownload(byte[] bytes) {

            }
        });
    }

    private static void saveImageIdToGroup(Context context, Group group, String imageObjectId) {

        group.setImageId(imageObjectId);
        BackendHandler.getInstance(context).updateGroup(group, null,false);
    }

    private static Bitmap getGroupBitmap() {
        return ImageUtil.loadImageFromStorage(Constant.ImageC.GROUP_IMG_NAME);
    }

    public static void downloadImage(String imageObjectId, ParseImageListener listener) {
        LeaderBoard.downloadImage(imageObjectId, listener);
    }
}
