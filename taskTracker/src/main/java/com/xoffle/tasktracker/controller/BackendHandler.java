package com.xoffle.tasktracker.controller;

import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.inkling.leaderboard.AppGroup;
import com.inkling.leaderboard.AppGroupTask;
import com.inkling.leaderboard.AppTaskBuds;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.LeaderBoardListener;
import com.inkling.leaderboard.SeenBy;
import com.inkling.leaderboard.Task;
import com.inkling.leaderboard.TaskBudBean;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.db.CursorFactory;
import com.xoffle.tasktracker.db.DBConstants;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.group.GroupController;
import com.xoffle.tasktracker.group.GroupInfo;
import com.xoffle.tasktracker.group.GroupListFragment;
import com.xoffle.tasktracker.group.GroupMember;
import com.xoffle.tasktracker.group.OnGroupCreateListener;
import com.xoffle.tasktracker.group.OnGroupUpdateListener;
import com.xoffle.tasktracker.json.JsonCreater;
import com.xoffle.tasktracker.json.JsonParser;
import com.xoffle.tasktracker.json.JsonUtils;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.pushnotification.PushNotification;
import com.xoffle.tasktracker.task.OnTaskUpdateListener;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by ravi on 17/4/15.
 */
public class BackendHandler {

    private static BackendHandler instance;
    private Context context;

    private BackendHandler(Context context) {
        this.context = context;
    }

    public static BackendHandler getInstance(Context context) {
        return instance == null ? instance = new BackendHandler(context) : instance;
    }

    /**
     * check whether a task is pending for seen then it marks as a seen and send notification to all
     * for this
     * @param currentGroup
     */
    public void updateSeenByInfoForThisGroup(Group currentGroup) {

        final String userId = DataStorage.getInstance(context).getUserId();

        Cursor cursor = DBController.getInstance(context).getGroupTasks(currentGroup.getGroupId());
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                String taskId = cursor.getString(cursor.getColumnIndex(DBConstants.TASK_ID));

//                if (!taskId.equals(userId)) {
                    Cursor cursorSeenInfo = DBController.getInstance(context).getTaskSeenInfoForMe(currentGroup.getGroupId(), userId);
                    if (cursorSeenInfo != null && cursorSeenInfo.getCount() > 0) {
                        cursorSeenInfo.moveToFirst();
                        int status = cursorSeenInfo.getInt(cursorSeenInfo.getColumnIndex(DBConstants.STATUS));
                        if (status != DBConstants.TASK_SEEN) {
                            SeenBy seenBy = new SeenBy(userId, DBConstants.TASK_SEEN, System.currentTimeMillis());
                            DBController.getInstance(context).updateSeenInfo(taskId, seenBy);

                            updateTaskSeenInfoToParse(userId, taskId);
                        }
                    }
//                }
            } while (cursor.moveToNext());
        }
    }

    private void updateTaskSeenInfoToParse(final String userId, final String taskId) {
        LeaderBoard.loadGroupTask(taskId, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask appTask, boolean isLoaded) {
                if (appTask != null) {
                    ArrayList<String> list = (ArrayList<String>) appTask.getSeenBy();

                    for (int i = list.size() - 1; i >= 0; i--) {
                        SeenBy seenBy = JsonUtils.stringToSeenBy(list.get(i));
                        if (seenBy.getUserId().equals(userId)) {
                            seenBy.setStatus(DBConstants.TASK_SEEN);
                            seenBy.setWhen(System.currentTimeMillis());
                        }
                    }

                    LeaderBoard.updateTask(new Task(appTask), new DataFetchListener<AppGroupTask>() {
                        @Override
                        public void onFetchComplete(AppGroupTask object, boolean isLoaded) {
                            if (object != null) {
                                Debug.debug("TaskSeenInfo Updated Successfully");

                                notifyToOthersOnTaskSeen(object.getGroupObjectId(), taskId, userId);
                            }
                        }

                        @Override
                        public void onListFetchComplete(List<AppGroupTask> objects) {

                        }
                    });
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    /**
     * update task after receiving from push notification, will update only in local database not in
     * parse.
     * @param taskId
     */
    public void onTaskUpdate(final String taskId) {
        LeaderBoard.loadGroupTask(taskId, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask appTask, boolean isLoaded) {
                Task task = new Task(appTask);
                DBController.getInstance(context).updateTask(appTask.getGroupObjectId(), task);
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    /**
     * delete task from the local server as well as parse server and send notification to all group
     * members, also task removed from group members task list
     * @param task
     * @param onTaskDeleteListener
     */
    public void deleteTask(final Task task, final OnTaskUpdateListener onTaskDeleteListener) {
        Debug.debug("delete task");
        final Group group = GroupController.getInstance().getCurrentGroup();
        LeaderBoard.deleteGroupTask(task.getObjectId(), new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask object, boolean isLoaded) {
                if (object == null) {
                    Debug.debug("Task not deleted successfully....");
                    return;
                }

                onTaskDeleteListener.onTaskDelete(task);
                Debug.debug("Successfully delete task");
                DBController.getInstance(context).deleteTask(task.getObjectId());

                PushNotification.notifyOnTaskDelete(group, task.getObjectId());

            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    /**
     * remove the task from local server only, no notification are sent to group members
     * @param task
     */
    public void removeTask(final Task task) {
//        final Group group = GroupController.getInstance().getCurrentGroup();
        DBController.getInstance(context).deleteTask(task.getObjectId());
    }

    public void deleteTaskFromLocalDb(String taskId) {
        DBController.getInstance(context).deleteTask(taskId);
    }

    public boolean isAcknowledgedBySamePersion(Task task) {
        Cursor taskCursor = DBController.getInstance(context).getGroupTask(task.getObjectId());
        if (taskCursor != null && taskCursor.getCount() > 0) {
            taskCursor.moveToFirst();

            String acknowledger = taskCursor.getString(taskCursor.getColumnIndex(DBConstants.TASK_ACK_CNF_ID));
            Debug.debug("acknowledgers id = " + acknowledger);
            if (!Utils.isNull(acknowledger) && acknowledger.equals(task.getAckCnfID())) {
                return true;
            }
        }
        return false;
    }

    public boolean isConfirmedBySamePersion(Task task) {
        return isAcknowledgedBySamePersion(task);
    }

    public void subscribe(final LeaderBoard.OnSubscrib onSubscrib) {

        LeaderBoard.subscribe(new LeaderBoard.OnSubscrib() {
            @Override
            public void onSubsrib(boolean isSuccessfullySubsribed) {

                if (isSuccessfullySubsribed) {
                    LeaderBoard.updateUserIdToInstallation(DataStorage.getInstance(context).getUserId(), new DataFetchListener<ParseInstallation>() {
                        @Override
                        public void onFetchComplete(ParseInstallation object, boolean isLoaded) {
                            if (onSubscrib != null) {
                                onSubscrib.onSubsrib(object != null);
                            }
                        }

                        @Override
                        public void onListFetchComplete(List<ParseInstallation> objects) {

                        }
                    });
                }
            }
        });
    }

    /**
     * create new task in local database after filtering data, fetched from parse through push
     * notification
     * @param groupId
     * @param taskId
     */
    public void onNewTaskCreate(final String groupId, String taskId) {

        LeaderBoard.loadGroupTask(taskId, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask appTask, boolean isLoaded) {
                if (appTask != null && isLoaded) {

                    addTaskToSQLite(groupId, appTask);

                    updateSeenInfo(groupId, appTask);
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    private void updateSeenInfo(final String groupId, final AppGroupTask appTask) {

        ArrayList<String> list = (ArrayList<String>) appTask.getSeenBy();

        String myId = DataStorage.getInstance(context).getUserId();
        SeenBy meSeenBy = getSeenByForId(myId, list);
        if (meSeenBy == null) {
            meSeenBy = new SeenBy(myId, DBConstants.TASK_DELIVERED, System.currentTimeMillis());
            list.add(JsonUtils.seenByToJsonString(meSeenBy));
        } else {
            meSeenBy.setStatus(DBConstants.TASK_DELIVERED);
            meSeenBy.setWhen(System.currentTimeMillis());
        }

        Task task = new Task(appTask);
        task.setSeenByList(list);

        LeaderBoard.updateTask(task, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask object, boolean isLoaded) {
                if (object != null) {
                    Debug.debug("TaskSeenInfo Updated Successfully");

                    notifyToOthersOnTaskSeen(object.getGroupObjectId(), appTask.getObjectId(), DataStorage.getInstance(context).getUserId());
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    private SeenBy getSeenByForId(String myId, ArrayList<String> list) {

        for (int i = list.size() - 1; i >= 0; i--) {
            SeenBy seenBy = JsonUtils.stringToSeenBy(list.get(i));

            if (seenBy.getUserId().equals(myId)) {
                return seenBy;
            }
        }

        return null;
    }

    private void notifyToOthersOnTaskDeliver(String groupId, int groupCount, AppGroupTask appTask, String myId) {

        Cursor cursor = DBController.getInstance(context).getGroupMembersId(groupId);
        if (cursor == null || cursor.getCount() == 0) {
            return;
        }

        ArrayList<String> membersIds = new ArrayList<>();

        cursor.moveToFirst();

        do {
            membersIds.add(cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID)));
        } while (cursor.moveToNext());

        PushNotification.notifyOnTaskDeliverToGroupBud(groupId, appTask.getObjectId(), myId, membersIds, DBConstants.TASK_DELIVERED);
    }

    private void notifyToOthersOnTaskSeen(String groupId, String taskId, String seenById) {

        Cursor cursor = DBController.getInstance(context).getGroupMembersId(groupId);
        if (cursor == null || cursor.getCount() == 0) {
            return;
        }

        cursor.moveToFirst();
        ArrayList<String> membersIds = new ArrayList<>();

        do {
//            String tbId = cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID));
//            String userName = DBController.getInstance(context).getUsername(tbId);
            membersIds.add(cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID)));
        } while (cursor.moveToNext());

        PushNotification.notifyOnTaskDeliverToGroupBud(groupId, taskId, seenById, membersIds, DBConstants.TASK_SEEN);
    }

    private void addTaskToSQLite(String groupId, AppGroupTask appTask) {
        DBController.getInstance(context).createTask(groupId, new Task(appTask));
    }

    /**
     * create new group in local database after filtering data, fetched from parse through push
     * notification
     * @param listener
     * @param groupId
     */
    public void onNewGroupCreate(final JsonParser.OnParseUpdateListener listener, final String groupId) {
        LeaderBoard.loadGroup(groupId, new LeaderBoardListener.OnGroupFetchListener() {
            @Override
            public void onCreate(AppGroup appGroup, boolean isLoaded) {
                if (appGroup != null && isLoaded) {

                    addImageUpdate(appGroup.getImageObjectId());

//                    final Group group = new Group(groupId, appGroup.getGroupName(), new ArrayList<GroupMember>());
                    final Group group = new Group(context, appGroup);

                    addGroupToSQLite(appGroup, new OnMemberAddedToGroupListener() {

                        @Override
                        public void onMemberAddedToGroup(TaskBudBean groupMember) {
                            group.addGroupMember(new GroupMember(groupMember));
                            if (listener != null) {
                                listener.onGroupMemberAddedToGroup();
                            }
                        }
                    });
                    if (listener != null) {
                        listener.onGroupLoaded(group);
                    }
                }
            }
        });
    }

    public void createGroup(String adminAndCreatorId, String groupName, final ArrayList<TaskBudBean> members, final OnGroupCreateListener onGroupCreateListener) {

        Debug.debug("Create Group");
        ArrayList<String> mList = getFbIdList(members);

        /*creating group to parse*/
        LeaderBoard.createGroup(groupName, adminAndCreatorId, context.getString(R.string.default_status), mList, new LeaderBoardListener.OnGroupFetchListener() {

            @Override
            public void onCreate(AppGroup appGroup, boolean isLoaded) {
                Debug.debug("createGroup onDataUpdate Listener");
                if (appGroup != null && !isLoaded) {
                    Debug.debug(appGroup);
                    storeDataToSqlite(appGroup);
                    PushNotification.sendGroupCreateNotification(appGroup);

                    Group group = new Group(context, appGroup);
                    onGroupCreateListener.onGroupCreated(group);

                } else {
                    Debug.debug("Null");
                }
            }
        });
    }

    private void storeDataToSqlite(AppGroup appGroup) {
        Debug.debug("storeDataToSqlite");
        DBController.getInstance(context).createGroup(appGroup);

        Debug.debug(" ------------size--= " + appGroup.getGroupMembers().size());
        ArrayList<String> membersId = (ArrayList<String>) appGroup.getGroupMembers();
        for (int i = membersId.size() - 1; i >= 0; i--) {
            Debug.debug(" ------------size--= " + appGroup.getObjectId() + ", id = " + membersId.get(i));
            DBController.getInstance(context).addGroupMember(appGroup.getObjectId(), membersId.get(i));
        }
    }

    private ArrayList<String> getFbIdList(ArrayList<TaskBudBean> fList) {
        ArrayList<String> list = new ArrayList<String>();

        for (int i = fList.size() - 1; i >= 0; i--) {
            list.add(fList.get(i).getMemberId());
        }
        return list;
    }

    public void handleBackendOnTaskUpdate(Task task, OnTaskUpdateListener onTaskUpdateListener) {
        Group group = GroupController.getInstance().getCurrentGroup();
        DBController.getInstance(context).updateTask(group.getGroupId(), task);

        PushNotification.notifyOnTaskUpdate(group, task.getObjectId());

        updateParseOnTaskAcknowledged(group, task, onTaskUpdateListener);
    }

    private void updateParseOnTaskAcknowledged(Group group, Task task, final OnTaskUpdateListener onTaskUpdateListener) {

        LeaderBoard.updateTask(task, new DataFetchListener<AppGroupTask>() {

            @Override
            public void onFetchComplete(AppGroupTask object, boolean isLoaded) {
                if (object != null) {
                    onTaskUpdateListener.onTaskUpdate(new Task(object));
                } else {
                    onTaskUpdateListener.onTaskUpdate(null);
                }
                Debug.debug("task acknowledged = " + object);
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {
            }
        });
    }

    /**
     * create new task in selected group and update task id on successful creation.
     * @param task
     * @param taskCreateListener
     */
    public void createTask(Task task, final Constructor.OnTaskCreateListener taskCreateListener) {
        Debug.debug("backend handler create task");

        final Group group = GroupController.getInstance().getCurrentGroup();
        final String groupId = group.getGroupId();

//        final Task task = new Task(taskInfo.taskId, taskInfo.msg);
        createTaskToParse(groupId, task, new Constructor.OnUpdateToParseListener<AppGroupTask>() {

            @Override
            public void onUpdateToParse(AppGroupTask appTask) {
                if (appTask == null) {
                    taskCreateListener.onTaskCreate(null);
                    return;
                } else {

                    Debug.debug(appTask);
                    Task newTask = new Task(appTask);
                    Debug.debug("backend handler update task to parse");
                    DBController.getInstance(context).createTask(newTask);

                    taskCreateListener.onTaskCreate(newTask);

                    notifyToGroupMembersOnTaskCreate(newTask);
//                    createSeenByInfoToParse(group, newTask);
                }
            }
        });
    }

    private void createSeenByInfoToParse(Group group, final Task task) {
        //TO DO

//        ArrayList<String> seenByIds = getSeenByIds(group);

//        LeaderBoard.loadOrCreateAppTaskSeenInfo(group.getMemberId(), group.getGroupCount(), task.taskCreatersId, task.taskId, seenByIds, new LeaderBoard.OnTaskSeenInfoFetchListerer() {
//            @Override
//            public void onFetch(AppTaskSeenInfo seenInfo, boolean isLoaded) {
//                if (seenInfo != null) {
        notifyToGroupMembersOnTaskCreate(task);
//                    Debug.debug("Task Seen Info Created Successfully...");
//                } else {
//                    Debug.debug("Task Seen Info creation failed...");
//                }
//            }
//        });
    }

    private ArrayList<String> getSeenByIds(Group group) {
        ArrayList<String> memberIds = group.getMemberIds();

        String myId = DataStorage.getInstance(context).getUserId();
        ArrayList<String> seenByIds = new ArrayList<>();
        int status;
        for (int i = memberIds.size() - 1; i >= 0; i--) {
            if (myId.equals(memberIds.get(i))) {
                status = DBConstants.TASK_SEEN;
            } else {
                status = DBConstants.TASK_NOT_DELIVERED;
            }
            String string = memberIds.get(i) + "#" + status + "#" + System.currentTimeMillis();
            seenByIds.add(string);
        }
        return seenByIds;
    }

    private void notifyToGroupMembersOnTaskCreate(Task task) {
        Group group = GroupController.getInstance().getCurrentGroup();
        ArrayList<String> userIds = group.getMemberIds();

        JSONObject json = JsonCreater.createJsonOnTaskCreate(group.getGroupId(), task.getObjectId(), context.getString(R.string.new_task_created) + group.getName());

        PushNotification.sendNotification(userIds, json);
    }

    private void createTaskToParse(final String gId, final Task task, final Constructor.OnUpdateToParseListener listener) {
        LeaderBoard.createGroupTask(task, new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask appTask, boolean isLoaded) {
                if (appTask != null) {
                    Debug.debug("Task Created Successfully, isLoaded = " + isLoaded);
                    listener.onUpdateToParse(appTask);

                } else {
                    Debug.debug("task error, not created");
                    listener.onUpdateToParse(null);
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    public void addGroupToSQLite(final AppGroup appGroup, final OnMemberAddedToGroupListener onMemberAddedToGroupListener) {
        DBController.getInstance(context).createGroup(appGroup);

        List<String> memberIdList = appGroup.getGroupMembers();

        if (memberIdList == null) {
            return;
        }

        for (int i = memberIdList.size() - 1; i >= 0; i--) {

            TaskBudBean member = null;
            if ((member = getGroupMemberFromSQLite(memberIdList.get(i))) != null) {
                addGroupMember(appGroup.getObjectId(), member.getUserName(), member.getMemberId(), onMemberAddedToGroupListener);
                continue;
            }

            LeaderBoard.loadAppMember(memberIdList.get(i), new DataFetchListener<AppTaskBuds>() {
                @Override
                public void onFetchComplete(AppTaskBuds appMember, boolean isLoaded) {
                    if (appMember != null) {
                        Debug.debug(" app Member = " + appMember.toString());
                        addGroupMember(appGroup.getObjectId(), appMember.getTbUserName(), appMember.getUserId(), onMemberAddedToGroupListener);
                    }
                }

                @Override
                public void onListFetchComplete(List<AppTaskBuds> objects) {
                }
            });
        }
    }

    /**
     * add the group member after Group is create by fetching data from parse to local sqlite
     * database
     * @param groupId
     * @param memberName
     * @param memberId
     * @param onMemberAddedToGroupListener
     */
    private void addGroupMember(String groupId, String memberName, String memberId, OnMemberAddedToGroupListener onMemberAddedToGroupListener) {
        DBController.getInstance(context).addGroupMember(groupId, memberId);
        if (onMemberAddedToGroupListener != null) {
            onMemberAddedToGroupListener.onMemberAddedToGroup(new TaskBudBean(memberId, memberName));
        }
    }

    private TaskBudBean getGroupMemberFromSQLite(String memberId) {
        Cursor cursor = DBController.getInstance(context).getTaskBudCursor(memberId);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            return new TaskBudBean(cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID)), cursor.getString(cursor.getColumnIndex(DBConstants.NAME)));
        }
        return null;
    }

    public boolean isTaskConfirmed(Task task) {
        Cursor taskCursor = DBController.getInstance(context).getGroupTask(task.getObjectId());
        if (taskCursor != null && taskCursor.getCount() > 0) {
            taskCursor.moveToFirst();

            int isConfirmedNum = taskCursor.getInt(taskCursor.getColumnIndex(DBConstants.TASK_IS_CONFIRMED));
            Debug.debug("acknowledgers id = " + isConfirmedNum);
            if (isConfirmedNum == 1) {
                return true;
            }
        }
        return false;
    }

    public void updateTaskSeenInfo(String taskId, SeenBy seenBy) {
        DBController.getInstance(context).updateSeenInfo(taskId, seenBy.getUserId(), seenBy.getStatus());
    }

    public String getUsername(String userId) {
        return DBController.getInstance(context).getUsername(userId);
    }

    public String getDisplayName(String userId) {
        return DBController.getInstance(context).getDisplayName(userId);
    }

    /**
     * return memberId/fbId and name of all members of particular group
     * @param groupId
     * @return list of GroupMembers related to given Group
     */
    public ArrayList<GroupMember> getGroupMembers(String groupId, String adminId) {

        Cursor cursor = DBController.getInstance(context).getGroupMembersId(groupId);
        ArrayList<GroupMember> groupMembers = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                String tbId = cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID));
//                String name = DBController.getInstance(context).getUsername(tbId);

                GroupMember memberBean = CursorFactory.createGroupMemberBean(tbId, context, adminId);
                groupMembers.add(memberBean);
            } while (cursor.moveToNext());
        }

        return groupMembers;
    }

    /**
     * return memberId/fbId and name of all members of particular group
     * @return list of GroupMembers related to given Group
     */
    public ArrayList<TaskBudBean> getAllTaskBuds() {

        Cursor cursor = DBController.getInstance(context).getAllTaskBuds();
        ArrayList<TaskBudBean> taskBudBeans = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                String tbId = cursor.getString(cursor.getColumnIndex(DBConstants.TASKBUD_ID));

                TaskBudBean memberBean = CursorFactory.createTaskBud(context, tbId);
                taskBudBeans.add(memberBean);
            } while (cursor.moveToNext());
        }

        return taskBudBeans;
    }

    public SeenBy getSeenInfoFor(String taskId, String memberId) {
        Cursor cursor = DBController.getInstance(context).getTaskSeenInfoFor(taskId, memberId);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            long updateDate = cursor.getLong(cursor.getColumnIndex(DBConstants.TASK_SEEN_INFO_WHEN));
            int status = cursor.getInt(cursor.getColumnIndex(DBConstants.STATUS));

            return new SeenBy(memberId, status, updateDate);
        }
        return null;
    }

    public GroupInfo getGroupInfo(String id) {
        GroupInfo groupInfo = null;
        Cursor cursor = DBController.getInstance(context).getGroupInfo(id);
        if (cursor != null || cursor.getCount() > 0) {
            cursor.moveToFirst();

            String groupName = cursor.getString(cursor.getColumnIndex(DBConstants.NAME));
            long createDate = cursor.getLong(cursor.getColumnIndex(DBConstants.CREATED_AT));
            long updateDate = cursor.getLong(cursor.getColumnIndex(DBConstants.UPDATED_AT));

            groupInfo = new GroupInfo(groupName, createDate, updateDate);
        }

        return groupInfo;
    }

    public TaskBudBean getMember(String groupId) {

        TaskBudBean friend = null;
        Cursor cursor = DBController.getInstance(context).getTaskBudCursor(groupId);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            String name = cursor.getString(cursor.getColumnIndex(DBConstants.NAME));
            friend = new TaskBudBean("", name);
        }

        return friend;
    }

    public void loadMyGroup(GroupListFragment fragment) {
        checkExistenceOfMyGroupOnParse(fragment);
    }

    private void checkExistenceOfMyGroupOnParse(final GroupListFragment fragment) {
        final String adminId = DataStorage.getInstance(context).getUserId();
        LeaderBoard.fetchMyGroups(adminId, new DataFetchListener() {
            @Override
            public void onFetchComplete(ParseObject object, boolean isLoaded) {
                if (object == null)//my group does not exist so create it
                {
                    createMyGroup(adminId, fragment);
                } else {
                    DataStorage.getInstance(context).setIsMyGroupCreated(true);
                }
            }

            @Override
            public void onListFetchComplete(List objects) {
            }
        });
    }

    private void createMyGroup(String adminId, final GroupListFragment fragment) {
        LeaderBoard.createMyGroup(adminId, new DataFetchListener<AppGroup>() {
            @Override
            public void onFetchComplete(AppGroup appGroup, boolean isLoaded) {
                if (appGroup != null) {
                    Toast.makeText(context, "MyTasks Group Created Successfully...", Toast.LENGTH_SHORT).show();
                    saveMyGroupToSqlite(appGroup);
                    DataStorage.getInstance(context).setIsMyGroupCreated(true);
                    fragment.onGroupLoaded(new Group(context, appGroup));
                }
            }

            @Override
            public void onListFetchComplete(List objects) {
            }
        });
    }

    private void saveMyGroupToSqlite(AppGroup appGroup) {
        DBController.getInstance(context).createMyGroup(appGroup);
    }

    public void updateGroupUpdateTime(final String groupId) {
        LeaderBoard.updateAppGroupUpdateTime(groupId, new DataFetchListener<AppGroup>() {
            @Override
            public void onFetchComplete(AppGroup object, boolean isLoaded) {
                if (object != null) {
                    DBController.getInstance(context).updateGroupUpdateTime(groupId);
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {

            }
        });
    }

    public void updateGroup(final Group group, final OnGroupUpdateListener onGroupUpdateListener, final boolean shouldNotify) {
        LeaderBoard.updateAppGroup(group, new DataFetchListener<AppGroup>() {
            @Override
            public void onFetchComplete(AppGroup object, boolean isLoaded) {
                if (object != null) {

//                    if (shouldNotify) {
                    notifyOnGroupUpdate(group, shouldNotify);
//                    }

                    DBController.getInstance(context).updateGroup(group);
                    if (onGroupUpdateListener != null) {
                        onGroupUpdateListener.onGroupUpdate(group);
                    }
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {
            }
        });
    }

    public void notifyOnGroupUpdate(Group group, boolean shouldNotify) {
        String myId = DataStorage.getInstance(context).getUserId();

        for (int i = group.getMemberBeanIdList().size() - 1; i >= 0; i--) {
            String memberId = group.getMemberBeanIdList().get(i);
            if (!memberId.equals(myId)) {
                PushNotification.notifyOnGroupUpdate(group.getGroupId(), memberId, shouldNotify);
            }
        }
    }

    public void onGroupUpdate(String groupId) {

        LeaderBoard.loadGroup(groupId, new LeaderBoardListener.OnGroupFetchListener() {
            @Override
            public void onCreate(AppGroup appGroup, boolean isLoaded) {
                addImageUpdate(appGroup.getImageObjectId());
                DBController.getInstance(context).updateGroup(new Group(context, appGroup));
            }
        });
    }

    private void addImageUpdate(String imageObjectId) {
        Debug.debug("addImageUpdate  imageObjectId= " + imageObjectId);
        if (imageObjectId != null) {

            DataStorage store = DataStorage.getInstance(context);
            HashSet<String> set = (HashSet<String>) store.getImageUpdates();

            if (set == null) {
                set = new HashSet<>();
            }

            set.add(imageObjectId);
            store.setImageUpdates(set);

            Debug.debug("addImageUpdate store = " + store.getImageUpdates());
        }
    }

    public Group getGroup(String groupId) {
        Cursor cursor = DBController.getInstance(context).loadGroup(groupId);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

//            Cursor mCursor = DBConhitroller.getInstance(this).getGroupMembersId(groupId);
            return new Group(context, cursor);
//
        }
        return null;
    }

    public void deleteLocalDatabase() {
        DBController.getInstance(context).deleteLocalDb();
    }

    public interface OnMemberAddedToGroupListener {
        void onMemberAddedToGroup(TaskBudBean groupMember);
    }

    public interface OnDataUpdateListener {
        void onDataUpdate(boolean isSuccessful, String groupId);
    }
}