package com.xoffle.tasktracker.controller;

import android.content.Context;

import com.inkling.leaderboard.Task;
import com.inkling.leaderboard.TaskBudBean;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

public class Constructor {

    private static Constructor instance;
    private Context context;

    public Constructor() {
    }

    public static Constructor getInstance() {
        return instance == null ? instance = new Constructor() : instance;
    }

    public interface OnUpdateToParseListener<T extends ParseObject> {
        void onUpdateToParse(T obj);
    }

    public interface OnTaskCreateListener {
        void onTaskCreate(Task task);
    }
}