package com.xoffle.tasktracker.controller;

import android.app.Application;

import com.inkling.leaderboard.LeaderBoard;
import com.parse.Parse;
import com.xoffle.tasktracker.R;

public class MyApplication extends Application {

	@Override
	public void onCreate() {
		Parse.initialize(getApplicationContext(), getString(R.string.app_id), getString(R.string.client_key));
		LeaderBoard.setDebug(true);
		LeaderBoard.initialize();
		super.onCreate();
	}
}