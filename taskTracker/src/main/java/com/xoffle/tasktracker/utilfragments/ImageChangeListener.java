package com.xoffle.tasktracker.utilfragments;

/**
 * Created by ravi on 5/7/15.
 */
public interface ImageChangeListener {

    void onImageChange(String imageName);
}
