package com.xoffle.tasktracker.utilfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.custom.LoadingDialog;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.view.MyFragment;

/**
 * Created by ravi on 9/6/15.
 */
public class TextChangeFragment extends MyFragment implements View.OnClickListener {

    private static final String SUBJECT_TITLE = "SUBJECT_TITLE";
    private static final String SUBJECT_MSG = "SUBJECT_MSG";
    private static final java.lang.String TEXT_LIMIT = "TEXT_LIMIT";

    private TextChangeListener textChangeListener;
    private TextView textViewLimit;
    private int availableLimit;
    private LoadingDialog loadingDialog;

    public static TextChangeFragment newInstance(String subject, String subjectMsg, int textLimit) {
        TextChangeFragment taskCreateFragment = new TextChangeFragment();

        Bundle bundle = new Bundle();

        bundle.putString(SUBJECT_TITLE, subject);
        bundle.putString(SUBJECT_MSG, subjectMsg);
        bundle.putInt(TEXT_LIMIT, textLimit);

        taskCreateFragment.setArguments(bundle);

        return taskCreateFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.text_change_fragment, container, false);

        initToolbar(view, R.id.textChangeFragmentToolbarSimple, "Update " + getArguments().getString(SUBJECT_TITLE));

        enableHomeButton();

        View buttonLayout = view.findViewById(R.id.textChangeFragmentButtonLayout);
        buttonLayout.findViewById(R.id.buttonLayoutButtonYes).setOnClickListener(this);
        buttonLayout.findViewById(R.id.buttonLayoutButtonNo).setOnClickListener(this);

        textViewLimit = (TextView) view.findViewById(R.id.textChangeFragmentTextLimitTv);

        setText(view);

        loadingDialog = new LoadingDialog();
        return view;
    }

    private void setText(View view) {
        Bundle bundle = getArguments();
        String subjectTitle = bundle.getString(SUBJECT_TITLE);
        String subjectMsg = bundle.getString(SUBJECT_MSG);
        if (subjectMsg == null) {
            subjectMsg = "";
        }
        availableLimit = bundle.getInt(TEXT_LIMIT);

        textViewLimit.setText((availableLimit - subjectMsg.length()) + "");
        EditText editTextSubject = ((EditText) view.findViewById(R.id.textChangeFragmentSubjectEt));

        editTextSubject.setFilters(new InputFilter[]{new InputFilter.LengthFilter(availableLimit)});

        ((TextView) view.findViewById(R.id.textChangeFragmentSubjectTv)).setText(subjectTitle);
        if (!Utils.isNull(subjectMsg)) {
            editTextSubject.setText(subjectMsg);
        }

        editTextSubject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setTextToLimitTextView(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setTextToLimitTextView(CharSequence s) {
        int textLimit = getAvailableLimit(s);
        textViewLimit.setText(textLimit + "");
    }

    private int getAvailableLimit(CharSequence s) {
        return availableLimit - s.length();
    }

    public void setTextChangeListener(TextChangeListener textChangeListener) {
        this.textChangeListener = textChangeListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonLayoutButtonYes:
                onYesBtnClick();
                break;

            case R.id.buttonLayoutButtonNo:
                onNoBtnClick();
                break;

            default:
        }
    }

    public void hideLoadingDialog() {
//        Utils.dismissDialog(loadingDialog);
        loadingDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        if (loadingDialog != null && loadingDialog.isVisible()) {
            loadingDialog.dismiss();
        }
        super.onDestroy();
    }

    private void onNoBtnClick() {
        getFragmentManager().popBackStackImmediate();
    }

    private void onYesBtnClick() {

        EditText subjectEt = (EditText) getView().findViewById(R.id.textChangeFragmentSubjectEt);

        Utils.hideKayboard(getActivity(),subjectEt.getWindowToken());

        String subject = subjectEt.getText().toString();

        if (Utils.isNull(subject)) {
            Toast.makeText(getActivity(), "Subject could not be null", Toast.LENGTH_SHORT).show();
            return;
        } else if (!Utils.isNetworkAvailble(getActivity())) {
            Utils.showToast(getActivity(), "No Internet Connection...");
            return;
        }

//        Utils.showLoadingDialog(loadingDialog,getFragmentManager());
        loadingDialog.show(getFragmentManager());
        textChangeListener.onTextUpdate(subject);
//        getFragmentManager().popBackStackImmediate();
    }
}