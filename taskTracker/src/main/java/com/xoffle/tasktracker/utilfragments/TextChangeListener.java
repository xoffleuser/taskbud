package com.xoffle.tasktracker.utilfragments;

/**
 * Created by ravi on 4/7/15.
 */
public interface TextChangeListener {

    void onTextUpdate(String subject);
}
