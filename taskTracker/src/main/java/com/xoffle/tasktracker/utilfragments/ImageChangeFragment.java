package com.xoffle.tasktracker.utilfragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.custom.LoadingDialog;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.view.MyFragment;
import com.xoffle.tasktracker.view.customviews.SquareImageView;

/**
 * Created by ravi on 9/6/15.
 */
public class ImageChangeFragment extends MyFragment {

    private static final String IMAGE_PATH = "IMAGE_PATH";
    private static final int REQUEST_PHOTO_PICK = 2;
    private ImageChangeListener imageChangeListener;
    private SquareImageView squreImageView;

    private LoadingDialog loadingDialog;

    public static ImageChangeFragment newInstance(String imagePath) {
        ImageChangeFragment imageChangeFragment = new ImageChangeFragment();

        Bundle bundle = new Bundle();
        bundle.putString(IMAGE_PATH, imagePath);

        imageChangeFragment.setArguments(bundle);

        return imageChangeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_change_fragment, container, false);

        initToolbar(view, R.id.imageChangeFragmentToolbarSimple, "Change Icon");
        enableHomeButton();

        squreImageView = (SquareImageView) view.findViewById(R.id.imageChangeFragmentImageView);

        loadingDialog = new LoadingDialog();

        ImageUtil.setImageFromStorage(squreImageView, getArguments().getString(IMAGE_PATH));

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.menu_change_image, menu);

        MenuItem shareItem = menu.findItem(R.id.menuChangeImageShare);
        ShareActionProvider mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getSharedIntent());

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuChangeImageEdit:
                chooseImage();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void chooseImage() {
        Intent pickImageIntent = ImageUtil.getImagePickIntent(200, 200);
        startActivityForResult(pickImageIntent, REQUEST_PHOTO_PICK);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Debug.debug("OnActivityResult");
        super.onActivityResult(requestCode, resultCode, intent);

        Debug.debug("onActivity Result");
        if (requestCode == REQUEST_PHOTO_PICK && resultCode == getActivity().RESULT_OK) {
            setImage(intent);
        }
    }

    private void setImage(Intent intent) {
        Bundle extras = intent.getExtras();
        Debug.debug("setImage extra = " + extras);

        if (extras != null) {
            Bitmap photo = extras.getParcelable("data");

            Debug.debug("Extra = " + extras);
            squreImageView.setImageBitmap(photo);

            ImageUtil.saveImage(getActivity(), photo, Constant.ImageC.GROUP_IMG_NAME);

            if (imageChangeListener != null) {
                imageChangeListener.onImageChange(Constant.ImageC.GROUP_IMG_NAME);
            }
        }
    }

    public void showLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.show(getFragmentManager());
        }
    }

    public void dismissLoadingDialog() {
        if (loadingDialog != null && loadingDialog.isVisible()) {
            loadingDialog.dismiss();
        }
    }

    private Intent getSharedIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
        sendIntent.setType("text/plain");
//        startActivity(sendIntent);
        return sendIntent;
    }

    public void setImageChangeListener(ImageChangeListener imageChangeListener) {
        this.imageChangeListener = imageChangeListener;
    }
}