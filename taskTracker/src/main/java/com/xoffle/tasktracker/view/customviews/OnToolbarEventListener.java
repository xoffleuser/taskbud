package com.xoffle.tasktracker.view.customviews;

/**
 * Created by ravi on 28/5/15.
 */
public interface OnToolbarEventListener {
    void onTextLayoutClick();

    void onLogoClick();
}
