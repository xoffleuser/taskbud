package com.xoffle.tasktracker.view;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.view.customviews.OnToolbarEventListener;

/**
 * Created by ravi on 12/5/15.
 */
public class MyFragment extends Fragment implements OnToolbarEventListener {

    public MenuInflater menuInflater;
    public Menu menu;
    protected Toolbar toolbar;
    protected RelativeLayout textLayout;
    protected TextView titleTv;
    protected TextView subtitleTv;
    protected ImageView logoIv;

    protected void initToolbar(View view, int toolbarId, String title) {
        toolbar = (Toolbar) view.findViewById(toolbarId);

        ((ActionBarActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setTitle(title);
    }

    protected void initGroupToolbar(View view, int toolbarResId, String title, String subtitle) {
        toolbar = (Toolbar) view.findViewById(toolbarResId);

        toolbar.setTitle("");
        ((ActionBarActivity) getActivity()).setSupportActionBar(toolbar);

        RelativeLayout toolbarLayout = (RelativeLayout) toolbar.findViewById(R.id.toolbarGroupRelativeLayout);
        titleTv = (TextView) toolbar.findViewById(R.id.toolbarTextTextViewTitle);
        subtitleTv = (TextView) toolbar.findViewById(R.id.toolbarGroupTextViewDesc);

        titleTv.setText(title);
        subtitleTv.setText(subtitle);

        toolbarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTextLayoutClick();
            }
        });

        logoIv = (ImageView) view.findViewById(R.id.toolbarGroupLogo);
        logoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLogoClick();
            }
        });
    }

    protected void initTextToolbar(View view, int toolbarResId, String title, String subtitle) {
        toolbar = (Toolbar) view.findViewById(toolbarResId);

        toolbar.setTitle("");

        ((ActionBarActivity) getActivity()).setSupportActionBar(toolbar);

        titleTv = (TextView) toolbar.findViewById(R.id.toolbarTextTextViewTitle);
        subtitleTv = (TextView) toolbar.findViewById(R.id.toolbarTextTextViewDesc);

        titleTv.setText(title);

        if (subtitle != null) {
            subtitleTv.setText(subtitle);
        }
    }

    protected void initMiddleTextToolbar(View view, int toolbarResId, String title) {
        toolbar = (Toolbar) view.findViewById(toolbarResId);

        toolbar.setTitle("");

        ((ActionBarActivity) getActivity()).setSupportActionBar(toolbar);

        titleTv = (TextView) toolbar.findViewById(R.id.toolbarTextMiddleTextViewTitle);
        titleTv.setText(title);
    }

    @Override
    public void onTextLayoutClick() {

    }

    @Override
    public void onLogoClick() {

    }

    public void enableHomeButton() {
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void enableHomeButton(int resId) {
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(resId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}