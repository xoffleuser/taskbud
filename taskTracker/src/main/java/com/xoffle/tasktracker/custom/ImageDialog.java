package com.xoffle.tasktracker.custom;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.Utils;

/**
 * Created by ravi on 6/6/15.
 */
public class ImageDialog extends DialogFragment {

    private static final String TAG = "ImageDialog.TAG";
    private static final String IMAGE_TAG = "IMAGE_TAG";

    public static ImageDialog newInstance(String title, String imageName, String tag) {
        Bundle bundle = new Bundle();
        bundle.putString(Constant.TITLE, title);
        bundle.putString(Constant.ImageC.IMAGE_NAME, imageName);
        bundle.putString(IMAGE_TAG, tag);

        ImageDialog imageDialog = new ImageDialog();
        imageDialog.setArguments(bundle);

        return imageDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, STYLE_NORMAL);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.image_dialog, container, false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));

        findViewByIds(view);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageDialogImageView);
        TextView nameTextView = (TextView) view.findViewById(R.id.imageDialogTitle);

        String imageName = getArguments().getString(Constant.ImageC.IMAGE_NAME);
        setImage(imageView, imageName);

        nameTextView.setText(getArguments().getString(Constant.TITLE));

        return view;

//        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void setImage(ImageView imageView, String imageName) {
        if (Utils.isNull(imageName)) {
            if ("icon_user_default".equalsIgnoreCase(getArguments().getString(IMAGE_TAG))) {
                imageView.setImageResource(R.drawable.icon_user_default);
            } else {
                imageView.setImageResource(R.drawable.ic_launcher);
            }
            return;
        }

        ImageUtil.setImage(getActivity(), imageView, imageName);
    }

    private void findViewByIds(View view) {
    }

    public void show(FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment previousFragment = fragmentManager.findFragmentByTag(TAG);
        if (previousFragment != null) {
            fragmentTransaction.remove(previousFragment);
        }

        show(fragmentManager, TAG);
    }
}