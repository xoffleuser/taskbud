package com.xoffle.tasktracker.custom;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.RadioButton;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.model.DataStorage;

/**
 * Created by ravi on 3/6/15.
 */
public class CustomActionProvider extends ActionProvider implements CompoundButton.OnCheckedChangeListener {
    private TaskFilterActionProviderListener providerListener;
    private RadioButton ackOnlyRB;
    private RadioButton confirmedRB;
    private RadioButton cnfdAndAckdRB;
    private RadioButton activeRB;
    private RadioButton noActivityRadioButton;
    private RadioButton allRadioButton;

    private RadioButton rencetRB;
    private RadioButton createAtRB;

    /**
     * Creates a new instance.
     * @param context
     *         Context for accessing resources.
     */
    public CustomActionProvider(Context context) {
        super(context);
    }

    @Override
    public View onCreateActionView() {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_action_provider_view, null);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupWindow(v);
            }
        });

        return view;
    }

    private void showPopupWindow(View view) {

        System.out.println("view = " + providerListener);

        PopupWindow popupwindow_obj = popupDisplay();
        popupwindow_obj.setBackgroundDrawable(new ColorDrawable(getContext().getResources().getColor(R.color.my_awesome_darker_color)));
        popupwindow_obj.showAsDropDown(view, 0, 0); // where u want show on view click event popupwindow.showAsDropDown(view, x, y);
    }

    public PopupWindow popupDisplay() {

        final PopupWindow popupWindow = new PopupWindow(getContext());

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.popup_view, null);

        setListener(view);

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(view);

        return popupWindow;
    }

    private void setListener(View view) {
        ackOnlyRB = (RadioButton) view.findViewById(R.id.acknowledgeOnly);
        ackOnlyRB.setOnCheckedChangeListener(this);

        confirmedRB = (RadioButton) view.findViewById(R.id.confirmed);
        confirmedRB.setOnCheckedChangeListener(this);

        cnfdAndAckdRB = (RadioButton) view.findViewById(R.id.cnfdAndAckd);
        cnfdAndAckdRB.setOnCheckedChangeListener(this);

        activeRB = (RadioButton) view.findViewById(R.id.active);
        activeRB.setOnCheckedChangeListener(this);

        noActivityRadioButton = (RadioButton) view.findViewById(R.id.noActivity);
        noActivityRadioButton.setOnCheckedChangeListener(this);

        allRadioButton = (RadioButton) view.findViewById(R.id.all);
        allRadioButton.setOnCheckedChangeListener(this);

        rencetRB = (RadioButton) view.findViewById(R.id.recent);
        rencetRB.setOnCheckedChangeListener(this);

        createAtRB = (RadioButton) view.findViewById(R.id.createAt);
        createAtRB.setOnCheckedChangeListener(this);

        setDefaultFilter(providerListener.getTaskAdapter().getFilterBy(), providerListener.getTaskAdapter().getSortBy());
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        DataStorage store = DataStorage.getInstance(getContext());

        switch (buttonView.getId()) {
            case R.id.acknowledgeOnly:
                providerListener.onTaskFilter(Constant.TaskConstant.FILTER_ACKNOWLEDGED_ONLY, isChecked);
                break;

            case R.id.confirmed:
                providerListener.onTaskFilter(Constant.TaskConstant.FILTER_CONFIRMED, isChecked);
                break;

            case R.id.active:
                providerListener.onTaskFilter(Constant.TaskConstant.FILTER_ACTIVE, isChecked);
                break;

            case R.id.cnfdAndAckd:
                providerListener.onTaskFilter(Constant.TaskConstant.FILTER_CNFD_AND_ACKD, isChecked);
                break;

            case R.id.noActivity:
                providerListener.onTaskFilter(Constant.TaskConstant.FILTER_NO_ACTIVITY, isChecked);
                break;

            case R.id.all:
                providerListener.onTaskFilter(Constant.TaskConstant.FILTER_ALL, isChecked);
                break;

            case R.id.recent:
                providerListener.onTaskSortBy(Constant.TaskConstant.SHOW_RENCET, isChecked);
                break;

            case R.id.createAt:
                providerListener.onTaskSortBy(Constant.TaskConstant.SHOW_CREATE_AT, isChecked);
                break;

            default:
                break;
        }
    }

    public void setItemClickListener(TaskFilterActionProviderListener providerListener) {
        this.providerListener = providerListener;
    }

    private void setDefaultFilter(int filterBy, int sortBy) {
        switch (filterBy) {
            case Constant.TaskConstant.FILTER_ACKNOWLEDGED_ONLY:
                ackOnlyRB.setChecked(true);
                break;

            case Constant.TaskConstant.FILTER_CONFIRMED:
                confirmedRB.setChecked(true);
                break;

            case Constant.TaskConstant.FILTER_CNFD_AND_ACKD:
                cnfdAndAckdRB.setChecked(true);
                break;

            case Constant.TaskConstant.FILTER_ACTIVE:
                activeRB.setChecked(true);
                break;

            case Constant.TaskConstant.FILTER_NO_ACTIVITY:
                noActivityRadioButton.setChecked(true);
                break;

            case Constant.TaskConstant.FILTER_ALL:
                allRadioButton.setChecked(true);
                break;
        }

        switch (sortBy) {
            case Constant.TaskConstant.SHOW_RENCET:
                rencetRB.setChecked(true);
                break;

            case Constant.TaskConstant.SHOW_CREATE_AT:
                createAtRB.setChecked(true);
                break;
        }
    }
}