package com.xoffle.tasktracker.custom;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.xoffle.tasktracker.R;

/**
 * Created by ravi on 9/6/15.
 */
public class LoadingDialog extends DialogFragment {

    private static final java.lang.String TAG = "LoadingDialog.TAG";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(true);
        setStyle(STYLE_NORMAL, STYLE_NO_FRAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(100, 0, 0, 0)));
        return inflater.inflate(R.layout.loading_dialog,container,false);
    }

    public void show(FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment preFragment = fragmentManager.findFragmentByTag(TAG);
        if (preFragment != null) {
            fragmentTransaction.remove(preFragment);
        }

        show(fragmentManager, TAG);
    }
}
