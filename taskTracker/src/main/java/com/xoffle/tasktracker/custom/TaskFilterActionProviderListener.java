package com.xoffle.tasktracker.custom;

/**
 * Created by ravi on 30/6/15.
 */
public interface TaskFilterActionProviderListener {
    void onTaskFilter(int filterBy, boolean isChecked);

    void onTaskSortBy(int sortBy, boolean isChecked);

    com.xoffle.tasktracker.task.TaskAdapter getTaskAdapter();
}
