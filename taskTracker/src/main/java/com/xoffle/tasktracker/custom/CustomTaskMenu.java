package com.xoffle.tasktracker.custom;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.View;

import com.xoffle.tasktracker.R;

/**
 * Created by ravi on 21/6/15.
 */
public class CustomTaskMenu extends ActionProvider{

    /**
     * Creates a new instance.
     * @param context
     *         Context for accessing resources.
     */
    public CustomTaskMenu(Context context) {
        super(context);
    }

    @Override
    public View onCreateActionView() {
        return LayoutInflater.from(getContext()).inflate(R.layout.menu_task_filter,null);
    }
}
