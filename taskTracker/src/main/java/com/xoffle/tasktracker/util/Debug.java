package com.xoffle.tasktracker.util;

public class Debug {

	private static String TAG = "<<<<<< Debug >>>>>>";

	private static boolean isDebug = true;

	public static void debug(Object object) {
		debug(TAG, object);
	}

	public static void debug(String tag, Object object) {
		if (isDebug) {
			System.out.println(tag + " " + object.toString());
		}
	}

	public void setDebug(boolean status) {
		isDebug = status;
	}
}