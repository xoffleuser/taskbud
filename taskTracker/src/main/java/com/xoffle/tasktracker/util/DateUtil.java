package com.xoffle.tasktracker.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ravi on 25/4/15.
 */
public class DateUtil {

    public static final int FORMAT_1 = 1;
    public static final int FORMAT_2 = 2;
    public static final int FORMAT_3 = 3;
    public static final int FORMAT_DATE_1 = 4;

    private static final String FORMAT1 = "hh:mm a, dd-MMM-yy";
    private static final String FORMAT2 = "MMM-dd-yy HH:mm a";
    private static final String FORMAT3 = "dd-MMM-yy HH:mm";
    private static final String FORMAT_DATE1 = "dd-MMM-yy";

    public static String format(Date date, int formatType) {

        String format;
        switch (formatType) {
            case FORMAT_1:
                format = FORMAT1;
                break;
            case FORMAT_2:
                format = FORMAT2;
                break;
            case FORMAT_3:
                format = FORMAT3;
                break;
            case FORMAT_DATE_1:
                format = FORMAT_DATE1;
                break;
            default:
                format = FORMAT1;
                break;
        }

        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        try {
            String s = formatter.format(calendar.getTime());
            return s;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String format(long timeInMilliSeconds, int formatType) {
        return format(new Date(timeInMilliSeconds), formatType);
    }
}