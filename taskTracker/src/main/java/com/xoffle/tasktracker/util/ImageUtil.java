package com.xoffle.tasktracker.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.widget.ImageView;

import com.inkling.leaderboard.AppImages;
import com.inkling.leaderboard.ParseImageListener;
import com.xoffle.tasktracker.image.ParseImageHandler;
import com.xoffle.tasktracker.model.Constant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by ravi on 23/8/15.
 */
public class ImageUtil {
    public static final String DIR_NAME = "imgDir";
    public static final String FILE_NAME = "group_icon";

    public static String saveImage(Context context, Bitmap photo, String imageName) {

        return saveImageToExternalStorage(photo, imageName);
//            ContextWrapper contextWrapper = new ContextWrapper(context.getApplicationContext());
////            File directory = contextWrapper.getDir(DIR_NAME, Context.MODE_PRIVATE);
//            File directory = contextWrapper.getExternalFilesDir(DIR_NAME);
//            File myPath = new File(directory, FILE_NAME);
//
//            FileOutputStream outputStream = null;
//
//            try {
//                outputStream = new FileOutputStream(myPath);
//                photo.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
//                outputStream.close();
//
//                System.out.println("file stored successfully");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            return directory.getAbsolutePath();
    }

    private static String saveImageToExternalStorage(Bitmap photo, String imageName) {
//            String root = Environment.getExternalStorageDirectory().toString();
//            File myDir = new File(root + Constant.ImageC.IMAGE_DIR);

        File myDir = new File(Constant.ImageC.IMAGE_PATH);

        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        File file = new File(myDir, imageName + Constant.ImageC.IMAGE_EXT);

        if (file.exists()) {
            file.delete();
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            photo.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return myDir.getAbsolutePath();
    }

    public static Bitmap loadImageFromStorage(String name) {

        Bitmap bitmap = null;
        try {
            File f = new File(Constant.ImageC.IMAGE_PATH, name + Constant.ImageC.IMAGE_EXT);
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public static Intent getImagePickIntent(int width, int height) {
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        pickImageIntent.setType("image/*");
        pickImageIntent.putExtra("crop", "true");
        pickImageIntent.putExtra("outputX", width);
        pickImageIntent.putExtra("outputY", height);
        pickImageIntent.putExtra("aspectX", 1);
        pickImageIntent.putExtra("aspectY", 1);
        pickImageIntent.putExtra("scale", true);

//        File file = new File("data/images/groupIcon.png");

//        pickImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, file.getAbsolutePath());
        pickImageIntent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.toString());
        return pickImageIntent;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static byte[] convertToBytes(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap convertToBitmap(byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        BitmapFactory.Options opt = new BitmapFactory.Options();

        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opt);

        Debug.debug("Convert To Bitmap = " + bitmap);

        return bitmap;
    }

    public static void setImageDrawableResToView(ImageView imageView, int drawableResId) {
        imageView.setImageResource(drawableResId);
    }

    public static boolean setImageFromStorage(ImageView imageView, String imageId) {

        if (Utils.isNull(imageId)) {
            return false;
        }

        Bitmap bitmap = loadImageFromStorage(imageId);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            return true;
        }

        return false;
    }

    /**
     * setImage to imageView from local storage, if there is no image with imageId then it download
     * from parse and set to imageView and also save it to local storage
     * @param context
     * @param userImage
     * @param imageId
     */
    public static void setImage(Context context, ImageView userImage, String imageId) {
        if (Utils.isNull(imageId)) {
            return;
        }

        if (!setImageFromStorage(userImage, imageId)) {
            downloadImage(context, userImage, imageId);
        }
    }

    /**
     * download the image and set it to given imageView after saving to local
     * @param context
     * @param imageView
     * @param imageId
     */
    private static void downloadImage(final Context context, final ImageView imageView, final String imageId) {
        ParseImageHandler.downloadImage(imageId, new ParseImageListener() {
            @Override
            public void onImageSaveOrUpdate(AppImages appImages) {
            }

            @Override
            public void onImageDownload(byte[] bytes) {

                Bitmap bitmap = ImageUtil.convertToBitmap(bytes);
                if (bitmap != null) {
                    saveImage(context, bitmap, imageId);
                    imageView.setImageBitmap(bitmap);
                }
            }
        });
    }
}
