package com.xoffle.tasktracker.util;

/**
 * Created by ravi on 5/8/15.
 */
public class MyQueue<T> {

    private final int capacity;
    private int pointer;

    private T[] items;

    public MyQueue(int capacity) {
        this.capacity = capacity;
        pointer = -1;
    }

    public T pop() {
        if (pointer < 0) {
            return null;
        } else {
            T t = items[pointer];
            items[pointer] = null;
            pointer--;
            return t;
        }
    }

    public T[] getAllElement() {
        return items;
    }

    public void push(T item) {
        if (pointer == capacity - 1) {
            shiftElements(1);
        } else {
            pointer++;
        }

        items[pointer] = item;
    }

    private void shiftElements(int shiftCount) {
        if (shiftCount > 0) {
            for (int i = 0; i < capacity; i++) {
                items[i] = getElement(i, shiftCount);
            }
        } else {
            for (int i = capacity - 1; i >= 0; i--) {
                items[i] = getElement(i, shiftCount);
            }
        }
    }

    private T getElement(int i, int shiftCount) {
        if (isOutOfRange(i + shiftCount)) {
            return null;
        } else {
            return items[i + shiftCount];
        }
    }

    private boolean isOutOfRange(int position) {
        return position >= 0 && position < capacity;
    }

    @Override
    public String toString() {
        return pointer + "" + super.toString();
    }
}