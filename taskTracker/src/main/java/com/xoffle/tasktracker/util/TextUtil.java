package com.xoffle.tasktracker.util;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

/**
 * Created by ravi on 23/8/15.
 */
public class TextUtil {
    public static void setSpannableText(TextView textView, String name, String searchText) {

        if (Utils.isNull(name)) {
            textView.setText("");
            return;
        }

        SpannableString spannableContentName = new SpannableString(name);

        int index = name.toLowerCase().indexOf(searchText.toLowerCase());
        if (index >= 0) {

            spannableContentName.setSpan(new ForegroundColorSpan(Color.BLUE), index, index + searchText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            textView.setText(spannableContentName);
        } else {
            textView.setText(name);
        }
    }
}
