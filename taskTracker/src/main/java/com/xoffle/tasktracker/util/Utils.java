package com.xoffle.tasktracker.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.custom.LoadingDialog;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.model.DataStorage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class Utils {

    public static String ArrayListToString(ArrayList<String> list) {
        if (list == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        int size = list.size();
        for (int i = 0; i < size; i++) {
            builder.append(list.get(i)).append(",");
        }

        String string = null;
        if (builder.length() > 0) {
            string = builder.substring(0, builder.length() - 1);
        }
        return string;
    }


    public static boolean isLoggedIn(DataStorage store) {
        String id = store.getUserId();
        String name = store.getDisplayName();

        return !Utils.isNull(id) && !Utils.isNull(name);
    }

    public static void showToast(final Activity activity, final String msg) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static boolean isNetworkAvailble(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static boolean isNull(String msg) {
        return msg == null || msg.trim().length() == 0;
    }

    public static void showLoadingDialog(LoadingDialog loadingDialog, FragmentManager fManager) {
        if (loadingDialog != null && !loadingDialog.isVisible()) {
            loadingDialog.show(fManager);
        }
    }

    public static void dismissDialog(LoadingDialog loadingDialog) {
        if (loadingDialog != null && loadingDialog.isVisible()) {
            loadingDialog.dismiss();
        }
    }

    public static void hideKayboard(Context context, IBinder windowToken) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromInputMethod(windowToken, 0);
    }

    public static void shareVia(FragmentActivity activity) {
        Intent i = new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(android.content.Intent.EXTRA_SUBJECT, activity.getString(R.string.share_via_extra_text));
//        i.putExtra(android.content.Intent.EXTRA_TEXT, activity.getString(R.string.share_via_extra_text));
        activity.startActivity(Intent.createChooser(i, activity.getString(R.string.share_via)));
    }

    public static void likeApp(FragmentActivity activity) {
        final String appPackageName = getPackageName(activity); // getPackageName() from Context or Activity object
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private static String getPackageName(FragmentActivity activity) {
        return activity.getPackageName();
    }

    public ArrayList<String> StringToArrayList(String str) {
        ArrayList<String> list = new ArrayList<String>();
        String[] split = str.split(",");
        int size = split.length;

        for (int i = 0; i < size; i++) {
            list.add(split[i]);
        }

        return list;
    }
}