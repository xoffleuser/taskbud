package com.xoffle.tasktracker.util;

import java.util.regex.Pattern;

/**
 * Created by ravi on 11/8/15.
 */
public class TextValidator {

    private static final String USERNAME_REGEX = "[a-zA-Z]{1}\\w{3,14}";

    public static boolean isValidUserName(String username) {
        return Pattern.matches(USERNAME_REGEX, username);
    }
}