package com.xoffle.tasktracker.fb;

import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;

public class DownloadUtil {

	private static final String	PARAM_HEIGHT	= "&height=";
	private static final String	PARAM_WIDTH		= "&width=";

	// URL1+id+URL2+details
	private static final String	URL1			= "http://graph.facebook.com/";
	private static final String	URL2			= "/picture?redirect=1";

	public interface OnDownloadListener {

		public void onDownloadComplete(Bitmap bitmap);
	}

	public static void downloadImage(String id, int width, int height, final OnDownloadListener listener) {
		final StringBuffer urlBuild = new StringBuffer();
		urlBuild.append(URL1).append(id).append(URL2).append(PARAM_WIDTH).append(width).append(PARAM_HEIGHT).append(height);
		new Thread() {

			public void run() {
				listener.onDownloadComplete(downloadBitmap(urlBuild.toString()));
			};
		}.start();
	}

	private static Bitmap downloadBitmap(String url) {
		final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
		final HttpGet request = new HttpGet(url);
		try {
			HttpResponse response = client.execute(request);
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Header[] headers = response.getHeaders("Location");
				if (headers != null && headers.length != 0) {
					String newUrl = headers[headers.length - 1].getValue();
					// call again with new URL
					return downloadBitmap(newUrl);
				} else {
					return null;
				}
			}

			final HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream inputStream = null;
				try {
					inputStream = entity.getContent();
					// do your work here
					return BitmapFactory.decodeStream(inputStream);
				} finally {
					if (inputStream != null) {
						inputStream.close();
					}
					entity.consumeContent();
				}
			}
		} catch (Exception e) {
//			AndroidUtil.log("DownloadUtil: " + e);
			request.abort();
		} finally {
			if (client != null) {
				client.close();
			}
		}
		return null;
	}

}
