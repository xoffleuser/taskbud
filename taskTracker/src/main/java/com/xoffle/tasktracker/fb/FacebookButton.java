package com.xoffle.tasktracker.fb;

import android.content.Context;

import com.facebook.Session;
import com.inkling.fbdata.UserNode;
import com.inkling.leaderboard.AppTaskBuds;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.fb.FacebookUtil.OnScoreSave;
import com.xoffle.tasktracker.fb.FacebookUtil.OnUserLoginToFb;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;

import java.util.List;

public class FacebookButton {

    private boolean isLoggedIn;
    private FacebookUtil fbUtil;
    private Context context;

    public FacebookButton(Context context, FacebookUtil fbUtil) {
        this.context = context;
        this.fbUtil = fbUtil;
        isLoggedIn = checkIsLoggedIn();
    }

    public void login(final OnLogginToFb listener) {
        fbUtil.loginToFb(new OnUserLoginToFb() {

            @Override
            public void onLoginToFb(UserNode userNode) {
                if (userNode != null) {
                    Debug.debug("Logged in Successfully");

                    setUserIdAndName(userNode);

                    addMember();

                    subscribe();

                    fbUtil.saveScore(100, new OnScoreSave() {

                        @Override
                        public void onComplete() {
                            listener.onLoggin();
                        }
                    });
                }
            }
        });
    }

    void subscribe() {
        BackendHandler.getInstance(context).subscribe(null);
    }

    private void addMember() {
        final DataStorage store = DataStorage.getInstance(context);
        TaskBudBean taskBudBean = new TaskBudBean(store.getUserId(), store.getDisplayName(), context.getString(R.string.default_status));
        LeaderBoard.loadOrCreateAppMember(taskBudBean, new DataFetchListener<AppTaskBuds>() {
            @Override
            public void onFetchComplete(AppTaskBuds object, boolean isLoaded) {
                Debug.debug("Successfully create to parse = " + object.toString());
            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> objects) {
            }
        });
    }

    protected void setUserIdAndName(UserNode userNode) {
        DataStorage store = DataStorage.getInstance(context);

        String userId = userNode.getUserId();
        String userName = userNode.getUserName();

        System.out.println("FacebookButton.setUserIdAndName() userId = " + userId + ", userName = " + userName);

        store.setUserId(userId);
        store.setDisplayName(userName);
    }

    private boolean checkIsLoggedIn() {
        DataStorage store = DataStorage.getInstance(context);

        String id = store.getUserId();
        String name = store.getDisplayName();

        return !Utils.isNull(id) && !Utils.isNull(name);
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void logout() {
        Session session = Session.getActiveSession();
        session.closeAndClearTokenInformation();
    }

    public interface OnLogginToFb {
        void onLoggin();
    }
}