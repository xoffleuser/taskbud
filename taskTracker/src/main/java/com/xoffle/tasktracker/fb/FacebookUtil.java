package com.xoffle.tasktracker.fb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import com.facebook.Response;
import com.inkling.facebook.FacebookLifecycleHelper;
import com.inkling.facebook.FacebookTaskCompleteListener;
import com.inkling.facebook.ResponseUtil;
import com.inkling.fbdata.ScoreNode;
import com.inkling.fbdata.UserNode;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.util.Utils;

import org.json.JSONObject;

import java.util.List;

public class FacebookUtil {

    private static FacebookUtil instance;
    // private final static String URL_GAME_LIST = new String("market://search?q=pub:Inkling Arts");
    private FacebookLifecycleHelper facebookLifecycleHelper;
    private Activity activity;

    private FacebookUtil(Activity activity) {
        this.activity = activity;
        facebookLifecycleHelper = new FacebookLifecycleHelper(activity, true);
    }

    public static FacebookUtil getInstance(Activity activity) {
        return instance == null ? instance = new FacebookUtil(activity) : instance;
    }

    public static void destroy() {
        instance = null;
    }

    public void getFbUsers(final OnFbFriendFetchListner onFbFriendFetchListner) {

        facebookLifecycleHelper.getAllScore(activity.getResources().getString(R.string.fb_app_id), new FacebookTaskCompleteListener() {

            @Override
            public void onComplete(Response response, JSONObject jsonObject) {

                if (response != null && ResponseUtil.getError(response) == null) {
                    List<ScoreNode> list = ResponseUtil.parseScore(response);

                    onFbFriendFetchListner.onComplete(list);
                }
            }
        });
    }

    public void loginToFb(final OnUserLoginToFb listener) {

        facebookLifecycleHelper.getUserInfo(new FacebookTaskCompleteListener() {
            @Override
            public void onComplete(Response response, JSONObject jsonObject) {
                if (response == null || response.getError() != null) {
                    listener.onLoginToFb(null);
                    return;
                }

                UserNode userNode = ResponseUtil.parseUser(jsonObject);
                listener.onLoginToFb(userNode);
            }
        });
    }

    public void saveScore(int score, final OnScoreSave onScoreSave) {

        facebookLifecycleHelper.saveScore(100, new FacebookTaskCompleteListener() {

            @Override
            public void onComplete(Response response, JSONObject jsonObject) {
                if (response != null && response.getError() == null) {
                    onScoreSave.onComplete();
                }
            }
        });
    }

    public void inviteFriends(final String title, final String msg, final OnInviteFriendListner listener) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                facebookLifecycleHelper.inviteFriend(title, msg, new FacebookTaskCompleteListener() {

                    @Override
                    public void onComplete(Response response, JSONObject jsonObject) {
                        if (jsonObject != null) {
                            List<String> idList = ResponseUtil.getFriendIdList(jsonObject);
                            if (idList != null) {
                                listener.onInveiteFriends(idList);
                            }
                        }
                    }
                });
            }
        });
    }

    // @Override
    // public void downloadImage(final String userId, final int width, final int height, final OnImageDownloadListner listner) {
    // activity.runOnUiThread(new Runnable() {
    //
    // @Override
    // public void run() {
    // DownloadUtil.downloadImage(userId, width, height, new OnDownloadListener() {
    //
    // @Override
    // public void onDownloadComplete(Bitmap bitmap) {
    //
    // if (bitmap != null) {
    // ByteArrayOutputStream stream = new ByteArrayOutputStream();
    // bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    // byte[] bytes = stream.toByteArray();
    //
    // listner.onDownloadComplete(bytes);
    // }
    // }
    // });
    //
    // }
    // });
    //
    // }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookLifecycleHelper.onActivityResult(requestCode, resultCode, data);
    }

    public void onResume() {
        facebookLifecycleHelper.onResume();
    }

    public void onPause() {
        facebookLifecycleHelper.onPause();
    }

    public void onStop() {
        facebookLifecycleHelper.onStop();
    }

    public void onDestroy() {
        facebookLifecycleHelper.onDestroy();
    }

    public void getMoreApps() {
        if (!Utils.isNetworkAvailble(activity)) {
            Utils.showToast(activity, "Internet not connected.");
            return;
        }

        // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL_GAME_LIST));
        // activity.startActivity(intent);
    }

    public void shareViaMsg() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, activity.getResources().getString(R.string.share_msg));
        activity.startActivity(Intent.createChooser(i, "Share Via.."));
    }

    public void postStatus() {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Facebook");
                builder.setMessage("Would you like to post on Facebook and share your experience?");

                builder.setNegativeButton("Yes", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        postFeedMsg(activity.getString(R.string.post_msg));
                    }
                });
                builder.setPositiveButton("No", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.setCancelable(false);
                builder.create().show();

            }
        });

    }

    public void postFeed(final String msg) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                postFeedMsg(msg);
            }
        });
    }

    private void postFeedMsg(String desc) {
        // String string = activity.getString(R.string.desc);
        // facebookLifecycleHelper.postFeed("TaskTracker", activity.getString(R.string.img_url), activity.getString(R.string.url), activity.getString(R.string.caption), desc, new FacebookTaskCompleteListener() {
        //
        // @Override
        // public void onComplete(final Response response, final JSONObject jsonObject) {
        // if (response.getError() == null)
        // showToast("Successfully Posted");
        // }
        // });
    }

    public void reviewApp() {
        if (Utils.isNetworkAvailble(activity)) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(buildUrl(activity.getString(R.string.review_url), "")));
                    //
                    // DataStorage.getInstance(activity).appReviewed();
                    // activity.startActivity(intent);
                }
            });
        } else {
            Utils.showToast(activity, "No Internet Connection");
        }

    }

    public void finishGame() {
        activity.finish();
    }

    public interface OnFbFriendFetchListner {
        void onComplete(List<ScoreNode> list);
    }

    public interface OnUserLoginToFb {
        void onLoginToFb(UserNode userNode);
    }

    public interface OnScoreSave {
        void onComplete();
    }

    public interface OnInviteFriendListner {
        void onInveiteFriends(List<String> idList);
    }
}