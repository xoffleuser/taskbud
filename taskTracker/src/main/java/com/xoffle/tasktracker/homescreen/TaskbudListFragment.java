package com.xoffle.tasktracker.homescreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.custom.ImageDialog;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.view.MyFragment;

import java.util.ArrayList;

/**
 * Created by ravi on 17/5/15.
 */
public class TaskbudListFragment extends MyFragment implements TaskBudListAdapter.OnTaskBudClickListener {

    public static boolean isActive;
    private ListView listView;
    private TaskBudListAdapter taskBudAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.task_bud_list_fragment, container, false);

        setToolbar(view);

        setListView(view);

        setSearchView(view);

        return view;
    }

    private void setSearchView(View view) {
        EditText searchEditView = (EditText) view.findViewById(R.id.taskBudListFragmentEditText);
        searchEditView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // System.out.println("cs = " + s + ", start = " + start + ", before = " + before + ", count = " + count);

                if (s.length() != 0) {
                    taskBudAdapter.searchText(s.toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.length() == 0) {
                    taskBudAdapter.reset();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        isActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();

        isActive = false;
    }

    private void setToolbar(View view) {
        initMiddleTextToolbar(view, R.id.taskBudListFragmentToolbar, getActivity().getString(R.string.task_buds));
    }

    private void setListView(View view) {
        listView = (ListView) view.findViewById(R.id.taskBudListFragmentListView);
        taskBudAdapter = new TaskBudListAdapter(getActivity(), R.layout.task_bud_list_fragment, this);
        listView.setAdapter(taskBudAdapter);

        loadFriends();
    }

    private void loadFriends() {

        ArrayList<String> idList = getExistingUserIds();

        for (TaskBudBean taskBudBean : BackendHandler.getInstance(getActivity()).getAllTaskBuds()) {
            if (!idList.contains(taskBudBean.getMemberId())) {
                taskBudAdapter.addTaskBud(taskBudBean);
            }
        }
    }

    private ArrayList<String> getExistingUserIds() {
        ArrayList<String> list = new ArrayList<>();
        list.add(DataStorage.getInstance(getActivity()).getUserId());
        return list;
    }

    @Override
    public void onUserImageClick(TaskBudBean taskBud, String tag) {
        ImageDialog imageDialog = ImageDialog.newInstance(taskBud.getDisplayName(),taskBud.getImageId(),tag);
        imageDialog.show(getFragmentManager());
    }
}