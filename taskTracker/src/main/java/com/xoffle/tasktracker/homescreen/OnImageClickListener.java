package com.xoffle.tasktracker.homescreen;

import android.view.View;

import com.xoffle.tasktracker.group.Group;

/**
 * Created by ravi on 9/4/15.
 */
public interface OnImageClickListener {

    void onImageClick(Group group);
}
