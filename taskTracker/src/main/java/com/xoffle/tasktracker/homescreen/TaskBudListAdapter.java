package com.xoffle.tasktracker.homescreen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.TextUtil;

import java.util.ArrayList;

public class TaskBudListAdapter extends ArrayAdapter<TaskBudBean> {

    private final OnTaskBudClickListener clickListener;
    private LayoutInflater inflater;
    private ArrayList<TaskBudBean> allData;

    private String searchText = "";

    public TaskBudListAdapter(Context context, int resource, OnTaskBudClickListener clickListener) {
        super(context, resource);
        inflater = LayoutInflater.from(context);
        allData = new ArrayList<>();

        this.clickListener = clickListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TaskBudBean taskBud = getItem(position);

        TaskBudHolder friendHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.task_bud_view, parent, false);

            friendHolder = new TaskBudHolder(getContext(), convertView, taskBud, clickListener, searchText);

            convertView.setTag(friendHolder);
        } else {
            friendHolder = (TaskBudHolder) convertView.getTag();
            friendHolder.setData(taskBud, clickListener, searchText);
        }

        return convertView;
    }

    public void addTaskBud(TaskBudBean taskBudBean) {
        if (isContain(taskBudBean)) {
            return;
        }
        add(taskBudBean);
        allData.add(taskBudBean);
    }

    private boolean isContain(TaskBudBean taskBudBean) {
        for (TaskBudBean f : getAllItems()) {
            if (f.getMemberId().equals(taskBudBean.getMemberId())) {
                return true;
            }
        }

        return false;
    }

    public ArrayList<TaskBudBean> getAllItems() {
        ArrayList<TaskBudBean> list = new ArrayList<>();
        for (int i = getCount() - 1; i >= 0; i--) {
            list.add(getItem(i));
        }

        return list;
    }

    public void reset() {

        searchText = "";

        clear();

        addAll(allData);

        notifyDataSetChanged();
    }

    public void searchText(String s) {

        if (s == null || s.length() == 0) {
            return;
        }
        searchText = s;

        clear();

        for (TaskBudBean taskBudBean : allData) {
            if (taskBudBean.getDisplayName().toLowerCase().contains(s.toLowerCase()) || (taskBudBean.getStatus() != null && taskBudBean.getStatus().toLowerCase().contains(s.toLowerCase()))) {
                add(taskBudBean);
            }
        }

        notifyDataSetChanged();
    }

    interface OnTaskBudClickListener {
        void onUserImageClick(TaskBudBean taskBud, String tag);
    }

    static class TaskBudHolder implements View.OnClickListener {

        private ImageView userImage;

        private TextView userNameTextView;
        private TextView statusTextView;

        private TaskBudBean taskBud;
        private OnTaskBudClickListener onTaskBudClickListener;
        private Context context;

        TaskBudHolder(Context context, View view, TaskBudBean taskBudBean, OnTaskBudClickListener onTaskBudClickListener, String searchText) {
            this.context = context;
            findViewsById(view);
            setData(taskBudBean, onTaskBudClickListener, searchText);
        }

        private void findViewsById(View view) {
            userImage = (ImageView) view.findViewById(R.id.taskBudUserImageView);
            userImage.setFocusable(false);
            userImage.setOnClickListener(this);

            userNameTextView = ((TextView) view.findViewById(R.id.taskBudTextViewName));
            statusTextView = ((TextView) view.findViewById(R.id.taskBudTextViewStatus));
        }


        public void setData(TaskBudBean taskBud, OnTaskBudClickListener onTaskBudClickListener, String searchText) {
            this.taskBud = taskBud;
            this.onTaskBudClickListener = onTaskBudClickListener;

            TextUtil.setSpannableText(userNameTextView, taskBud.getDisplayName(), searchText);

            TextUtil.setSpannableText(statusTextView, taskBud.getStatus(), searchText);

            ImageUtil.setImage(context, userImage, taskBud.getImageId());
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.taskBudUserImageView:
                    onTaskBudClickListener.onUserImageClick(taskBud, (String) userImage.getTag());
                    break;
            }
        }
    }
}