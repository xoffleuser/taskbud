package com.xoffle.tasktracker.homescreen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.View;

import com.inkling.fbdata.ScoreNode;
import com.inkling.fbdata.UserNode;
import com.inkling.leaderboard.AppTaskBuds;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.db.DBConstants;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.fb.FacebookUtil;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.group.GroupListFragment;
import com.xoffle.tasktracker.group.OnGroupLoadListener;
import com.xoffle.tasktracker.image.ImageController;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.userauth.UserauthActivity;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;

import java.util.List;

public class MainActivity extends ActionBarActivity {

    public static final String DATA_RECEIVER_ACTION = "com.xoffle.tasktracker.homescreen.datareceiver";
    private static final int REQUEST_CODE = 1;
    public final String TAG = "GROUP_FRAGMENT_TAG";
    private BroadcastReceiver dataReceiver;
    private NavigationFragment navigationFragment;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        checkUpdate();

        setNavigator();

        GroupListFragment fragment = new GroupListFragment();
        addFragment(fragment);

        downloadData(getIntent(), fragment);

        createMyGroup(fragment);
    }

    private void checkUpdate() {
        if (DataStorage.getInstance(this).isUpdateAvailable()) {
            updateApp();
            DataStorage.getInstance(this).setIsUpdateAvailable(false);
        }
    }

    private void updateApp() {
        DBController.getInstance(this).updateDB();
    }

    private void createMyGroup(GroupListFragment fragment) {
        if (DataStorage.getInstance(this).isMyGroupCreated()) {
            return;
        }

        BackendHandler.getInstance(this).loadMyGroup(fragment);
    }

    private void downloadData(Intent intent, OnGroupLoadListener onGroupLoadListener) {

        loadFriends();
        loadAllUpdatesImages();

        if (DataStorage.getInstance(this).getUsername() == null) {
            LeaderBoard.loadAppMember(DataStorage.getInstance(this).getUserId(), new DataFetchListener<AppTaskBuds>() {
                @Override
                public void onFetchComplete(AppTaskBuds object, boolean isLoaded) {
                    if (object != null && Utils.isNull(object.getTbUserName())) {
                        showPopupForUsername();
                    }
                }

                @Override
                public void onListFetchComplete(List<AppTaskBuds> objects) {

                }
            });
        }

        boolean isFirstTimeLogging = intent.getBooleanExtra(UserauthActivity.IS_LOGGIN_FIRST_TIME, false);
        if (isFirstTimeLogging) {
//            showPopupForUsername();
            downloadPreviousData(onGroupLoadListener);
        } else {
        }
    }

    private void loadAllUpdatesImages() {
        new ImageController().downloadAllUpdatedImages(this);
    }

    private void showPopupForUsername() {
        UsernameDialog usernameDialog = new UsernameDialog();
        usernameDialog.show(getSupportFragmentManager());
    }

    private void setNavigator() {
//        ListView listView = (ListView) findViewById(R.id.mainActivityListView);
//
//        String[] arr = {"one", "Two", "three"};
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, arr);
//
//        View view = LayoutInflater.from(this).inflate(R.layout.profile_header, null, false);
//        listView.addHeaderView(view);
//        listView.setAdapter(adapter);

        drawerLayout = (DrawerLayout) findViewById(R.id.mainActivityDrawerLayout);

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

                navigationFragment.refreshData();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        navigationFragment = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.mainActivityNavigationFragment);

    }

    public void openDrawerLayout() {
        if (!drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    public void closeDrawerLayout() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    public void addFragment(Fragment fragment) {
        addFragment(fragment, false);
    }

    public void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        Fragment previousFragment = fragmentManager.findFragmentByTag(TAG);
        if (previousFragment != null) {
            transaction.hide(previousFragment);
        }

        transaction.add(R.id.mainActivityFrameLayout, fragment, TAG);
        if (addToBackStack) {
            transaction.addToBackStack(TAG);
        }
        transaction.commit();
    }

    private void init() {
        DataStorage.getInstance(this);
    }

    private void downloadPreviousData(final OnGroupLoadListener onGroupLoadListener) {
        Intent intent = new Intent(this, DataFetchService.class);
        intent.putExtra(DataFetchService.ACTION, DataFetchService.LOAD_ALL_DATA);
        startService(intent);

        dataReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String groupId = intent.getStringExtra(DBConstants.GROUP_ID);

                onGroupLoadListener.onGroupLoaded(loadGroupFromSQLite(groupId));
            }
        };

        registerReceiver(dataReceiver, new IntentFilter(DATA_RECEIVER_ACTION));
    }

    /**
     * load the group from SQLite
     * @param groupId
     */
    private Group loadGroupFromSQLite(String groupId) {
        if (Utils.isNull(groupId)) {
            return null;
        }

        Cursor cursor = DBController.getInstance(this).loadGroup(groupId);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

//          Cursor mCursor = DBConhitroller.getInstance(this).getGroupMembersId(groupId);
            return new Group(this, cursor);
//          onGroupLoadListener.onGroupLoaded(group);
        }
        return null;
    }

    private void loadFriends() {

        FacebookUtil fbUtil = FacebookUtil.getInstance(this);

        fbUtil.getFbUsers(new FacebookUtil.OnFbFriendFetchListner() {

            @Override
            public void onComplete(List<ScoreNode> list) {

                if (list != null && list.size() > 0) {
                    System.out.println("list " + list.size());

                    DBController dbController = DBController.getInstance(getApplicationContext());

                    for (int i = list.size() - 1; i >= 0; i--) {
                        UserNode userNode = list.get(i).getUserNode();
                        String id = userNode.getUserId();
                        String name = userNode.getUserName();

                        Debug.debug("taskBudBean load friends");

                        dbController.addFriends(id, name);

                        DBController.getInstance(MainActivity.this).updateTaskBudFromParse(id);
                    }
                } else {
                }
            }
        });
    }

//    private void createNewGroup() {
//        if (!Utils.isNetworkAvailble(this)) {
//            Utils.showToast(this, getString(R.string.no_internet_connection));
//            return;
//        }
//
//        Intent intent = new Intent(this, CreateGroupActivity.class);
//        startActivityForResult(intent, REQUEST_CODE);
//    }
}