package com.xoffle.tasktracker.homescreen;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.inkling.leaderboard.AppTaskBuds;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.TextValidator;
import com.xoffle.tasktracker.util.Utils;

import java.util.List;

/**
 * Created by ravi on 17/6/15.
 */
public class UsernameDialog extends DialogFragment implements View.OnClickListener {

    private static final String TAG = "com.xoffle.tasktracker.homescreen.UsernameDialog.TAG";
    private EditText usernameEt;
    private TextView errorTv;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(STYLE_NO_FRAME, STYLE_NO_TITLE);
        setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(170, 0, 0, 0)));

        View view = inflater.inflate(R.layout.username_dialog, container, false);

        usernameEt = (EditText) view.findViewById(R.id.usernameDialogEditTextUsername);
        errorTv = (TextView) view.findViewById(R.id.usernameDialogTextViewErrorMsg);
        progressBar = (ProgressBar) view.findViewById(R.id.usernameDialogProgressBar);

        view.findViewById(R.id.usernameDialogButtonOk).setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.usernameDialogButtonOk:
                onOkButtonPressed();
                break;
        }
    }

    private void onOkButtonPressed() {
        String username = usernameEt.getText().toString();

        if (isValidUserName(username)) {
            checkOnParse(username);
        } else {
            Toast.makeText(getActivity(), getActivity().getString(R.string.username_error_msg), Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isValidUserName(String username) {
        if (Utils.isNull(username)) {
            return false;
        }

        return TextValidator.isValidUserName(username);
    }

    private void checkOnParse(final String username) {

        errorTv.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        LeaderBoard.checkAvailability(username, new DataFetchListener<AppTaskBuds>() {
            @Override
            public void onFetchComplete(AppTaskBuds object, boolean isLoaded) {

                if (object != null) {
                    progressBar.setVisibility(View.GONE);
                    showErrorMsg();
                } else {
                    onSpaceAvailable(username);
                }
            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> objects) {

            }
        });
    }

    private void onSpaceAvailable(final String username) {

        LeaderBoard.updateUsername(DataStorage.getInstance(getActivity()).getUserId(), username, new DataFetchListener<AppTaskBuds>() {
            @Override
            public void onFetchComplete(AppTaskBuds object, boolean isLoaded) {

                progressBar.setVisibility(View.GONE);

                if (object == null) {
                    showErrorMsg();
                } else {
                    onSuccessfullyUpdate(username);
                }
            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> objects) {

            }
        });
    }

    private void onSuccessfullyUpdate(String username) {
        Toast.makeText(getActivity(), "Username is successfully updated...", Toast.LENGTH_SHORT).show();
        DataStorage.getInstance(getActivity()).setUsername(username);
        dismiss();
    }

    private void showErrorMsg() {
        errorTv.setVisibility(View.VISIBLE);
    }

    public void show(FragmentManager fragmentManager) {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment preFragment = fragmentManager.findFragmentByTag(TAG);
        if (preFragment != null) {
            fragmentTransaction.remove(preFragment);
        }

        show(fragmentManager, TAG);
    }
}