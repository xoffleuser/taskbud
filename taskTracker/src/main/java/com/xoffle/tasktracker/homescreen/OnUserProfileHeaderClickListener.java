package com.xoffle.tasktracker.homescreen;

/**
 * Created by ravi on 13/8/15.
 */
public interface OnUserProfileHeaderClickListener {
    void onHeaderClick();

    void onHeaderImageClick();

    void onHeaderDisplayClick();
}
