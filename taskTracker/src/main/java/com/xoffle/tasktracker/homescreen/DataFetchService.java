package com.xoffle.tasktracker.homescreen;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;

import com.inkling.leaderboard.AppGroup;
import com.inkling.leaderboard.AppGroupTask;
import com.inkling.leaderboard.AppImages;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.ParseImageListener;
import com.inkling.leaderboard.Task;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.db.DBConstants;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.image.ParseImageHandler;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;

import java.util.List;

public class DataFetchService extends IntentService {

    public static final String ACTION = "ACTION";
    public static final int LOAD_FB_FRIENDS = 1;
    public static final int LOAD_ALL_DATA = 2;

    public DataFetchService() {
        super("DataFetchService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        int action = intent.getIntExtra(ACTION, 0);

        Debug.debug("action = " + action);

        switch (action) {
            case LOAD_ALL_DATA:
                loadAllData();
                break;
            default:
                break;
        }
    }

    private void loadAllData() {
        DataStorage store = DataStorage.getInstance(null);
        String adminId = store.getUserId();

        LeaderBoard.fetchAllMyGroups(adminId, new DataFetchListener<AppGroup>() {
            @Override
            public void onFetchComplete(AppGroup object, boolean isLoaded) {
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {
                if (objects != null && objects.size() > 0) {

//                    Debug.debug("loadAllData() size= "+objects.size());
                    for (int i = 0; i < objects.size(); i++) {
                        AppGroup appGroup = objects.get(i);

                        addGroupToDB(appGroup);

                        loadTaskOfGroup(appGroup);

                        downloadImage(appGroup.getImageObjectId());
                    }
                } else {
                    Debug.debug("error in loading all group " + objects);
                }
            }
        });
    }

    private void downloadImage(final String imageObjectId) {
        ParseImageHandler.downloadImage(imageObjectId, new ParseImageListener() {
            @Override
            public void onImageSaveOrUpdate(AppImages appImages) {
            }

            @Override
            public void onImageDownload(byte[] bytes) {

                Bitmap bitmap = ImageUtil.convertToBitmap(bytes);
                if (bitmap != null) {

                    ImageUtil.saveImage(getApplicationContext(), bitmap, imageObjectId);
                }
            }
        });
    }

    /**
     * send broadcast to calling activity (MainActivity)
     * @param appGroup
     *         new loaded group that is going to notify to group_create activity
     */
    private void broadcastToActivity(AppGroup appGroup) {
        Intent intent = new Intent(MainActivity.DATA_RECEIVER_ACTION);
        intent.putExtra(DBConstants.GROUP_ID, appGroup.getObjectId());
        sendBroadcast(intent);
    }

    /**
     * load all task related to this group from parse and store to sqlite db
     * @param appGroup
     */
    private void loadTaskOfGroup(final AppGroup appGroup) {
        LeaderBoard.fetchAllTaskOfGroup(appGroup.getObjectId(), new DataFetchListener<AppGroupTask>() {
            @Override
            public void onFetchComplete(AppGroupTask object, boolean isLoaded) {
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> appTasks) {
                if (appTasks != null && appTasks.size() > 0) {

                    for (int i = appTasks.size() - 1; i >= 0; i--) {
                        if (!appTasks.get(i).getIsDeleted()) {
                            addTaskToSQLite(appGroup.getObjectId(), appTasks.get(i));
                        }
                    }
                }
/* notify on new group and its task loaded */
                broadcastToActivity(appGroup);
            }


        });
    }

    /**
     * add the task to sqlite db
     * @param groupId
     * @param appGroupTask
     */
    private void addTaskToSQLite(String groupId, AppGroupTask appGroupTask) {
        Task task = new Task(appGroupTask);

        Debug.debug("App GroupTask = " + appGroupTask.toString());

        DBController.getInstance(DataFetchService.this).createTask(groupId, task);
    }

    /**
     * add new loaded group from parse to SQLite db
     * @param appGroup
     *         newly loaded AppGroup
     */
    private void addGroupToDB(AppGroup appGroup) {
        BackendHandler.getInstance(DataFetchService.this).addGroupToSQLite(appGroup, null);
    }
}