package com.xoffle.tasktracker.homescreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.setting.SettingsActivity;
import com.xoffle.tasktracker.user.UserDetailFragment;
import com.xoffle.tasktracker.userauth.UserauthActivity;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.Utils;

import static com.xoffle.tasktracker.homescreen.NavigationFragment.NavigationEnumItem.ITEM_TASK_BUDS;

/**
 * Created by ravi on 11/8/15.
 */
public class NavigationFragment extends Fragment implements OnUserProfileHeaderClickListener, AdapterView.OnItemClickListener {

    private static final int ID_TASK_BUD = 1;
    private static final int ID_SETTING = 2;
    private static final int ID_LIKE = 3;
    private static final int ID_SHARE = 4;
    private static final int ID_LOGOUT = 5;

    private ProfileHeader profileHeader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ListView view = (ListView) inflater.inflate(R.layout.navigation_fragment, container, false);

        NavigationAdapter adapter = new NavigationAdapter(getActivity(), R.layout.navigation_fragment);

        adapter.add(new NavigationItem(ITEM_TASK_BUDS.setTitle(getActivity().getString(R.string.task_buds))));
        adapter.add(new NavigationItem(NavigationEnumItem.ITEM_SETTING.setTitle(getActivity().getString(R.string.setting))));
        adapter.add(new NavigationItem(NavigationEnumItem.ITEM_LIKE.setTitle(getActivity().getString(R.string.like))));
        adapter.add(new NavigationItem(NavigationEnumItem.ITEM_SHARE.setTitle(getActivity().getString(R.string.share))));
        adapter.add(new NavigationItem(NavigationEnumItem.ITEM_LOGOUT.setTitle(getActivity().getString(R.string.logout))));

        View header = LayoutInflater.from(getActivity()).inflate(R.layout.profile_header, null, false);

        setDataToHeader(header);

        view.addHeaderView(header);
        view.setAdapter(adapter);

        view.setOnItemClickListener(this);

        return view;
    }

    private void setDataToHeader(View header) {

        String memberId = DataStorage.getInstance(getActivity()).getUserId();
        TaskBudBean taskBudBean = DBController.getInstance(getActivity()).getTaskBud(memberId);
        profileHeader = new ProfileHeader(header, taskBudBean, this);
    }

    @Override
    public void onHeaderClick() {

        ((MainActivity) getActivity()).closeDrawerLayout();

        if (UserDetailFragment.isActive) {
            return;
        }

        UserDetailFragment fragment = UserDetailFragment.newInstance(profileHeader.taskBudBean);
        ((MainActivity) getActivity()).addFragment(fragment, true);
    }

    @Override
    public void onHeaderImageClick() {
        onHeaderClick();
    }

    @Override
    public void onHeaderDisplayClick() {

    }

    public void refreshData() {
        String memberId = DataStorage.getInstance(getActivity()).getUserId();
        TaskBudBean taskBudBean = DBController.getInstance(getActivity()).getTaskBud(memberId);

        profileHeader.refressData(taskBudBean);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case ID_TASK_BUD:
                showTaskBuds();
                break;
            case ID_SETTING:
//                openSetting();
                break;
            case ID_LIKE:
                likeApp();
                break;
            case ID_SHARE:
                shareViaApp();
                break;
            case ID_LOGOUT:
                logout();
                break;
        }
    }

    private void logout() {
        for (Fragment fragment : getFragmentManager().getFragments()) {
            getFragmentManager().beginTransaction().remove(fragment);
        }

        DataStorage.getInstance(getActivity()).setUserId("");
        DataStorage.getInstance(getActivity()).setDisplayName("");

        deleteLocalDb();

        Intent intent = new Intent(getActivity(), UserauthActivity.class);
        intent.putExtra(Constant.SHOULD_LOGOUT, true);
        startActivity(intent);

        getActivity().finish();
    }

    private void deleteLocalDb() {
        BackendHandler.getInstance(getActivity()).deleteLocalDatabase();
    }

    private void shareViaApp() {
        Utils.shareVia(getActivity());
    }

    private void likeApp() {
        Utils.likeApp(getActivity());
    }

    private void openSetting() {
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent);
    }

    private void showTaskBuds() {

        ((MainActivity) getActivity()).closeDrawerLayout();

        if (TaskbudListFragment.isActive) {
            return;
        }

        TaskbudListFragment fragment = new TaskbudListFragment();
        ((MainActivity) getActivity()).addFragment(fragment, true);

//        Intent intent = new Intent(getActivity(), SelectFriendsActivity.class);
//        setExistingTaskBud(intent);
//        startActivity(intent);
    }

//    private void setExistingTaskBud(Intent intent) {
//        ArrayList<String> list = new ArrayList<>();
//        list.add(DataStorage.getInstance(getActivity()).getUserId());
//
//        Bundle bundle = new Bundle();
//        bundle.putStringArrayList(Constant.EXISTING_USER, list);
//
//        intent.putExtras(bundle);
//    }

    enum NavigationEnumItem {


        ITEM_TASK_BUDS("", ID_TASK_BUD),
        ITEM_SETTING("", ID_SETTING),
        ITEM_LIKE("", ID_LIKE),
        ITEM_SHARE("", ID_SHARE),
        ITEM_LOGOUT("", ID_LOGOUT);

        private String title;
        private int id;

        NavigationEnumItem(String title, int id) {
            this.title = title;
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public NavigationEnumItem setTitle(String title) {
            this.title = title;
            return this;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    class NavigationAdapter extends ArrayAdapter<NavigationItem> {

        public NavigationAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            NavigationItem item = getItem(position);

            NavigationItemHolder itemHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.navigation_item, null, false);
                itemHolder = new NavigationItemHolder(convertView, getContext(), item);
                convertView.setTag(itemHolder);
            } else {
                itemHolder = (NavigationItemHolder) convertView.getTag();
                itemHolder.setData(getContext(), item);
            }

            return convertView;
        }

        class NavigationItemHolder {
            private TextView titleTextView;
            private NavigationItem item;

            NavigationItemHolder(View view, Context context, NavigationItem item) {
                findViewsById(view);
                setData(context, item);
            }

            private void findViewsById(View view) {
                titleTextView = (TextView) view.findViewById(R.id.navigationItem);
            }

            public void setData(Context context, NavigationItem item) {
                this.item = item;

                titleTextView.setText(item.enumItem.getTitle());
            }
        }
    }

    class ProfileHeader implements View.OnClickListener {
        TaskBudBean taskBudBean;
        TextView displayNameTextView;
        TextView userNameTextView;
        ImageView userProfilePic;
        private OnUserProfileHeaderClickListener headerClickListener;

        ProfileHeader(View headerView, TaskBudBean taskBudBean, OnUserProfileHeaderClickListener headerClickListener) {
            this.headerClickListener = headerClickListener;

            findViewsById(headerView);

            setData(taskBudBean);

            headerView.setOnClickListener(this);
            userProfilePic.setOnClickListener(this);
//            displayNameTextView.setOnClickListener(this);
        }

        void updateHeader(TaskBudBean taskBudBean) {
            setData(taskBudBean);
        }

        private void setData(TaskBudBean taskBudBean) {
            this.taskBudBean = taskBudBean;
            if (taskBudBean == null) {
                return;
            }

            displayNameTextView.setText(taskBudBean.getDisplayName());
            userNameTextView.setText(taskBudBean.getUserName());

            ImageUtil.setImageFromStorage(userProfilePic, taskBudBean.getImageId());
        }

        private void findViewsById(View headerView) {
            userProfilePic = (ImageView) headerView.findViewById(R.id.profileHeaderImageView);
            displayNameTextView = (TextView) headerView.findViewById(R.id.profileHeaderDisplayName);
            userNameTextView = (TextView) headerView.findViewById(R.id.profileHeaderUsername);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.profileHeader:
                    headerClickListener.onHeaderClick();
                    break;

                case R.id.profileHeaderImageView:
                    headerClickListener.onHeaderImageClick();
                    break;
            }

        }

        public void refressData(TaskBudBean taskBudBean) {
            setData(taskBudBean);
        }
    }

    class NavigationItem {
        NavigationEnumItem enumItem;

        NavigationItem(NavigationEnumItem enumItem) {
            this.enumItem = enumItem;
        }
    }
}