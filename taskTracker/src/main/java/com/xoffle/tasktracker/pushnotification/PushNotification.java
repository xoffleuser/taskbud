package com.xoffle.tasktracker.pushnotification;

import com.inkling.leaderboard.AppGroup;
import com.inkling.leaderboard.Installation;
import com.inkling.leaderboard.LeaderBoard;
import com.xoffle.tasktracker.group.Group;
import com.xoffle.tasktracker.json.JsonCreater;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Debug;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravi on 11/4/15.
 */
public class PushNotification {

    public static void notifyOnTaskUpdate(Group group, String taskCreatersId) {
        JSONObject json = JsonCreater.createJsonForTaskUpdate(group.getGroupId(), taskCreatersId,true);

        final List<String> sList = group.getMemberIds();
        sendNotification(sList, json);

    }

    private static void sendNotification(final String userId, JSONObject json) {
        LeaderBoard.sendJsonNotification(userId, json, new Installation.MessageSendCallback() {
            @Override
            public void onComplete(boolean isSuccess, String msg) {
                if (isSuccess) {
                    Debug.debug("successfully message send to " + userId);
                } else {
                    Debug.debug("failed to send notification to " + userId);
                }
            }
        });
    }

    public static void notifyOnTaskDelete(Group group, String taskId) {
        JSONObject json = JsonCreater.createJsonForTaskDelete(taskId);

        final List<String> sList = group.getMemberIds();

        sendNotification(sList, json);
    }
//
//    public interface OnNotificationSendListener {
//        void onMsgSent();
//    }

    public static void sendGroupCreateNotification(final AppGroup appGroup) {
        Debug.debug("PushNotification.sendGroupCreateNotification()");
        JSONObject json = JsonCreater.createJsonForGroupCreate(appGroup);

        String adminId = appGroup.getAdminId();

        final List<String> sList = appGroup.getGroupMembers();

        for (int i = sList.size() - 1; i >= 0; i--) {

            final String userId = sList.get(i);
            if (userId.equals(adminId)) {
                continue;
            }

            sendNotification(userId, json);
        }
    }

    public static void sendNotification(final List<String> userIds, JSONObject json) {
        String adminId = DataStorage.getInstance(null).getUserId();

        for (int i = userIds.size() - 1; i >= 0; i--) {

            final String userId = userIds.get(i);
            if (userId.equals(adminId)) {
                continue;
            }

            sendNotification(userId, json);
        }
    }

    public static void notifyOnTaskDeliverToGroupBud(String groupId,String taskId,String seenById, ArrayList<String> membersIds,int status) {
        JSONObject json = JsonCreater.createJsonForTaskUpdate(groupId, taskId,false);

        for (int i = membersIds.size() - 1; i >= 0; i--) {
            if (!seenById.equals(membersIds.get(i))) {
                sendNotification(membersIds.get(i), json);
            }
        }
    }

    public static void notifyOnGroupUpdate(String groupId, String memberId, boolean shouldNotify) {
        JSONObject json = JsonCreater.createJsonForUpdate(groupId,shouldNotify);
        sendNotification(memberId,json);
    }

}