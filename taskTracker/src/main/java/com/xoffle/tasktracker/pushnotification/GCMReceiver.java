package com.xoffle.tasktracker.pushnotification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.parse.ParsePushBroadcastReceiver;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.json.JsonCreater;
import com.xoffle.tasktracker.json.JsonParser;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ravi on 12/4/15.
 */
public class GCMReceiver extends ParsePushBroadcastReceiver {

    private static final String PARSE_KEY = "com.parse.Data";
    private static final String OPEN = "com.parse.push.intent.RECEIVE";
    private static final CharSequence TASK_TRACKER = "Task Tracker";

    @Override
    public void onReceive(Context context, Intent intent) {
//        super.onReceive(context, intent);

        Bundle extras = intent.getExtras();
        String message = extras != null ? extras.getString(PARSE_KEY) : "";

        if (OPEN.equals(intent.getAction())) {
//            showNotification(message, context);
            filterJson(message, context);
        }

        System.out.println("GCMReceiver.onPushReceive() = " + message);
    }

    private void showNotification(String alert, Context context) {
//        String alert = getAlertFromMessage(message, context);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
//        Notification.Builder builder = new Notification.Builder(context);
//        builder.setVisibility(Notification.VISIBILITY_PUBLIC);

        builder.setSmallIcon(R.drawable.ic_launcher).setContentTitle(TASK_TRACKER).setContentText(alert).setNumber(2);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        Intent resultIntent = new Intent(context, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        stackBuilder.addNextIntent(resultIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent).setAutoCancel(true);
//        builder.setFullScreenIntent(pendingIntent,true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, builder.build());
    }

    private String getAlertFromMessage(String message, Context context) {
        String alert = "";
        try {
            JSONObject jsonObject = new JSONObject(message);
            alert = jsonObject.getString(JsonCreater.ALERT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int updateCount = DataStorage.getInstance(context).getUpdateMsgCounter();
        if (updateCount == 0)
            return alert;

        return (updateCount + 1) + " new messages are received";
    }

    private void filterJson(String message, Context context) {

        String alert = getAlertFromMessage(message, context);

        if (!Utils.isNull(alert)) {
            showNotification(alert, context);
        }

        System.out.println("message = " + message);
        System.out.println("messageToString = " + message.toString());

        DataStorage store = DataStorage.getInstance(context);
        store.addUpdateMsg(message);
        store.setUpdateMsgCounter(store.getUpdateMsgCounter() + 1);

        JsonParser.parseAppUpdateFromNotification(context, message, null);
    }
}