package com.xoffle.tasktracker.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xoffle.tasktracker.R;

import java.util.ArrayList;

/**
 * Created by ravi on 23/6/15.
 */
public class SpinnerAdapter extends BaseAdapter {

    private final Context context;
    private ArrayList<String> list;

    public SpinnerAdapter(Context context) {

        this.context = context;
        list = new ArrayList<>();

        list.add("Default");
        list.add("One");
        list.add("Two");
        list.add("Three");
        list.add("Four");
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.check_box, null, false);
        }

        if(position==0) {
            TextView textView = new TextView(context);
            textView.setText("Filter");
            return textView;
        }
        return convertView;
    }
}