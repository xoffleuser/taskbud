package com.xoffle.tasktracker.test;

import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;

public class AsynTest {

	int[][] values = { { 1, 10 }, { 2, 20 }, { 3, 30 } };
	ArrayList<int[][]> list = new ArrayList<int[][]>();

	public AsynTest(Context context) {

		initList();

		MyAsyn asyn = new MyAsyn();
		asyn.execute("Hi");

	}

	private void initList() {
		list.add(values);
	}

	private class MyAsyn extends AsyncTask<String, Integer, Integer> {

		@Override
		protected void onPreExecute() {
			System.out.println("AsynTest.MyAsyn.onPreExecute()");
		}

		@Override
		protected Integer doInBackground(String... params) {

			for (int i = 0; i < params.length; i++) {
				System.out.println("i=" + i);
				onProgressUpdate(i);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return 1;
		}

		@Override
		protected void onPostExecute(Integer result) {
			System.out.println("AsynTest.MyAsyn.onPostExecute() result = " + result);

		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			System.out.println("AsynTest.MyAsyn.onProgressUpdate()" + values);
		}
	}
}