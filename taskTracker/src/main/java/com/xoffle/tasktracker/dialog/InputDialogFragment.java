package com.xoffle.tasktracker.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;

public class InputDialogFragment extends DialogFragment {

    private static final String TAG = "dialog";
    private EditText editText;
    private int num;
    private String title = "Title";
    private String btn1Txt = "Create";
    private String btn2Txt = "Cancel";
    private String hint = "Enter Text";

    private OnDialogInputListener listener;

    public static InputDialogFragment newInstance(int num) {
        InputDialogFragment instance = new InputDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("num", num);

        instance.setArguments(bundle);

        return instance;
    }

    public void setDialogInputListener(OnDialogInputListener listener) {
        this.listener = listener;
    }

    public void init(String title, String btn1Txt, String btn2Txt, String hint) {

        this.title = title;
        this.btn1Txt = btn1Txt;
        this.btn2Txt = btn2Txt;
        this.hint = hint;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getArguments().getInt("num");

        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        switch (num) {
            case 1:
                style = DialogFragment.STYLE_NO_TITLE;
                break;
            default:
                break;
        }

        switch (num) {
            case 1:
                theme = android.R.style.Theme_Black_NoTitleBar;
                break;
            default:
                break;
        }

        setStyle(style, theme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.input_dialog_fragment, container, false);

        getDialog().setTitle(title);

        ((Button) v.findViewById(R.id.input_dialog_button_create)).setText(btn1Txt);
        ((Button) v.findViewById(R.id.input_dialog_button_cancel)).setText(btn2Txt);

        editText = ((EditText) v.findViewById(R.id.input_dialog_msg));
        editText.setHint(hint);

        setMessage(editText);

        setClickListeren(v);

        return v;
    }

    private void setMessage(EditText editText) {
        Bundle bundle = getArguments();
        String remark = bundle.getString(Constant.UPDATE_REMARK);
        String msg = bundle.getString(Constant.UPDATE_MSG);

        if (remark != null) {
            editText.setText(remark);
        } else if (msg != null) {
            editText.setText(msg);
        }
    }

    private void setClickListeren(View v) {
        ((Button) v.findViewById(R.id.input_dialog_button_create)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Debug.debug("Fragment Dialog on click of Create Btn");

                Debug.debug("listener = " + listener);
                String txt = editText.getText().toString();
                if (listener != null && !Utils.isNull(txt))
                    listener.onButton1Click(editText.getText().toString());
            }
        });

        ((Button) v.findViewById(R.id.input_dialog_button_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Debug.debug("Fragment Dialog on click of Create Btn");
                listener.onButton2Click(null);
            }
        });
    }

    public void showDialog(FragmentManager sFM) {

        FragmentTransaction ft = sFM.beginTransaction();

        Fragment prev = sFM.findFragmentByTag(TAG);

        if (prev != null)
            ft.remove(prev);

        ft.addToBackStack(null);

//        DialogFragment fragment = InputDialogFragment.newInstance(num);
//
//        fragment.show(ft, TAG);
        this.show(ft, TAG);
    }

    public interface OnDialogInputListener {
        void onButton1Click(String txt);

        void onButton2Click(String txt);
    }
}