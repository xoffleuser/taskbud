package com.xoffle.tasktracker.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.userauth.UserauthActivity;
import com.xoffle.tasktracker.util.Utils;

/**
 * Created by ravi on 28/7/15.
 */
public class SplashActivity extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkAuthentication();
        setContentView(R .layout.splash_activity);
    }

    private void checkAuthentication() {
        Intent intent;

        if (Utils.isLoggedIn(DataStorage.getInstance(this))) {
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(this, UserauthActivity.class);
        }

        finish();
        startActivity(intent);
    }
}