package com.xoffle.tasktracker.group;

/**
 * Created by ravi on 11/4/15.
 */
public class
        GroupController {

    private static GroupController instance;
    private Group currentGroup;

    private GroupController() {
    }

    public static GroupController getInstance() {
        return instance == null ? instance = new GroupController() : instance;
    }

    public Group getCurrentGroup() {
        return currentGroup;
    }

    public void setCurrentGroup(Group currentGroup) {
        this.currentGroup = currentGroup;
    }

}
