package com.xoffle.tasktracker.group;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ravi on 6/8/15.
 */
public class GroupMemberListView {
    private GroupDetailFragment detailFragment;
    private LinearLayout layout;
    private LayoutInflater inflater;

    public GroupMemberListView(GroupDetailFragment detailFragment, LinearLayout layout, LayoutInflater inflater) {
        this.detailFragment = detailFragment;

        this.layout = layout;
        this.inflater = inflater;

        init();
    }

    public void init() {
        resetData();
//        Group group = GroupController.getInstance().getCurrentGroup();
//        String myId = DataStorage.getInstance(activity).getUserId();
//
//        ArrayList<GroupMemberBean> memberList = BackendHandler.getInstance(activity).getGroupMembers(group.getGroupId(), group.getAdminId());
//
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.setMargins(0, 1, 0, 0);
//
//
//        GroupMemberBean me = null;
//        for (int i = memberList.size() - 1; i >= 0; i--) {
//            GroupMemberBean member = memberList.get(i);
//            if (member.getMemberId().equals(myId)) {
//                me = member;
//                continue;
//            }
//
//            addMemberToLayout(layout, layoutParams, inflater, member, false);
//        }
//
//        if (me != null) {
//            me.setUserName(activity.getResources().getString(R.string.you));
//            addMemberToLayout(layout, layoutParams, inflater, me, true);
//        }
    }

    private void addMemberToLayout(LinearLayout layout, LinearLayout.LayoutParams layoutParams, LayoutInflater inflater, GroupMember member) {
        View userView = inflater.inflate(R.layout.seen_info_view, null, false);

        TextView userStatus = (TextView) userView.findViewById(R.id.seenInfoViewDate);
        TextView username = (TextView) userView.findViewById(R.id.seenInfoViewUsersName);
//        ImageView userImage = (ImageView) userView.findViewById(R.id.seenInfoViewUserImage);

        username.setText(member.getDisplayName());
        userStatus.setText("Task Bud only");

        if (member.isAdmin()) {
            username.setTextColor(detailFragment.getActivity().getResources().getColor(R.color.accent_material_light));
            userStatus.setTextColor(detailFragment.getActivity().getResources().getColor(R.color.accent_material_dark));
        }

        if (!member.isMe()) {
            userView.setTag(member);
            detailFragment.registerForContextMenu(userView);
        }

        layout.addView(userView, layoutParams);
    }

    void resetData() {

        if (layout != null) {
            layout.removeAllViews();
        }

        Group currentGroup = GroupController.getInstance().getCurrentGroup();
        ArrayList<GroupMember> memberList = BackendHandler.getInstance(detailFragment.getActivity()).getGroupMembers(currentGroup.getGroupId(), currentGroup.getAdminId());

        Collections.sort(memberList);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 1, 0, 0);


        for (int i = 0; i < memberList.size(); i++) {
            GroupMember member = memberList.get(i);
            addMemberToLayout(layout, layoutParams, inflater, member);
        }
    }
}