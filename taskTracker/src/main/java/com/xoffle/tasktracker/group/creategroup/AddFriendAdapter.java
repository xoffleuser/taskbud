package com.xoffle.tasktracker.group.creategroup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.group.GroupMember;

import java.util.ArrayList;

public class AddFriendAdapter extends ArrayAdapter<TaskBudBean> {

    private final OnAddFriendAdapterClickListener clickListener;
    private LayoutInflater inflater;
    private GroupMember groupMember;

    public AddFriendAdapter(Context context, int resource, OnAddFriendAdapterClickListener clickListener) {
        super(context, resource);
        inflater = LayoutInflater.from(context);

        this.clickListener = clickListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TaskBudBean taskBud = (TaskBudBean) getItem(position);

        FriendHolder friendHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.add_friend_view, parent, false);

            friendHolder = new FriendHolder(convertView, taskBud, clickListener);

            convertView.setTag(friendHolder);
        } else {
            friendHolder = (FriendHolder) convertView.getTag();
            friendHolder.setData(taskBud, clickListener);
        }

//        ImageView userImage = (ImageView) convertView.findViewById(R.id.imageButtonAddFriendView);
//        userImage.setFocusable(false);
//        ((TextView) convertView.findViewById(R.id.textViewAddFriendName)).setText(friend.getName());
//
//        Button crossButton = (Button) convertView.findViewById(R.id.buttonCrossAddFriendView);
//        setClickListener(crossButton, position);

        return convertView;
    }

//    private void setClickListener(Button image, final int position) {
//        image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                clickListener.onCrossImageClick(position);
//            }
//        });
//    }

    public void addNewFriend(TaskBudBean taskBudBean) {
        if (isContain(taskBudBean)) {
            return;
        }
        add(taskBudBean);
    }

    private boolean isContain(TaskBudBean taskBudBean) {
        for (TaskBudBean f : getAllItems()) {
            if (f.getMemberId().equals(taskBudBean.getMemberId())) {
                return true;
            }
        }

        return false;
    }

    public ArrayList<TaskBudBean> getAllItems() {
        ArrayList<TaskBudBean> list = new ArrayList<>();
        for (int i = getCount() - 1; i >= 0; i--) {
            list.add(getItem(i));
        }

        return list;
    }

    static class FriendHolder implements View.OnClickListener {

        private ImageView userImage;
        private TextView userNameTextView;
        private Button crossButton;
        private TaskBudBean taskBud;
        private OnAddFriendAdapterClickListener clickListener;

        FriendHolder(View view, TaskBudBean taskBudBean, OnAddFriendAdapterClickListener onClickListener) {
            findViewsById(view);
            setData(taskBudBean, onClickListener);
        }

        private void findViewsById(View view) {
            userImage = (ImageView) view.findViewById(R.id.imageButtonAddFriendView);
            userImage.setFocusable(false);
            userNameTextView = ((TextView) view.findViewById(R.id.textViewAddFriendName));

            crossButton = (Button) view.findViewById(R.id.buttonCrossAddFriendView);

        }


        public void setData(TaskBudBean taskBud, OnAddFriendAdapterClickListener clickListener) {
            this.taskBud = taskBud;
            this.clickListener = clickListener;
            userNameTextView.setText(taskBud.getDisplayName());

            crossButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonCrossAddFriendView:
                    clickListener.onCrossImageClick(taskBud);
                    break;
            }
        }
    }
}