package com.xoffle.tasktracker.group.creategroup;

import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SearchView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.db.CursorFactory;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.model.DataStorage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class SelectFriendsActivity extends ActionBarActivity {

    public static final String FLIST = "fList";
    private ListView listView;
    private FriendAdapter friendAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_friends);

        setListView();

        inflateToolbar();
    }

    private void inflateToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activitySelectFriendsToolbarSimple);
        setSupportActionBar(toolbar);
    }

    private void setListView() {
        listView = (ListView) findViewById(R.id.listViewSelectFriends);
        friendAdapter = new FriendAdapter(this, R.layout.activity_select_friends);
        listView.setAdapter(friendAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Friend friend = (Friend) friendAdapter.getItem(position);
                if (friend.isChecked) {
                    friend.setChecked(false);
                } else {
                    friend.setChecked(true);
                }

                ((CheckBox) view.findViewById(R.id.checkBoxFriendView)).setChecked(friend.isChecked);
            }
        });

        loadFriends();
    }

    private void loadFriends() {

        ArrayList<String> idList = getExistingUserIds();

        Cursor cursor = DBController.getInstance(this).loadAllFriends();

        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();

            DataStorage store = DataStorage.getInstance(this);
            do {
                String id = cursor.getString(0);
                TaskBudBean taskBudBean = CursorFactory.createTaskBud(this,id);
                String name = cursor.getString(1);

                if(!idList.contains(id)){
//                if (!id.equals(store.getUserId())) {
                    friendAdapter.addNewFriend(new Friend(taskBudBean));
                }
            } while (cursor.moveToNext());
        }
    }

    private ArrayList<String> getExistingUserIds() {
        Bundle bundle = getIntent().getExtras();
        return bundle.getStringArrayList(Constant.EXISTING_USER);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (intent.getAction() == Intent.ACTION_SEARCH) {
            String searchText = intent.getStringExtra(SearchManager.QUERY);
            friendAdapter.search(searchText);
        }
    }

    @Override
    public void onBackPressed() {

        if (friendAdapter.isAllDataLoaded()) {
            super.onBackPressed();
        } else {
            friendAdapter.loadAllData();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.select_friends, menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            SearchView searchView = (SearchView) menu.findItem(R.id.selectFriendSearch).getActionView();
            if (searchView != null) {
                SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.selectFriendSearch:
                onSearchRequested();
                return true;

            case R.id.action_done:
                packSelectedFriends();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void packSelectedFriends() {

        ArrayList<Friend> fList = getSelectedFriends();

        Intent intent = getIntent();
        intent.putExtra(FLIST, fList);
        setResult(RESULT_OK, intent);
        finish();
    }

    private ArrayList<Friend> getSelectedFriends() {

        List<String> list = Arrays.asList(new String[]{"A","B","C"});

        ArrayList<Friend> fList = new ArrayList<Friend>();

        for (int i = friendAdapter.getCount() - 1; i >= 0; i--) {
            Friend friend = (Friend) friendAdapter.getItem(i);

            if (friend.isChecked) {
                fList.add(friend);
            }
        }
        return fList;
    }
}