package com.xoffle.tasktracker.group.creategroup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;

import java.io.Serializable;
import java.util.ArrayList;

public class FriendAdapter extends ArrayAdapter<Friend> {

    private LayoutInflater inflater;

    private ArrayList<Friend> allFriends;

    public FriendAdapter(Context context, int resource) {
        super(context, resource);
        allFriends = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Friend friend = (Friend) getItem(position);

        FriendHolder friendHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.friend_view, parent, false);

            friendHolder = new FriendHolder(convertView, friend);
            convertView.setTag(friendHolder);
        } else {
            friendHolder = (FriendHolder) convertView.getTag();
            friendHolder.setData(friend);
        }


//        ImageView button = (ImageView) convertView.findViewById(R.id.imageButtonFriendView);
//        button.setFocusable(false);
//
//        ((TextView) convertView.findViewById(R.id.textViewFriendName)).setText(friend.getName());
////        ((TextView) convertView.findViewById(R.id.textViewFriendStatus)).setText(friend.getStatus());
//
//        ((CheckBox) convertView.findViewById(R.id.checkBoxFriendView)).setSelected(friend.isChecked());

        return convertView;
    }

    public void addNewFriend(Friend friend) {
        add(friend);
        allFriends.add(friend);
    }

    public void search(String quertText) {
        clear();
        int size = allFriends.size();
        for (int i = 0; i < size; i++) {
            Friend friend = allFriends.get(i);
            if (friend.taskBudBean.getDisplayName().contains(quertText)) {
                add(friend);
            }
        }
    }

    public boolean isAllDataLoaded() {
        return getCount() == allFriends.size();
    }


    public void loadAllData() {
        clear();
        addAll(allFriends);
    }

    static class FriendHolder {

        private ImageView imageView;
        private TextView textView;
        private CheckBox checkBox;

        FriendHolder(View view, Friend friend) {
            findViewsById(view);
            setData(friend);
        }

        private void findViewsById(View view) {
            imageView = (ImageView) view.findViewById(R.id.imageButtonFriendView);

            textView = ((TextView) view.findViewById(R.id.textViewFriendName));
//        TextView statusTextView = ((TextView) view.findViewById(R.id.textViewFriendStatus));

            checkBox = ((CheckBox) view.findViewById(R.id.checkBoxFriendView));

        }


        public void setData(Friend friend) {
            imageView.setFocusable(false);
            textView.setText(friend.taskBudBean.getDisplayName());
            checkBox.setSelected(friend.isChecked);
        }
    }
}

class Friend implements Serializable{
    TaskBudBean taskBudBean;
    boolean isChecked;

    Friend(TaskBudBean taskBudBean) {
        this.taskBudBean = taskBudBean;
    }

    public void setChecked(boolean b) {
        this.isChecked = b;
    }
}