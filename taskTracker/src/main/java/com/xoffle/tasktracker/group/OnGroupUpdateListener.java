package com.xoffle.tasktracker.group;

/**
 * Created by ravi on 7/7/15.
 */
public interface OnGroupUpdateListener {
    void onGroupUpdate(Group group);
}
