package com.xoffle.tasktracker.group;

import android.content.Context;
import android.database.Cursor;

import com.inkling.leaderboard.AppGroup;
import com.inkling.leaderboard.GroupBean;
import com.xoffle.tasktracker.db.DBConstants;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.view.TaskView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ravi on 19/4/15. this group is treated as unique on the basis of groupId and
 * groupCount
 */
public class Group extends GroupBean implements TaskView {

    private ArrayList<GroupMember> groupMembers;

    private String memberText;
    private int updateCount;

    public Group(Context context, AppGroup appGroup) {
        super(appGroup);

        groupMembers = DBController.getInstance(context).getGroupMembers(groupId, adminId);

        setMemberBeanIdList(getMemberIds());
        convertToString(groupMembers);
    }

    public Group(Context context, Cursor cursor) {
        groupId = cursor.getString(cursor.getColumnIndex(DBConstants.GROUP_ID));
        name = cursor.getString(cursor.getColumnIndex(DBConstants.NAME));

        adminId = cursor.getString(cursor.getColumnIndex(DBConstants.ADMIN_ID));
        creatorId = cursor.getString(cursor.getColumnIndex(DBConstants.CREATOR_ID));
        status = cursor.getString(cursor.getColumnIndex(DBConstants.STATUS));
        statusUpdateOn = cursor.getLong(cursor.getColumnIndex(DBConstants.STATUS_UPDATE_ON));
        createTime = cursor.getLong(cursor.getColumnIndex(DBConstants.CREATED_AT));
        updateTime = cursor.getLong(cursor.getColumnIndex(DBConstants.UPDATED_AT));

        int index = cursor.getColumnIndex(DBConstants.IS_MY_GROUP);
        if (index != -1) {
            isMyGroup = cursor.getInt(cursor.getColumnIndex(DBConstants.IS_MY_GROUP)) == 0 ? false : true;
        }

        groupMembers = DBController.getInstance(context).getGroupMembers(groupId, adminId);
        setMemberBeanIdList(getMemberIds());
        convertToString(groupMembers);

        setImageId(cursor.getString(cursor.getColumnIndex(DBConstants.IMAGE_OBJECT_ID)));
    }

    public static String arrayListToString(ArrayList<GroupMember> list) {
        if (list == null) {
            return null;
        }

        Collections.sort(list);
        StringBuilder builder = new StringBuilder();

        DataStorage store = DataStorage.getInstance(null);

        int size = list.size();

        for (int i = 0; i < size; i++) {
//            if (!list.get(i).getMemberId().equals(store.getUserId())) {
            builder.append(list.get(i).getDisplayName()).append(",");
//            }
        }
//
//        if (size > 1) {
//            builder.append("You");
//        }

        return builder.toString();
    }

    public void setGroupMembers(ArrayList<GroupMember> groupMember) {
        this.groupMembers = groupMember;
    }

    private void convertToString(ArrayList<GroupMember> members) {
        memberText = arrayListToString(members);
    }

    public String getMembersText() {
        return memberText;
    }

    public ArrayList<String> getMemberIds() {
        ArrayList<String> ids = new ArrayList<>();
        for (int i = groupMembers.size() - 1; i >= 0; i--) {
            ids.add(groupMembers.get(i).getMemberId());
        }
        return ids;
    }

    public void addGroupMember(GroupMember member) {

        if (groupMembers.contains(member)) {
            return;
        }

        groupMembers.add(member);

        if (memberText != null && memberText.length() > 0) {
            memberText += ", " + member.getUserName();
        } else {
            memberText = member.getUserName();
        }
    }

    @Override
    public int hashCode() {
        return groupId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        Group group = (Group) o;
        return groupId.equals(group.groupId);
    }

    public boolean contains(String groupId) {
        return this.groupId.equals(groupId);
    }

//    public void setGroupMember(ArrayList<GroupMember> groupMembers) {
//        this.setGroupMember(groupMembers);
//        convertToString(groupMembers);
//    }

    public int getUpdateCount() {
        return updateCount;
    }

    public void setUpdateCount(int updateCount) {
        this.updateCount = updateCount;
    }

    public void removeGroupMember(String memberId) {
        for (int i = groupMembers.size() - 1; i >= 0; i--) {
            GroupMember member = groupMembers.get(i);
            if (member.getMemberId().equals(memberId)) {
                groupMembers.remove(member);
                break;
            }
        }

        setMemberBeanIdList(getMemberIds());
    }

//    public void setMembers(ArrayList<GroupMember> members) {
//        setGroupMember(members);
//    }

//    public String getMemberText() {
//        return memberText;
//    }
//
//    public void setMemberText(String memberText) {
//        this.memberText = memberText;
//    }
}