package com.xoffle.tasktracker.group;

import com.inkling.leaderboard.TaskBudBean;

/**
 * Created by ravi on 8/8/15.
 */
public class GroupMember extends TaskBudBean implements Comparable<GroupMember> {

    private boolean isMe;
    private boolean isAdmin;

    public GroupMember(String memberId, String userName) {
        super(memberId, userName);
    }

    public GroupMember(String memberId, String userName, String status) {
        super(memberId, userName, status);
    }

    public GroupMember(String memberId, String userName, String status, String imageName) {
        super(memberId, userName, status, imageName);
    }

    public GroupMember(TaskBudBean taskBudBean) {
        super(taskBudBean);
    }

    public boolean isMe() {
        return isMe;
    }

    public void setMe(boolean isMe) {
        this.isMe = isMe;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public int compareTo(GroupMember o) {
        if (o.isMe) {
            return -1;
        } else if (o.isAdmin) {
            return 1;
        } else {
            if(displayName==null||o.getDisplayName()==null)
                return userName.compareToIgnoreCase(o.userName);
            return displayName.compareToIgnoreCase(o.displayName);
        }
    }
}