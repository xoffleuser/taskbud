package com.xoffle.tasktracker.group.creategroup;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.Utils;

public class CreateGroupActivity extends ActionBarActivity implements View.OnClickListener {

    public static final String GROUP_NAME = "groupName";
    private static final int REQUEST_CODE = 1;
    private static final int REQUEST_PHOTO_PICK = 2;
    private static final int REQUEST_CROP_IMAGE = 3;
    protected boolean enable;
    private String groupName;
    private ImageView groupIconImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        setListenerToEditText();

        inflateToolbar();

        groupIconImageView = (ImageView) findViewById(R.id.activityCreateGroupImageView);
        groupIconImageView.setOnClickListener(this);
    }

    private void inflateToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activityCreateGroupToolbarSimple);
        setSupportActionBar(toolbar);
    }

    private void setListenerToEditText() {
        ((EditText) findViewById(R.id.editTextGroupName)).addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // System.out.println("cs = " + s + ", start = " + start + ", before = " + before + ", count = " + count);

                if (s.length() == 0) {
                    enable = false;
                    supportInvalidateOptionsMenu();
                } else if (!enable) {
                    enable = true;
                    supportInvalidateOptionsMenu();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_group, menu);
        menu.getItem(0).setEnabled(enable);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.getItem(0).setEnabled(enable);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_next && item.isEnabled()) {
            groupName = ((EditText) findViewById(R.id.editTextGroupName)).getText().toString();
            addFriends();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addFriends() {
        Intent intent = new Intent(this, AddFriendsToGroupActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

            intent.putExtra(GROUP_NAME, groupName);
            setResult(RESULT_OK, intent);
            finish();
        } else if (requestCode == REQUEST_PHOTO_PICK && resultCode == RESULT_OK) {
            setImage(intent);
        }

        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void setImage(Intent intent) {
        Bundle extras = intent.getExtras();

        if (extras != null) {
            Bitmap photo = extras.getParcelable("data");

            groupIconImageView.setImageBitmap(photo);

            saveImageToFile(photo);
        }
    }

    private void saveImageToFile(Bitmap photo) {
        String path = ImageUtil.saveImage(this, photo, Constant.ImageC.GROUP_IMG_NAME);

        Bitmap bitmap = ImageUtil.loadImageFromStorage(ImageUtil.FILE_NAME);
        System.out.println("Bitmap = "+bitmap);
        System.out.println("Bitmap path = "+path);

//        ParseImageHandler.saveGroupImage("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activityCreateGroupImageView:
                chooseImage();
        }
    }

    private void chooseImage() {
        Intent pickImageIntent = ImageUtil.getImagePickIntent(200,200);
        startActivityForResult(pickImageIntent, REQUEST_PHOTO_PICK);
    }
}