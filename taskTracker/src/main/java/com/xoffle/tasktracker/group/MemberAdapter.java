package com.xoffle.tasktracker.group;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;

import java.util.ArrayList;

/**
 * Created by ravi on 3/8/15.
 */
public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.MemberHolder> {
    private final ArrayList<GroupMember> list;

    public MemberAdapter(Context context) {
        list = new ArrayList<GroupMember>();
    }

    @Override
    public MemberHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        Debug.debug("added to create holder");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.seen_info_view, viewGroup, false);
        MemberHolder memberHolder = new MemberHolder(viewGroup.getContext(), view, list.get(position));

        return memberHolder;
    }

    @Override
    public void onBindViewHolder(MemberHolder memberHolder, int position) {
        memberHolder.setData(list.get(position));
        Debug.debug("added to bind Holder");
    }

    public void add(GroupMember groupMember) {
        Debug.debug("added to Recycler view");
        list.add(groupMember);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        Debug.debug("added count = "+list.size());
        return list.size();
    }

    class MemberHolder extends RecyclerView.ViewHolder {
        private ImageView userImageView;
        private TextView userNameTextView;
        private TextView userStatus;
        private Context context;

        public MemberHolder(Context context, View view, GroupMember groupMember) {
            super(view);
            this.context = context;

            findViewsById(view);
            setData(groupMember);
        }

        private void findViewsById(View view) {
            userImageView = (ImageView) view.findViewById(R.id.seenInfoViewUserImage);
            userNameTextView = (TextView) view.findViewById(R.id.seenInfoViewUsersName);
            userStatus = (TextView) view.findViewById(R.id.seenInfoViewDate);
        }

        public void setData(GroupMember groupMember) {
            ImageUtil.setImageFromStorage(userImageView, groupMember.getImageId());
            userStatus.setText(groupMember.getStatus());

            if (groupMember.isMe()) {
                userNameTextView.setText(context.getString(R.string.you));
            } else {
                userNameTextView.setText(groupMember.getUserName());
            }

            if (groupMember.isAdmin()) {
                userNameTextView.setTextColor(context.getResources().getColor(R.color.teal));
            } else {
                userNameTextView.setTextColor(context.getResources().getColor(R.color.material_blue_grey_800));
            }
        }
    }
}