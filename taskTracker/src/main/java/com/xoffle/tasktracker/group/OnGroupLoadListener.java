package com.xoffle.tasktracker.group;

import android.database.Cursor;

import java.util.List;

/**
 * Created by ravi on 17/5/15.
 */
public interface OnGroupLoadListener {

    void onGroupLoaded(Group group);
}
