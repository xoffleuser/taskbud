package com.xoffle.tasktracker.group;

/**
 * Created by ravi on 20/5/15.
 */
public class GroupInfo {

    private String id;
    private int groupCount;
    private String name;
    private long createTime;
    private long updateDate;

    public GroupInfo() {}

    public GroupInfo(String groupName, long createTime, long updateTime) {
        this.name = groupName;
        this.createTime = createTime;
        this.updateDate = updateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(int groupCount) {
        this.groupCount = groupCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

}
