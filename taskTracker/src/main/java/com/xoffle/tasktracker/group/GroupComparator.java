package com.xoffle.tasktracker.group;

import java.util.Comparator;

/**
 * Created by ravi on 20/6/15.
 */
public class GroupComparator implements Comparator<Group> {
    @Override
    public int compare(Group lhs, Group rhs) {
        if (lhs.isMyGroup()) {
            return -1;
        }
        if (rhs.isMyGroup()) {
            return 1;
        }

        return lhs.getUpdateTime() > rhs.getUpdateTime() ? -1 : 1;
    }
}
