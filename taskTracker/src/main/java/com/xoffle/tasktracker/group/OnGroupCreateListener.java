package com.xoffle.tasktracker.group;

/**
 * Created by ravi on 23/7/15.
 */
public interface OnGroupCreateListener {
    void onGroupCreated(Group group);
}
