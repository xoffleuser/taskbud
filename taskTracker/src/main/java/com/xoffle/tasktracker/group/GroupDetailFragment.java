package com.xoffle.tasktracker.group;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.custom.LoadingDialog;
import com.xoffle.tasktracker.group.creategroup.AddFriendsToGroupActivity;
import com.xoffle.tasktracker.group.creategroup.SelectFriendsActivity;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.image.ParseImageHandler;
import com.xoffle.tasktracker.listeners.OnImageUploadListener;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.DateUtil;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.ImageUtil;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.utilfragments.ImageChangeFragment;
import com.xoffle.tasktracker.utilfragments.ImageChangeListener;
import com.xoffle.tasktracker.utilfragments.TextChangeFragment;
import com.xoffle.tasktracker.utilfragments.TextChangeListener;
import com.xoffle.tasktracker.view.MyFragment;
import com.xoffle.tasktracker.view.customviews.SquareImageView;

import java.util.ArrayList;

/**
 * Created by ravi on 18/5/15.
 */
public class GroupDetailFragment extends MyFragment implements View.OnClickListener {

    private static final int REQUEST_SELECT_FRIENDS = 10;
    private static final int MAX_PARTICIPENTS = 100;

    private DataStorage store;

    private Group currentGroup;

    private RelativeLayout groupIcon;
    private ScrollView scrollView;
    private TextView groupStatusTv;
    private SquareImageView groupIconImage;

    private LoadingDialog loadingDialog;
    private GroupMemberListView memberListView;

    private boolean isLoaded;
    private boolean amIAdmin;
    private GroupMember clickedGroupMember;

    private Button addParticipant;
    private Button buttonExit;

    private ImageView statusViewEditImageView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setUserVisibleHint(true);

        currentGroup = GroupController.getInstance().getCurrentGroup();
        store = DataStorage.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group_detail_fragment, container, false);

        amIAdmin = store.getUserId().equals(GroupController.getInstance().getCurrentGroup().getAdminId());

        initView(view, inflater);

        ImageUtil.setImageFromStorage(groupIconImage, currentGroup.getImageObjectId());

        loadingDialog = new LoadingDialog();
        return view;
    }

    private void initView(View view, LayoutInflater inflater) {
        setToolbar(view);

        groupIcon = (RelativeLayout) view.findViewById(R.id.groupDetailFragmentGroupPicLayout);
        groupIconImage = (SquareImageView) groupIcon.findViewById(R.id.groupDetailFragmentGroupPic);
        groupIcon.setOnClickListener(this);

        scrollView = (ScrollView) view.findViewById(R.id.groupDetailFragmentScrollView);

        measureLayout();

        inflateScrollView(view, inflater);

        setStyle();
        setTextToViews(view);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_group_detail, menu);

        this.menuInflater = inflater;
        this.menu = menu;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                ((MainActivity)getActivity()).openDrawerLayout();
                return true;

            case R.id.menuGroupDetailChangeGroupName:
                openNameChangeFragment();
                break;

            case R.id.menuGroupDetailChangeGroupIcon:
                openChangeImageFragment();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        System.out.println("onHiddenChanged GroupDetail hidden = " + hidden);
        super.onHiddenChanged(hidden);

        if (!hidden) {
            if (menu != null) {
                menu.clear();
                menuInflater.inflate(R.menu.menu_group_detail, menu);
            }
        }
    }

    private void setToolbar(View view) {
        Group group = GroupController.getInstance().getCurrentGroup();

        GroupInfo groupInfo = BackendHandler.getInstance(getActivity()).getGroupInfo(group.getGroupId());

        TaskBudBean admin = BackendHandler.getInstance(getActivity()).getMember(group.getAdminId());
        String subtitle = "Created by " + admin.getDisplayName() + " on " + DateUtil.format(groupInfo.getUpdateDate(), DateUtil.FORMAT_1);

        initTextToolbar(view, R.id.userDetailFragmentMiddleTextToolbar, group.getName(), subtitle);

        enableHomeButton(R.drawable.ic_list_white);
    }

    private void setTextToViews(View view) {

        View statusView = view.findViewById(R.id.groupDetailFragmentStatusView);
        TextView statusDateText = (TextView) statusView.findViewById(R.id.statusViewTextViewDate);

        statusViewEditImageView = (ImageView) statusView.findViewById(R.id.statusViewEditIv);

        setStatusChangeImageView();

        groupStatusTv = (TextView) statusView.findViewById(R.id.statusViewStatus);
        groupStatusTv.setText(currentGroup.getStatus());


//        ImageView editStatusIv = (ImageView) statusView.findViewById(R.id.statusViewEditIv);
//        if (amIAdmin) {
//            editStatusIv.setVisibility(View.VISIBLE);
//        }

        View cardViewHeader = view.findViewById(R.id.groupDetailFragmentScrollViewCardViewHeaderText);
        TextView headerRightText = (TextView) cardViewHeader.findViewById(R.id.headerTextViewRightText);
        TextView headerLeftText = (TextView) cardViewHeader.findViewById(R.id.headerTextViewLeftText);

        Group group = GroupController.getInstance().getCurrentGroup();

        GroupInfo groupInfo = BackendHandler.getInstance(getActivity()).getGroupInfo(group.getGroupId());

        statusDateText.setText(DateUtil.format(groupInfo.getUpdateDate(), DateUtil.FORMAT_1));

        headerLeftText.setText(getActivity().getString(R.string.participents));
        headerRightText.setText(group.getMemberIds().size() + getActivity().getString(R.string.out_of) + MAX_PARTICIPENTS);
    }

    private void setStatusChangeImageView() {
        if (amIAdmin) {
            statusViewEditImageView.setVisibility(View.VISIBLE);
            statusViewEditImageView.setOnClickListener(this);
        } else {
            statusViewEditImageView.setVisibility(View.GONE);
        }
    }

    private void refressViewOnAdminChange() {
        amIAdmin = store.getUserId().equals(GroupController.getInstance().getCurrentGroup().getAdminId());
        setAddParticipentButton();
        setStatusChangeImageView();
        updateScrollView();
    }

    private void setStyle() {
        groupIcon.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (!isLoaded) {
                    isLoaded = true;
                    setExtendedToolbarProperty(groupIcon.getHeight());
                }
                return true;
            }
        });
    }

    private void measureLayout() {
        toolbar.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        toolbar.layout(0, 0, toolbar.getMeasuredWidth(), toolbar.getMeasuredHeight());

        groupIcon.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        groupIcon.layout(0, 0, groupIcon.getMeasuredWidth(), groupIcon.getMeasuredHeight());
    }

    private void setExtendedToolbarProperty(int height) {

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, toolbar.getHeight(), 0, 0);
        System.out.println("toolbar height = " + toolbar.getBottom() + "," + toolbar.getHeight());
        groupIcon.setLayoutParams(params);
        scrollView.setPadding(0, toolbar.getHeight() + height, 0, 0);

        setDefaultScroll();

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                float scrollY = scrollView.getScrollY();
                scrollImageIcon(scrollY);
            }
        });
    }

    private void scrollImageIcon(float scrollY) {
        groupIcon.setTranslationY(-scrollY / 2f);
    }

    private void setDefaultScroll() {
        float defaultValue = scrollView.getPaddingTop() / 3;

        scrollView.scrollTo(0, (int) defaultValue);
        scrollImageIcon(defaultValue);
    }

    private void inflateScrollView(View view, LayoutInflater inflater) {

        Group group = GroupController.getInstance().getCurrentGroup();
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.groupDetailFragmentLinearLayoutMembers);

        memberListView = new GroupMemberListView(this, layout, inflater);

        addParticipant = (Button) view.findViewById(R.id.groupDetailFragmentButtonAddParticipant);
        buttonExit = (Button) view.findViewById(R.id.groupDetailFragmentButtonExit);

        if (group.isMyGroup()) {
            view.findViewById(R.id.groupDetailFragmentScrollViewCardView).setVisibility(View.INVISIBLE);
            addParticipant.setVisibility(View.INVISIBLE);
            buttonExit.setVisibility(View.INVISIBLE);
        } else {
            setAddParticipentButton();
            buttonExit.setOnClickListener(this);
        }

    }

    private void setAddParticipentButton() {
        if (amIAdmin) {
            addParticipant.setVisibility(View.VISIBLE);
            addParticipant.setOnClickListener(this);
        } else {
            addParticipant.setVisibility(View.GONE);
        }
    }

    private void updateScrollView() {
        memberListView.resetData();
        //TODO
        Utils.showToast(getActivity(), "updateScrollView");
        Debug.debug("updateScrollView");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_task_bud, menu);

        MenuItem menuItem = menu.findItem(R.id.menu_task_bud_remove_member);

        TextView textView = (TextView) v.findViewById(R.id.seenInfoViewUsersName);
        String text = textView.getText().toString();

        menuItem.setTitle("Remove " + text.split(" ")[0] + " From Group");

        clickedGroupMember = (GroupMember) v.getTag();

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_task_bud_remove_member:
                removeMember();
                break;

            case R.id.menu_task_bud_make_admin:
                makeGroupAdmin();
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void makeGroupAdmin() {

        String newAdminId = clickedGroupMember.getMemberId();

        currentGroup.setAdminId(newAdminId);

        Utils.showLoadingDialog(loadingDialog, getFragmentManager());

        BackendHandler.getInstance(getActivity()).updateGroup(currentGroup, new OnGroupUpdateListener() {

            @Override
            public void onGroupUpdate(Group group) {
                Utils.dismissDialog(loadingDialog);
                refressViewOnAdminChange();
            }
        },true);
    }

    private void updateGroup() {
        Utils.showLoadingDialog(loadingDialog, getFragmentManager());

        BackendHandler.getInstance(getActivity()).updateGroup(currentGroup, new OnGroupUpdateListener() {

            @Override
            public void onGroupUpdate(Group group) {
                Utils.dismissDialog(loadingDialog);
                updateScrollView();
            }
        },true);
    }

    private void removeMember() {

        String removeMemberId = clickedGroupMember.getMemberId();

        currentGroup.removeGroupMember(removeMemberId);

        updateGroup();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.statusViewEditIv:
                openStatusChangeFragment();
                break;
            case R.id.groupDetailFragmentGroupPicEditIv:
                openChangeImageFragment();
                break;
            case R.id.groupDetailFragmentButtonAddParticipant:
                addParticipant();
                break;
            case R.id.groupDetailFragmentButtonExit:
                exitGroup();
                break;
        }
    }

    private void exitGroup() {
        String myId = DataStorage.getInstance(getActivity()).getUserId();
        Debug.debug("AdminId = " + myId);
        currentGroup.removeGroupMember(myId);

        Utils.showLoadingDialog(loadingDialog, getFragmentManager());
        BackendHandler.getInstance(getActivity()).updateGroup(currentGroup, new OnGroupUpdateListener() {
            @Override
            public void onGroupUpdate(Group group) {
//                updateScrollView();
                Utils.dismissDialog(loadingDialog);
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        },true);
    }

    private void addParticipant() {
        Intent intent = new Intent(getActivity(), AddFriendsToGroupActivity.class);
        setExistingTaskBud(intent);
        startActivityForResult(intent, REQUEST_SELECT_FRIENDS);
    }

    private void setExistingTaskBud(Intent intent) {
        Bundle bundle = new Bundle();

        bundle.putStringArrayList(Constant.EXISTING_USER, currentGroup.getMemberIds());

        intent.putExtras(bundle);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_SELECT_FRIENDS) {
                @SuppressWarnings("unchecked") ArrayList<TaskBudBean> fList = (ArrayList<TaskBudBean>) data.getSerializableExtra(SelectFriendsActivity.FLIST);
                addToGroup(fList);
            }
        }
    }

    /**
     * add multiple friends to current group
     */
    private void addToGroup(ArrayList<TaskBudBean> fList) {
        Debug.debug("addToGroup() flist = " + fList);

        for (int i = fList.size() - 1; i >= 0; i--) {
            TaskBudBean friend = fList.get(i);
//            Debug.debug("flist = id = " + friend.getMemberId() + " name = " + friend.getDisplayName());
            GroupMember memberBean = new GroupMember(friend.getMemberId(), friend.getDisplayName());
            currentGroup.addGroupMember(memberBean);
        }

        updateGroup();
    }

    private void openChangeImageFragment() {
        Utils.showToast(getActivity(), "Open Change Image Fragment");
        final ImageChangeFragment fragment = ImageChangeFragment.newInstance(currentGroup.getImageObjectId());
        ((MainActivity) getActivity()).addFragment(fragment, true);
        fragment.setImageChangeListener(new ImageChangeListener() {
            @Override
            public void onImageChange(String imageName) {
                fragment.showLoadingDialog();

                updateImageToDb(fragment, imageName);
            }
        });
    }

    private void updateImageToDb(final ImageChangeFragment fragment, String imageName) {
        ParseImageHandler.saveGroupImage(getActivity(), currentGroup, new OnImageUploadListener() {
            @Override
            public void onImageUpload(Bitmap bitmap, String objectId) {
                Debug.debug("updateImageToDb bitmap = " + bitmap);
                if (bitmap != null) {
                    fragment.dismissLoadingDialog();
                    groupIconImage.setImageBitmap(bitmap);
                }
            }
        });
    }

    private void openStatusChangeFragment() {
        TextChangeFragment fragment = TextChangeFragment.newInstance("Status", GroupController.getInstance().getCurrentGroup().getStatus(), 100);
        ((MainActivity) getActivity()).addFragment(fragment, true);

        fragment.setTextChangeListener(new TextChangeListener() {
            @Override
            public void onTextUpdate(String status) {
                currentGroup.setStatus(status);
                updateGroupToDb();
            }
        });
    }

    private void openNameChangeFragment() {
        TextChangeFragment fragment = TextChangeFragment.newInstance("Group Name", GroupController.getInstance().getCurrentGroup().getName(), 40);
        ((MainActivity) getActivity()).addFragment(fragment, true);

        fragment.setTextChangeListener(new TextChangeListener() {
            @Override
            public void onTextUpdate(String groupName) {
                currentGroup.setName(groupName);
                updateGroupToDb();
            }
        });
    }

    private void updateGroupToDb() {
        BackendHandler.getInstance(getActivity()).updateGroup(currentGroup, new OnGroupUpdateListener() {
            @Override
            public void onGroupUpdate(Group group) {
                getFragmentManager().popBackStackImmediate();

                titleTv.setText(group.getName());
                groupStatusTv.setText(currentGroup.getStatus());
            }
        },true);
    }
}