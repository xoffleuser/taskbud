package com.xoffle.tasktracker.group.creategroup;

import com.inkling.leaderboard.TaskBudBean;

/**
 * Created by ravi on 4/6/15.
 */
public interface OnAddFriendAdapterClickListener {
    void onCrossImageClick(TaskBudBean taskBudBean);
}
