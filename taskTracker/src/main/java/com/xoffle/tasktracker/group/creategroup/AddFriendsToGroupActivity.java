package com.xoffle.tasktracker.group.creategroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.inkling.leaderboard.AppTaskBuds;
import com.inkling.leaderboard.DataFetchListener;
import com.inkling.leaderboard.LeaderBoard;
import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.custom.LoadingDialog;
import com.xoffle.tasktracker.model.Constant;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddFriendsToGroupActivity extends ActionBarActivity implements OnAddFriendAdapterClickListener {

    private static final int REQUEST_CODE = 1;
    private ListView listView;
    private AddFriendAdapter addFriendAdapter;
    private EditText editText;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friends_to_group);

        loadingDialog = new LoadingDialog();

        setListView();

        inflateToolbar();

        setSearchView();
    }

    private void setSearchView() {
        editText = (EditText) findViewById(R.id.ActivityAddFriendEditText);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Debug.debug("onTouchEvent");
                if ((event.getAction() == MotionEvent.ACTION_UP) && event.getRawX() > (editText.getRight() - editText.getCompoundDrawables()[Constant.EditTextC.DRAWABLE_RIGHT].getBounds().width())) {
                    searchTaskBud();
                    return true;
                }
                return false;
            }
        });
    }

    private void searchTaskBud() {
        String text = editText.getText().toString();
        if (!Utils.isNull(text)) {
            searchUser(text.trim());
        }
    }

    private void searchUser(String text) {

        Debug.debug("Search-------------");
        Utils.showLoadingDialog(loadingDialog, getSupportFragmentManager());

        LeaderBoard.searchUser(text, new DataFetchListener<AppTaskBuds>() {
            @Override
            public void onFetchComplete(AppTaskBuds object, boolean isLoaded) {

            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> appTaskBudList) {

                Utils.dismissDialog(loadingDialog);

                if (appTaskBudList != null && appTaskBudList.size() > 0) {

                    for (int i = 0; i < appTaskBudList.size(); i++) {
                        AppTaskBuds appTaskBud = appTaskBudList.get(i);

                        TaskBudBean taskBudBean = new TaskBudBean(appTaskBud);
                        addFriendAdapter.addNewFriend(taskBudBean);
                    }
                }
            }
        });
    }

//    private void hideLoadingBar() {
//        if (loadingDialog != null) {
//            loadingDialog.dismiss();
//        }
//    }

    private void inflateToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activityAddFriendsToolbarSimple);
        setSupportActionBar(toolbar);
    }

    private void setListView() {
        listView = (ListView) findViewById(R.id.listViewAddFriends);
        addFriendAdapter = new AddFriendAdapter(this, R.layout.activity_add_friends_to_group, this);
        listView.setAdapter(addFriendAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_friends_to_group, menu);
        return true;
    }

    public void addParticipants(View view) {
        Intent intent = new Intent(this, SelectFriendsActivity.class);
        setExistingTaskBud(intent);
        startActivityForResult(intent, REQUEST_CODE);
    }

    private void setExistingTaskBud(Intent intent) {
        ArrayList<String> eList = new ArrayList<String>();

        Bundle b = getIntent().getExtras();
        if (b == null) {
            eList.add(DataStorage.getInstance(this).getUserId());
        } else {
            eList.addAll(b.getStringArrayList(Constant.EXISTING_USER));
        }

        ArrayList<String> list = new ArrayList<String>();

        list.addAll(eList);


        for (TaskBudBean f : addFriendAdapter.getAllItems()) {
            list.add(f.getMemberId());
        }

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constant.EXISTING_USER, list);

        intent.putExtras(bundle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add_friends) {

            createGroup();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createGroup() {
        ArrayList<TaskBudBean> friends = addFriendAdapter.getAllItems();

        Intent intent = getIntent();
        intent.putExtra(SelectFriendsActivity.FLIST, friends);
        setResult(RESULT_OK, intent);

        finish();
    }

    @Override
    protected void onActivityResult(int responceCode, int resultCode, Intent data) {

        if (responceCode == REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<Friend> fList = (ArrayList<Friend>) data.getSerializableExtra(SelectFriendsActivity.FLIST);
            System.out.println("AddFriendsToGroupActivity.onActivityResult()" + fList.size());
            addToListView(fList);
        }

        super.onActivityResult(responceCode, resultCode, data);
    }

    private void addToListView(ArrayList<Friend> fList) {
        for (int i = 0; i < fList.size(); i++) {
            System.out.println("list = " + fList.get(i).taskBudBean.getDisplayName());
            addFriendAdapter.addNewFriend(fList.get(i).taskBudBean);
        }
    }

    @Override
    public void onCrossImageClick(TaskBudBean taskBudBean) {
        addFriendAdapter.remove(taskBudBean);
    }
}