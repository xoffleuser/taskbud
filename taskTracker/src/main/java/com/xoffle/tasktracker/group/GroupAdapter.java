package com.xoffle.tasktracker.group;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.homescreen.OnImageClickListener;
import com.xoffle.tasktracker.util.DateUtil;
import com.xoffle.tasktracker.util.ImageUtil;

public class GroupAdapter extends ArrayAdapter<Group> {

    private LayoutInflater inflater;

    private Context context;
    private OnImageClickListener listener;

    public GroupAdapter(Context context, OnImageClickListener listener, int resource) {
        super(context, resource);
        this.context = context;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Group group = (Group) getItem(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.group_view, parent, false);

            GroupHolder groupHolder = new GroupHolder(convertView, group, getContext(), listener);
            convertView.setTag(groupHolder);
        } else {
            GroupHolder groupHolder = (GroupHolder) convertView.getTag();
            groupHolder.setData(group, getContext());
        }

//        registerForContextMenu(v);

        return convertView;
    }

    public void addNewGroup(Group group) {
        if(!isContain(group))
        add(group);
    }

    private boolean isContain(Group group) {
        for (int i = getCount()-1; i>=0;i--){
            Group g = getItem(i);

            if(g.getGroupId().equals(group.getGroupId()))
                return true;
        }

        return false;
    }

    public void sort() {
        sort(new GroupComparator());
        notifyDataSetChanged();
    }

    private static class GroupHolder implements View.OnClickListener {
        ImageView iconImageView;
        TextView groupNameTextView;
        TextView membersNameTextView;
        TextView dateTextView;
        TextView groupCounterTextView;
        private Group group;
        private OnImageClickListener listener;

        GroupHolder(View view, Group group, Context context, OnImageClickListener listener) {
            this.listener = listener;
            findViewsById(view);
            setData(group, context);
        }

        private void setListeners() {
            iconImageView.setOnClickListener(this);
        }

        private void setData(Group group, Context context) {
            this.group = group;

            iconImageView.setFocusable(false);
            groupNameTextView.setText(group.getName());
            membersNameTextView.setText(group.getStatus());
            dateTextView.setText(DateUtil.format(group.getUpdateTime(), DateUtil.FORMAT_DATE_1));

            if (group.getUpdateCount() == 0) {
                groupCounterTextView.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                groupCounterTextView.setText("");
            } else {
                groupCounterTextView.setBackgroundColor(context.getResources().getColor(R.color.green));
                groupCounterTextView.setText(group.getUpdateCount() + "");
            }

            if (group.isMyGroup()) {
                groupNameTextView.setTextColor(context.getResources().getColor(R.color.blue));
            } else {
                groupNameTextView.setTextColor(context.getResources().getColor(R.color.black));
            }

            downloadImage();

            setListeners();
        }

        private void downloadImage() {
            if (!ImageUtil.setImageFromStorage(iconImageView, group.getImageObjectId())) {
                ImageUtil.setImageDrawableResToView(iconImageView, R.drawable.ic_launcher);
            }
        }

        private void findViewsById(View view) {
            iconImageView = (ImageView) view.findViewById(R.id.imageButtonIcon);
            groupNameTextView = ((TextView) view.findViewById(R.id.textViewGroupName));
            membersNameTextView = ((TextView) view.findViewById(R.id.textViewMembers));
            dateTextView = ((TextView) view.findViewById(R.id.group_view_tv_time));
            groupCounterTextView = (TextView) view.findViewById(R.id.group_view_tv_group_counter);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imageButtonIcon:
                    listener.onImageClick(group);
            }
        }
    }
}