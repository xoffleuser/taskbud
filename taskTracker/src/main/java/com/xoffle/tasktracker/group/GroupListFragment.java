package com.xoffle.tasktracker.group;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.inkling.leaderboard.TaskBudBean;
import com.xoffle.tasktracker.R;
import com.xoffle.tasktracker.controller.BackendHandler;
import com.xoffle.tasktracker.custom.ImageDialog;
import com.xoffle.tasktracker.custom.LoadingDialog;
import com.xoffle.tasktracker.db.DBController;
import com.xoffle.tasktracker.group.creategroup.CreateGroupActivity;
import com.xoffle.tasktracker.group.creategroup.SelectFriendsActivity;
import com.xoffle.tasktracker.homescreen.MainActivity;
import com.xoffle.tasktracker.homescreen.OnImageClickListener;
import com.xoffle.tasktracker.image.ParseImageHandler;
import com.xoffle.tasktracker.json.JsonParser;
import com.xoffle.tasktracker.listeners.OnImageUploadListener;
import com.xoffle.tasktracker.model.DataStorage;
import com.xoffle.tasktracker.task.TaskListFragment;
import com.xoffle.tasktracker.util.Debug;
import com.xoffle.tasktracker.util.Utils;
import com.xoffle.tasktracker.view.MyFragment;

import java.util.ArrayList;

/**
 * Created by ravi on 16/5/15.
 */
public class GroupListFragment extends MyFragment implements OnGroupLoadListener {

    private static final int REQUEST_CODE_CREATE_NEW_GROUP = 1;

    private ListView listView;
    private GroupAdapter groupAdapter;

    private LoadingDialog loadingDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group_list_fragment, container, false);

        init();

        inflateToolbar(view);

        inflateListView(view);

        DBController.getInstance(getActivity()).loadAllData(this);
        return view;
    }

    private void init() {
        loadingDialog = new LoadingDialog();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.group_create, menu);

        this.menu = menu;
        this.menuInflater = inflater;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
//        System.out.println("onHiddenChanged GroupList hidden = " + hidden);
        super.onHiddenChanged(hidden);

        if (hidden) {

        } else {

            groupAdapter.clear();
            DBController.getInstance(getActivity()).loadAllData(this);

            groupAdapter.notifyDataSetChanged();
            if (menu != null) {
                menu.clear();
                menuInflater.inflate(R.menu.group_create, menu);
                groupAdapter.sort();
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    private void inflateToolbar(View view) {

        toolbar = (Toolbar) view.findViewById(R.id.groupListFragmentToolbar);
        ((ActionBarActivity) getActivity()).setSupportActionBar(toolbar);
        enableHomeButton(R.drawable.ic_list_white);
    }

    private void inflateListView(View view) {

        listView = (ListView) view.findViewById(R.id.fragmentGroupListListView);

        registerForContextMenu(listView);

        groupAdapter = new GroupAdapter(getActivity(), new OnImageClickListener() {
            @Override
            public void onImageClick(Group group) {
                showImagePopup(group);
            }
        }, R.layout.activity_main);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                openGroup(position);
                                                Debug.debug("item clicked = " + position);
                                            }
                                        }

        );

        listView.setAdapter(groupAdapter);

//        DBController.getInstance(getActivity()).loadAllData(this);
    }

    private void showImagePopup(Group group) {
        ImageDialog imageDialog = ImageDialog.newInstance(group.getName(), group.getImageObjectId(), null);
        imageDialog.show(getFragmentManager());
    }

    private void openGroup(int position) {

        Group group = groupAdapter.getItem(position);
        group.setUpdateCount(0);
        groupAdapter.notifyDataSetChanged();

        GroupController.getInstance().setCurrentGroup(group);

        ((MainActivity) getActivity()).addFragment(new TaskListFragment(), true);
    }

    @Override
    public void onGroupLoaded(Group group) {
        if (loadingDialog != null && loadingDialog.isVisible()) {
            loadingDialog.dismiss();
        }

        groupAdapter.addNewGroup(group);
        groupAdapter.sort();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                ((MainActivity) getActivity()).openDrawerLayout();
                return true;
            case R.id.action_create_group:
                createNewGroup();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void createNewGroup() {
        if (!Utils.isNetworkAvailble(getActivity())) {
            Utils.showToast(getActivity(), getString(R.string.no_internet_connection));
            return;
        }

        Intent intent = new Intent(getActivity(), CreateGroupActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CREATE_NEW_GROUP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CODE_CREATE_NEW_GROUP) {

                @SuppressWarnings("unchecked") ArrayList<TaskBudBean> fList = (ArrayList<TaskBudBean>) data.getSerializableExtra(SelectFriendsActivity.FLIST);
                String groupName = data.getStringExtra(CreateGroupActivity.GROUP_NAME);

                addToAdmin(fList);
                createNewGroup(groupName, fList);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * this add me in member list of group
     * @param fList
     *         friend list selected for this group
     */
    private void addToAdmin(ArrayList<TaskBudBean> fList) {
        DataStorage store = DataStorage.getInstance(getActivity());
        fList.add(new TaskBudBean(store.getUserId(), store.getDisplayName()));
    }

    /**
     * create new group to local as well as on parse
     * @param groupName
     * @param fList
     */
    private void createNewGroup(String groupName, ArrayList<TaskBudBean> fList) {
        if (!Utils.isNull(groupName)) {
            loadingDialog.show(getFragmentManager());

            final DataStorage store = DataStorage.getInstance(null);

            final String adminId = store.getUserId();

            BackendHandler.getInstance(getActivity()).createGroup(adminId, groupName, fList, new OnGroupCreateListener() {
                public void onGroupCreated(final Group group) {
                    if (group != null) {

                        onGroupLoaded(group);
                        groupAdapter.sort();

                        ParseImageHandler.saveGroupImage(getActivity(), group, new OnImageUploadListener() {
                            @Override
                            public void onImageUpload(Bitmap bitmap, String objectId) {

//                                Utils.ImageUtils.saveOrUpdateImage(getActivity(), bitmap, group.getImageObjectId());
                                Utils.showToast(getActivity(), getActivity().getString(R.string.image_upload_msg));
                                groupAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (groupAdapter != null) {
            groupAdapter.sort();
        }
        updateApp();

    }

    private void updateApp() {
        JsonParser.parseAppUpdates(getActivity(), new JsonParser.OnParseUpdateListener() {

            @Override
            public void onGroupMemberAddedToGroup() {
                groupAdapter.notifyDataSetChanged();
            }

            @Override
            public void onGroupLoaded(Group group) {
                groupAdapter.addNewGroup(group);
            }

            @Override
            public void updateGroupUpdates(String groupId) {

                for (int i = groupAdapter.getCount() - 1; i >= 0; i--) {
                    Group group = groupAdapter.getItem(i);

                    if (group.contains(groupId)) {
                        group.setUpdateCount(group.getUpdateCount() + 1);
                    }
                }

                groupAdapter.notifyDataSetChanged();
            }
        });
    }
}