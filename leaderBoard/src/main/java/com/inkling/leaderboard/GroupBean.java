package com.inkling.leaderboard;

import java.util.ArrayList;

/**
 * Created by ravi on 6/7/15.
 */
public class GroupBean {

    protected ArrayList<String> memberBeanIdList;

    protected String groupId;
    protected String name;
    protected String adminId;
    protected String creatorId;
    protected String status;
    protected long statusUpdateOn;
    protected long createTime;
    protected long updateTime;
    protected boolean isMyGroup;
    private String imageObjectId;

    public GroupBean() {
    }

    public GroupBean(AppGroup appGroup) {
        groupId = appGroup.getObjectId();
        name = appGroup.getGroupName();
        adminId = appGroup.getAdminId();
        creatorId = appGroup.getCreatorId();
        status = appGroup.getGroupStatus();
        statusUpdateOn = appGroup.getStatusUpdateOn();
        createTime = appGroup.getCreatedAt().getTime();
        updateTime = appGroup.getUpdatedAt().getTime();
        isMyGroup = appGroup.isMyGroup();

        imageObjectId = appGroup.getImageObjectId();
    }

    public ArrayList<String> getMemberBeanIdList() {
        return memberBeanIdList;
    }

    public void setMemberBeanIdList(ArrayList<String> memberBeanIdList) {
        this.memberBeanIdList = memberBeanIdList;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getStatusUpdateOn() {
        return statusUpdateOn;
    }

    public void setStatusUpdateOn(long statusUpdateOn) {
        this.statusUpdateOn = statusUpdateOn;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isMyGroup() {
        return isMyGroup;
    }

    public void setMyGroup(boolean isMyGroup) {
        this.isMyGroup = isMyGroup;
    }

    public void setImageId(String imageObjectId) {
        this.imageObjectId = imageObjectId;
    }

    public String getImageObjectId() {
        return imageObjectId;
    }

//    public ArrayList<String> getGroupMembersId() {
//        ArrayList<String> list = new ArrayList<String>();
//        if (memberBeanIdList != null) {
//            for (int i = memberBeanIdList.size() - 1; i >= 0; i--) {
//                list.add(memberBeanIdList.get(i).getMemberId());
//            }
//        }
//        return list;
//    }

//    public void removeGroupMember(String memberId) {
//        if (memberBeanIdList != null) {
//            for (int i = memberBeanIdList.size() - 1; i >= 0; i--) {
//                if (memberBeanIdList.get(i).getMemberId().equals(memberId)) {
//                    memberBeanIdList.remove(memberBeanIdList.get(i));
//                }
//            }
//        }
//    }
}
