package com.inkling.leaderboard;

/**
 * Created by ravi on 7/6/15.
 */
public class SeenBy {
    private String userId;
    private int status;
    private long when;

    public SeenBy(String userId,int status,long when)
    {
        this.userId = userId;
        this.status = status;
        this.when = when;
    }

    public SeenBy() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getWhen() {
        return when;
    }

    public void setWhen(long when) {
        this.when = when;
    }
}
