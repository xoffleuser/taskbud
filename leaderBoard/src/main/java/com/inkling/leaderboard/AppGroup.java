package com.inkling.leaderboard;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ParseClassName(value = "AppGroup")
public class AppGroup extends ParseObject implements Serializable {

    protected static final String ClassName = "AppGroup";
    private static final long serialVersionUID = -6241390411224337772L;
    private static final String GROUP_NAME = "GroupName";
    private static final String STATUS = "Status";
    private static final String STATUS_UPDATE_ON = "StatusUpadateOn";
    private static final String ADMIN_ID = "AdminId";/*Facebook Id*/
    private static final String CREATOR_ID = "CreatorId";/*Facebook Id*/
    private static final String GROUP_MEMBERS = "GROUP_MEMBERS";
    private static final String IS_MY_GROUP = "IS_MY_GROUP";
    private static final String DEFAULT_MY_GROUP_NAME = "MyTasks";
    private static final String DEFAULT_STATUS = "Taskbud only";
    private static final String IMAGE_OBJECT_ID = "ImageId";

    private static void debug(String msg) {
        LeaderBoard.debug("AppGroup: " + msg);
    }

    public static void load(final String objectId, final DataFetchListener<AppGroup> listener) {
        debug("Loading new AppGroup");

        ParseQuery<AppGroup> query = new ParseQuery<AppGroup>(ClassName);

        query.getInBackground(objectId, new GetCallback<AppGroup>() {
            @Override
            public void done(AppGroup appGroup, ParseException e) {
                debug("Load= Exception : " + e + "   List of Object: " + appGroup);
                if (e != null || appGroup == null) {
                    listener.onFetchComplete(null, true);
                } else {
                    listener.onFetchComplete(appGroup, true);
                }
            }
        });
    }

    public static void createNew(String groupName, String adminAndCreatorId, String status, ArrayList<String> mList, final DataFetchListener<AppGroup> listener) {
        debug("Creating new AppGroup");

        final AppGroup appGroup = new AppGroup();
        appGroup.setGroupName(groupName);
        appGroup.setAdminId(adminAndCreatorId);
        appGroup.setCreatorId(adminAndCreatorId);
        appGroup.setGroupStatus(status);
        appGroup.setIsMyGroup(false);
        appGroup.setStatusUpdateOn(System.currentTimeMillis());

        appGroup.setGroupMembers(mList);

        appGroup.saveInBackground(new SaveCallback() {

            public void done(ParseException e) {
                debug("Create New user=  Exception :" + e);
                if (e == null) {
                    listener.onFetchComplete(appGroup, false);
                } else {
                    listener.onFetchComplete(null, false);
                }
            }
        });
    }

    public static void createMyGroup(String adminAndCreatorId, final DataFetchListener<AppGroup> listener) {
        debug("Creating new AppGroup");

        final AppGroup appGroup = new AppGroup();
        appGroup.setGroupName(DEFAULT_MY_GROUP_NAME);
        appGroup.setAdminId(adminAndCreatorId);
        appGroup.setCreatorId(adminAndCreatorId);
        appGroup.setGroupStatus(DEFAULT_STATUS);
        appGroup.setIsMyGroup(true);
        appGroup.setStatusUpdateOn(System.currentTimeMillis());

        appGroup.saveInBackground(new SaveCallback() {

            public void done(ParseException e) {
                debug("Create New user=  Exception :" + e);
                if (e == null) {
                    listener.onFetchComplete(appGroup, false);
                } else {
                    listener.onFetchComplete(null, false);
                }
            }
        });
    }

//    public static void loadOrCreate(String groupName,  final ArrayList<String> mList, final DataFetchListener<AppGroup> listener) {
//        load(gId, gCount, new DataFetchListener<AppGroup>() {
//
//            public void onFetchComplete(AppGroup object, boolean isLoaded) {
//                debug("LoadOrCreate=  NeedNew?: " + (object == null));
//                // New user
//                if (object == null) {
//                    createNew(gId, gCount, gName, mList, listener);
//                }
//                // old User
//                else {
//                    listener.onFetchComplete(object, isLoaded);
//                }
//            }
//
//            @Override
//            public void onListFetchComplete(List<AppGroup> objects) {
//
//            }
//        });
//    }

    public static void updateAppGroup(String objectId, final String groupNameNew, final String status, final ArrayList<String> membersList, final String adminId, final DataFetchListener<AppGroup> listener) {
        load(objectId, new DataFetchListener<AppGroup>() {

            public void onFetchComplete(final AppGroup appGroup, final boolean isLoaded) {

                if (appGroup != null) {

                    if (groupNameNew != null) {
                        appGroup.setGroupName(groupNameNew);
                    }
                    if (status != null) {
                        appGroup.setGroupStatus(status);
                        appGroup.setStatusUpdateOn(System.currentTimeMillis());
                    }

                    if (adminId != null) {
                        appGroup.setAdminId(adminId);
                    }
                    if (membersList != null) {
                        appGroup.setGroupMembers(membersList);
                    }

                    appGroup.saveInBackground(new SaveCallback() {

                        public void done(ParseException e) {
                            if (e == null) {
                                listener.onFetchComplete(appGroup, true);
                            } else {
                                listener.onFetchComplete(null, isLoaded);
                            }
                        }
                    });
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {

            }
        });
    }

    public static void deleteAppGroup(String objectId, final DataFetchListener<AppGroup> listener) {
        load(objectId, new DataFetchListener<AppGroup>() {

            public void onFetchComplete(final AppGroup object, final boolean isLoaded) {

                if (object != null) {
                    object.deleteInBackground(new DeleteCallback() {

                        public void done(ParseException e) {
                            if (e == null) {
                                listener.onFetchComplete(object, isLoaded);
                            } else {
                                listener.onFetchComplete(null, isLoaded);
                            }
                        }
                    });
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {
            }
        });
    }

    public static void loadAllMyGroups(String adminId, final DataFetchListener listener) {
        ParseQuery<AppGroup> query = new ParseQuery<AppGroup>(AppGroup.ClassName);
        query.whereEqualTo(ADMIN_ID, adminId);

        query.findInBackground(new FindCallback<AppGroup>() {

            public void done(List<AppGroup> objects, ParseException e) {
                debug("load=> Exception: " + e + "  , List: " + objects);
                if (e != null || objects == null || objects.size() == 0) {
                    listener.onListFetchComplete(null);
                } else {
                    listener.onListFetchComplete(objects);
                }
            }
        });
    }

    public static void loadMyGroups(String adminId, final DataFetchListener listener) {
        ParseQuery<AppGroup> query = new ParseQuery<AppGroup>(AppGroup.ClassName);
        query.whereEqualTo(ADMIN_ID, adminId);
        query.whereEqualTo(IS_MY_GROUP, true);

        query.findInBackground(new FindCallback<AppGroup>() {

            public void done(List<AppGroup> objects, ParseException e) {
                debug("load=> Exception: " + e + "  , List: " + objects);
                if (e != null || objects == null || objects.size() == 0) {
                    listener.onFetchComplete(null, true);
                } else {
                    listener.onFetchComplete(objects.get(0), true);
                }
            }
        });
    }

    public static void updateAppGroupUpdateTime(String groupId, final DataFetchListener<AppGroup> dataFetchListener) {
        load(groupId, new DataFetchListener<AppGroup>() {
            @Override
            public void onFetchComplete(final AppGroup object, boolean isLoaded) {
                if (object != null) {
                    object.setStatusUpdateOn(object.getStatusUpdateOn());
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                dataFetchListener.onFetchComplete(object, true);
                            }
                        }
                    });
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {

            }
        });
    }

    public static void updateAppGroup(final GroupBean groupBean, final DataFetchListener<AppGroup> dataFetchListener) {
        load(groupBean.getGroupId(), new DataFetchListener<AppGroup>() {
            @Override
            public void onFetchComplete(final AppGroup object, boolean isLoaded) {
                if (object != null) {
                    updateObject(object, groupBean);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                dataFetchListener.onFetchComplete(object, true);
                            }
                        }
                    });
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {

            }
        });
    }

    private static void updateObject(AppGroup appGroup, GroupBean groupBean) {

        appGroup.setGroupName(groupBean.getName());
        appGroup.setAdminId(groupBean.getAdminId());
        appGroup.setCreatorId(groupBean.getCreatorId());
        appGroup.setGroupStatus(groupBean.getStatus());
        appGroup.setIsMyGroup(groupBean.isMyGroup());
        appGroup.setStatusUpdateOn(groupBean.getStatusUpdateOn());
        appGroup.setImageObjectId(groupBean.getImageObjectId());
        appGroup.setGroupMembers(groupBean.getMemberBeanIdList());
    }

    public String getGroupName() {
        return getString(GROUP_NAME);
    }

    private void setGroupName(String groupName) {
        put(GROUP_NAME, groupName);
    }

    public String getGroupStatus() {
        return getString(STATUS);
    }

    private void setGroupStatus(String status) {
        put(STATUS, status);
    }

    public long getStatusUpdateOn() {
        return getLong(STATUS_UPDATE_ON);
    }

    private void setStatusUpdateOn(long statusUpdateOn) {
        put(STATUS_UPDATE_ON, statusUpdateOn);
    }

    public String getAdminId() {
        return getString(ADMIN_ID);
    }

    private void setAdminId(String adminId) {
        put(ADMIN_ID, adminId);
    }

    public String getCreatorId() {
        return getString(CREATOR_ID);
    }

    private void setCreatorId(String creatorId) {
        put(CREATOR_ID, creatorId);
    }

    public List<String> getGroupMembers() {
        return getList(GROUP_MEMBERS);
    }

    private void setGroupMembers(ArrayList<String> groupMembers) {
        put(GROUP_MEMBERS, groupMembers);
    }

    private void setIsMyGroup(boolean isMyGroup) {
        put(IS_MY_GROUP, isMyGroup);
    }

    public boolean isMyGroup() {
        return getBoolean(IS_MY_GROUP);
    }

//    public void setGroupMembers(ArrayList<String> mList, boolean isUpdate) {
//        if (!isUpdate && getGroupMembers() != null) {
//            debug("Can't Change Members once set.");
//            return;
//        }
//        put(GROUP_MEMBERS, mList);
//    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("[").append(getGroupName()).append(getAdminId()).append(getCreatorId()).append(getGroupMembers()).append(getGroupStatus()).append(getStatusUpdateOn()).append("]");
        return buffer.toString();
    }

    public String getImageObjectId() {
        return getString(IMAGE_OBJECT_ID);
    }

    public void setImageObjectId(String imageObjectId) {
        if (imageObjectId != null) {
            put(IMAGE_OBJECT_ID, imageObjectId);
        }
    }
}