package com.inkling.leaderboard;

import com.parse.ParseFile;

/**
 * Created by ravi on 21/7/15.
 */
public interface ParseImageListener {
    void onImageSaveOrUpdate(AppImages appImages);

    void onImageDownload(byte[] bytes);
}
