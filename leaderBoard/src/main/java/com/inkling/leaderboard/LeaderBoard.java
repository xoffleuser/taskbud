package com.inkling.leaderboard;

import com.inkling.leaderboard.Installation.MessageSendCallback;
import com.inkling.leaderboard.LeaderBoardListener.OnGroupFetchListener;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *  <b> Fetch method won't change any data while fetching.</b>
 *  <b> Update method  will update details if required</b>
 * </pre>
 */
public class LeaderBoard {

    /**
     * Tag used in systrace for this class.
     */
    private static final String TAG_LEADER_BOARD = "<<==== LeaderBoard ====>> ";
    private static boolean DEBUG = false;

    /**
     * @param debug
     *         true to systrace all logs.
     */
    public static void setDebug(boolean debug) {
        DEBUG = debug;
        Util.setDebug(debug);
    }

    static void debug(Object object) {
        if (DEBUG) {
            System.out.println(TAG_LEADER_BOARD + object);
        }
    }

    /**
     * Call this method from onFetchComplete.
     *
     * <pre>
     * <b>Note: Also call  Parse.initialize(context, id, key);</b>
     * </pre>
     *
     * Didn't call because want to make a simple java project.
     */
    public static void initialize() {
        debug("LeaderBoard Initialize");
        ParseObject.registerSubclass(AppGroup.class);
        ParseObject.registerSubclass(AppGroupTask.class);
        ParseObject.registerSubclass(AppTaskBuds.class);
        ParseObject.registerSubclass(AppImages.class);
    }

    public static void subscribe(final OnSubscrib onSubscrib) {
        ParsePush.subscribeInBackground("", new SaveCallback() {

            public void done(ParseException e) {
                if (e == null) {

                    debug("successfully subscribed to the broadcast channel");

                    if (onSubscrib != null) {
                        onSubscrib.onSubsrib(true);
                    }

                } else {
                    debug("failed to subscribe for push " + e.getMessage());

                    if (onSubscrib != null) {
                        onSubscrib.onSubsrib(false);
                    }
                }
            }
        });
    }

    public static void updateUserIdToInstallation(String userId, DataFetchListener<ParseInstallation> listener) {
        Installation.createInstallation(userId, listener);
    }

    public static void sendJsonNotification(String userId, JSONObject jsonObject, MessageSendCallback listener) {
        if (Util.isNull(userId) || jsonObject == null || Util.isNull(jsonObject.toString())) {
            if (listener != null) {
                listener.onComplete(false, "userName or msg is null");
            }
            debug("UserId or mag is null");
            return;
        }

        Installation.sendMessage(userId, jsonObject, listener);
    }

    /* all activities related to installation object*/

    public static void loadGroup(String objectId, final OnGroupFetchListener listener) {
        debug("Fetch Group");
        AppGroup.load(objectId, new DataFetchListener<AppGroup>() {

            public void onFetchComplete(AppGroup object, boolean isLoaded) {
                listener.onCreate(object, isLoaded);
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {
            }
        });
    }

    public static void createGroup(String groupName, String adminAndCreatorId, String status, ArrayList<String> groupMembersList, final OnGroupFetchListener listener) {
        debug("Fetch Group");
        AppGroup.createNew(groupName, adminAndCreatorId, status, groupMembersList, new DataFetchListener<AppGroup>() {

            public void onFetchComplete(AppGroup object, boolean isLoaded) {
                listener.onCreate(object, isLoaded);
            }

            @Override
            public void onListFetchComplete(List<AppGroup> objects) {
            }
        });
    }

    public static void createMyGroup(String adminOrCreatorId, DataFetchListener listener) {
        AppGroup.createMyGroup(adminOrCreatorId, listener);
    }

    /* related task on notification*/

    /**
     * load all the groups which contain admin id as a groupId as well as membersId of group
     * @param adminId
     *         userId which is included in groupId or memberId in group
     * @param listener
     */
    public static void fetchAllMyGroups(String adminId, DataFetchListener listener) {
        AppGroup.loadAllMyGroups(adminId, listener);
    }

    public static void fetchMyGroups(String adminId, DataFetchListener listener) {
        AppGroup.loadMyGroups(adminId, listener);
    }

    /*activities related to Group*/

    public static void loadGroupTask(String objectId, final DataFetchListener<AppGroupTask> listener) {
        AppGroupTask.load(objectId, new DataFetchListener<AppGroupTask>() {

            public void onFetchComplete(AppGroupTask object, boolean isLoaded) {
                if (object != null) {
                    listener.onFetchComplete(object, isLoaded);
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {
            }
        });
    }

    /*Activities related to Task*/

    public static void createGroupTask(Task task, DataFetchListener<AppGroupTask> listener) {
        AppGroupTask.createNew(task, listener);
    }

    public static void deleteGroupTask(String objectId, DataFetchListener<AppGroupTask> listener) {
        AppGroupTask.deleteAppTask(objectId, listener);
    }

    public static void fetchAllTaskOfGroup(String groupObjectId, DataFetchListener<AppGroupTask> listener) {
        AppGroupTask.loadAllTaskOfGroup(groupObjectId, listener);
    }

    public static void updateTask(Task task, DataFetchListener<AppGroupTask> listener) {
        AppGroupTask.updateAppTask(task, listener);
    }

    /**
     * load unique user of app
     * @param userId
     * @param listener
     */
    public static void loadAppMember(String userId, DataFetchListener<AppTaskBuds> listener) {
        if (userId == null) {
            listener.onFetchComplete(null, false);
            return;
        }

        AppTaskBuds.load(userId, AppTaskBuds.LOAD_BY_USER_ID, listener);
    }

    /**
     * load existing AppMember if not exist then create create new one.
     * @param listener
     */
    public static void loadOrCreateAppMember(TaskBudBean taskBudBean, DataFetchListener<AppTaskBuds> listener) {
        AppTaskBuds.loadOrCreate(taskBudBean, listener);
    }

    public static void checkAvailability(String username, DataFetchListener<AppTaskBuds> listener) {
        AppTaskBuds.load(username, AppTaskBuds.LOAD_BY_USERNAME, listener);
    }

    public static void updateUsername(String userId, String username, DataFetchListener<AppTaskBuds> dataFetchListener) {
        AppTaskBuds.updateUserName(userId, username, dataFetchListener);
    }

    public static void updateAppTaskBud(TaskBudBean taskBudBean, DataFetchListener<AppTaskBuds> dataFetchListener) {
        AppTaskBuds.update(taskBudBean, dataFetchListener);
    }

    public static void updateAppGroupUpdateTime(String groupId, DataFetchListener<AppGroup> dataFetchListener) {
        AppGroup.updateAppGroupUpdateTime(groupId, dataFetchListener);
    }

    public static void updateAppGroup(GroupBean groupBean, DataFetchListener<AppGroup> dataFetchListener) {
        AppGroup.updateAppGroup(groupBean, dataFetchListener);
    }

    public static void searchUser(String text, DataFetchListener<AppTaskBuds> dataFetchListener) {
        AppTaskBuds.searchTaskBuds(text, dataFetchListener);
    }

    public static void saveOrUpdateImage(String imageObjectId, byte[] bytes, ParseImageListener listener) {
        AppImages.saveOrUpdateFile(imageObjectId, bytes, listener);
    }

    public static void downloadImage(String imageObjectId, ParseImageListener listener) {
        AppImages.getImage(imageObjectId, listener);
    }

    /* activities related to App Members*/

    public interface OnSubscrib {
        void onSubsrib(boolean isSuccessfullySubsribed);
    }
}
