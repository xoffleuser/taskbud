package com.inkling.leaderboard;

import com.parse.ParseObject;

import java.util.List;

public interface DataFetchListener<T extends ParseObject> {

	public void onFetchComplete(T object,boolean isLoaded);

    public void onListFetchComplete(List<T> objects);
}
