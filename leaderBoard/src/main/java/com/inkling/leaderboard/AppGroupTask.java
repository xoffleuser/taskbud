package com.inkling.leaderboard;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ParseClassName(value = "AppGroupTask")
public class AppGroupTask extends ParseObject implements Serializable {

    protected static final String ClassName = "AppGroupTask";
    private static final long serialVersionUID = -6241390411224337772L;
    private static final String ACK_CNF_ID = "AckCnfId";
    private static final String ACK_TIME = "AckTime";
    private static final String CNF_TIME = "CnfTime";
    private static final String IS_ACKD = "IsAckd";
    private static final String IS_CNFD = "IsConfd";
    private static final String IS_DELETED = "IsDeleted";
    private static final String UNSUBSCRIBED_BY = "UnsubscribedBy";
    private static final String SUBJECT = "Subject";
    private static final String DESCRIPTION = "Description";
    private static final String REMARK = "Remark";
    private static final String TASK_CREATOR_ID = "TaskCreatorId";
    private static final String SEEN_BY = "SeenBy";
    private static final String EXPIRE_DATE = "ExpireDate";
    private static final String GROUP_OBJECT_ID = "GroupObjectId";

    public static void load(String objectId, final DataFetchListener<AppGroupTask> listener) {
        ParseQuery<AppGroupTask> query = new ParseQuery<AppGroupTask>(ClassName);

//        query.whereEqualTo()

        query.getInBackground(objectId, new GetCallback<AppGroupTask>() {
            @Override
            public void done(AppGroupTask appGroupTask, ParseException e) {
                if (e != null || appGroupTask == null) {
                    listener.onFetchComplete(null, true);
                } else {
                    listener.onFetchComplete(appGroupTask, true);
                }
            }
        });
    }

    public static void createNew(Task task, final DataFetchListener<AppGroupTask> listener) {
        final AppGroupTask appTask = new AppGroupTask();

        setData(appTask, task);

        appTask.saveInBackground(new SaveCallback() {

            public void done(ParseException e) {
                if (e == null) {
                    listener.onFetchComplete(appTask, false);
                } else {
                    listener.onFetchComplete(null, false);
                }
            }
        });
    }

    private static void setData(AppGroupTask appTask, Task task) {
        if (task.getAckCnfID() != null) {
            appTask.setAckCnfId(task.getAckCnfID());
        }

        if (task.getAckTime() != 0) {
            appTask.setAckTime(task.getAckTime());
        }

        if (task.getCnfTime() != 0) {
            appTask.setCnfTime(task.getCnfTime());
        }

        if (task.getDescription() != null) {
            appTask.setDescription(task.getDescription());
        }

        if (task.getExpireDate() != 0) {
            appTask.setExpireDate(task.getExpireDate());
        }

        if (task.getRemark() != null) {
            appTask.setRemark(task.getRemark());
        }

        if (task.getSeenByList() != null) {
            appTask.setSeenBy(task.getSeenByList());
        }

        if (task.getSubject() != null) {
            appTask.setSubject(task.getSubject());
        }

        if (task.getTaskCreaterId() != null) {
            appTask.setTaskCreatorsId(task.getTaskCreaterId());
        }

        if (task.getGroupObjectId() != null) {
            appTask.setGroupObjectId(task.getGroupObjectId());
        }

        appTask.setIsACKD(task.isAckd());
        appTask.setIsCnfd(task.isCnfd());
        appTask.setIsDeleted(task.isDeleted());
    }

    public static void updateAppTask(final Task task, final DataFetchListener<AppGroupTask> listener) {
        load(task.getObjectId(), new DataFetchListener<AppGroupTask>() {

            public void onFetchComplete(final AppGroupTask appTask, final boolean isLoaded) {

                if (appTask != null) {
                    setData(appTask, task);
                    appTask.saveInBackground(new SaveCallback() {

                        public void done(ParseException e) {
                            if (e == null) {
                                listener.onFetchComplete(appTask, isLoaded);
                            } else {
                                listener.onFetchComplete(null, isLoaded);
                            }
                        }
                    });
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    public static void deleteAppTask(String objectId, final DataFetchListener<AppGroupTask> listener) {
        load(objectId, new DataFetchListener<AppGroupTask>() {

            public void onFetchComplete(final AppGroupTask object, final boolean isLoaded) {

                if (object != null) {
                    object.setIsDeleted(true);

                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                listener.onFetchComplete(object, isLoaded);
                            } else {
                                listener.onFetchComplete(null, isLoaded);
                            }
                        }
                    });
                }
            }

            @Override
            public void onListFetchComplete(List<AppGroupTask> objects) {

            }
        });
    }

    public static void loadAllTaskOfGroup(String groupObjectId, final DataFetchListener<AppGroupTask> listener) {
        ParseQuery<AppGroupTask> query = new ParseQuery<AppGroupTask>(AppGroupTask.ClassName);

        query.whereEqualTo(GROUP_OBJECT_ID, groupObjectId);

        query.findInBackground(new FindCallback<AppGroupTask>() {

            public void done(List<AppGroupTask> objects, ParseException e) {
                if (e != null || objects == null || objects.size() == 0) {
                    listener.onListFetchComplete(null);
                } else {
                    listener.onListFetchComplete(objects);
                }
            }
        });
    }

    public String getAckCnfId() {
        return getString(ACK_CNF_ID);
    }

    private void setAckCnfId(String ackCnfId) {
        put(ACK_CNF_ID, ackCnfId);
    }

    public long getAckTime() {
        return getLong(ACK_TIME);
    }

    private void setAckTime(long ackTime) {
        put(ACK_TIME, ackTime);
    }

    public long getCnfTime() {
        return getLong(CNF_TIME);
    }

    private void setCnfTime(long cnfTime) {
        put(CNF_TIME, cnfTime);
    }

    private void setIsACKD(boolean isAckd) {
        put(IS_ACKD, isAckd);
    }

    public boolean getIsAckd() {
        return getBoolean(IS_ACKD);
    }

    public boolean getIsCnfd() {
        return getBoolean(IS_CNFD);
    }

    private void setIsCnfd(boolean isCnfd) {
        put(IS_CNFD, isCnfd);
    }

    public boolean getIsDeleted() {
        return getBoolean(IS_DELETED);
    }

    private void setIsDeleted(boolean isDeleted) {
        put(IS_DELETED, isDeleted);
    }

    public List<String> getUnsubscribedBy() {
        return getList(UNSUBSCRIBED_BY);
    }

    private void setUnsubscribedBy(ArrayList<String> list) {
        put(UNSUBSCRIBED_BY, list);
    }

    public String getSubject() {
        return getString(SUBJECT);
    }

    private void setSubject(String subject) {
        put(SUBJECT, subject);
    }

    public String getDescription() {
        return getString(DESCRIPTION);
    }

    private void setDescription(String description) {
        put(DESCRIPTION, description);
    }

    public String getRemark() {
        return getString(REMARK);
    }

    private void setRemark(String remark) {
        put(REMARK, remark);
    }

    private void setTaskCreatorsId(String taskCreatorsId) {
        put(TASK_CREATOR_ID, taskCreatorsId);
    }

    public String getTaskCreatorId() {
        return getString(TASK_CREATOR_ID);
    }

    public List<String> getSeenBy() {
        return getList(SEEN_BY);
    }

    private void setSeenBy(ArrayList<String> seenBy) {
        put(SEEN_BY, seenBy);
    }

    public long getExpireDate() {
        return getLong(EXPIRE_DATE);
    }

    private void setExpireDate(long expireDate) {
        put(EXPIRE_DATE, expireDate);
    }

    public String getGroupObjectId() {
        return getString(GROUP_OBJECT_ID);
    }

    private void setGroupObjectId(String objectId) {
        put(GROUP_OBJECT_ID, objectId);
    }
}