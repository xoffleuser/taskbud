package com.inkling.leaderboard;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Installation implements Serializable {

//    private static String ClassName = "ParseInstallation";
//
//    public static void updateUserIdToInstallation(String userId, DataFetchListener<ParseInstallation> listener) {
//
//    }

    public interface MessageSendCallback {
        void onComplete(boolean isSuccess, String msg);
    }

    private static final long serialVersionUID = 6641290013968245775L;

    public static final String FIELD_USER_ID = "UserId";

    static void createInstallation(String userId, final DataFetchListener<ParseInstallation> listener) {
        final ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(FIELD_USER_ID, userId);
        installation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null)
                    listener.onFetchComplete(installation, false);
                else
                    listener.onFetchComplete(null, false);
            }
        });
    }

    // static void addOrUpdateFieldToInstallation(String key, String value) {
    // ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    // installation.put(key, value);
    // installation.saveInBackground();
    // }

    static void sendMessage(String userId, String msg) {
        ParseQuery<ParseInstallation> query = ParseInstallation.getQuery();
        ArrayList<String> list = new ArrayList<String>();
        query.whereEqualTo(FIELD_USER_ID, list);
        // query.whereExists(FIELD_USER_ID);

        ParsePush parsePush = new ParsePush();
        parsePush.setQuery(query);
        parsePush.setMessage(msg);

        parsePush.sendInBackground(new SendCallback() {

            public void done(ParseException e) {

                if (e == null) {
                    Util.debug("PushNotification.Installation.sendMessage(...).new SendCallback() {...}.done() Successfully");
                } else {
                    Util.debug("PushNotification.Installation.sendMessage(...).new SendCallback() {...}.done() Successfully notification error = " + e.getMessage());
                }
            }
        });
    }

    static void sendMessage(String userId, JSONObject json, final MessageSendCallback listener) {
        ParseQuery<ParseInstallation> query = ParseInstallation.getQuery();
        query.whereEqualTo(FIELD_USER_ID, userId);
        // query.whereExists(FIELD_USER_ID);

        ParsePush parsePush = new ParsePush();
        parsePush.setQuery(query);
        parsePush.setData(json);

        parsePush.sendInBackground(new SendCallback() {

            public void done(ParseException e) {
                if (e == null) {
                    Util.debug("PushNotification.Installation.sendMessage(...).new SendCallback() {...}.done() Successfully");
                    if (listener != null)
                        listener.onComplete(true, "successfull");
                } else {
                    Util.debug("PushNotification.Installation.sendMessage(...).new SendCallback() {...}.done() Successfully notification error = " + e.getMessage());
                    if (listener != null)
                        listener.onComplete(false, "error occured");
                }
            }
        });
    }
}