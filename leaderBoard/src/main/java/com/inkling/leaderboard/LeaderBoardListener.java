package com.inkling.leaderboard;

public interface LeaderBoardListener {

	public interface OnGroupFetchListener {
		void onCreate(AppGroup object,boolean isLoaded);
	}

//	public interface OnGroupMemberFetchListener {
//		void onFetchComplete(AppGroupMembers object);
//	}

	public interface OnGroupTaskFetchListener {
		void onFetchComplete(AppGroupTask appTask, boolean isLoaded);
	}
}
