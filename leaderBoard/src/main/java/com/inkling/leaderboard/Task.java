package com.inkling.leaderboard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Task implements Serializable{

    private String objectId;
    private String groupObjectId;
    private String ackCnfID;

    private long ackTime;
    private long cnfTime;
    private boolean isAckd;
    private boolean isCnfd;
    private boolean isDeleted;

    private String subject;
    private String description;
    private String remark;
    private String taskCreaterId;
    private Date createdAt;
    private Date updatedAt;

    private long expireDate;

    private ArrayList<String> unsubscribedBy;
    private ArrayList<String> seenByList;

    /*
    * this constructor is only called before <b>new task</b> is created on parse
    * */
    public Task(String groupObjectId, String taskCreatorsId, String subject, String description, long expireDate, String seenBy) {
        this.groupObjectId = groupObjectId;
        this.taskCreaterId = taskCreatorsId;
        this.subject = subject;
        this.description = description;
        this.expireDate = expireDate;
        seenByList = new ArrayList<String>();
        seenByList.add(seenBy);
    }

    /*
    * this constructor is called while updating or fetching App task from parse
    * */
    public Task(AppGroupTask appTask) {
        objectId = appTask.getObjectId();
        groupObjectId = appTask.getGroupObjectId();
        ackCnfID = appTask.getAckCnfId();
        ackTime = appTask.getAckTime();
        cnfTime = appTask.getCnfTime();
        isAckd = appTask.getIsAckd();
        isCnfd = appTask.getIsCnfd();
        isDeleted = appTask.getIsDeleted();
        unsubscribedBy = (ArrayList<String>) appTask.getUnsubscribedBy();
        subject = appTask.getSubject();
        description = appTask.getDescription();
        remark = getRemark();
        taskCreaterId = appTask.getTaskCreatorId();
        createdAt = appTask.getCreatedAt();
        updatedAt = appTask.getUpdatedAt();
        seenByList = (ArrayList<String>) appTask.getSeenBy();
        expireDate = appTask.getExpireDate();
    }

    public Task() {
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getAckCnfID() {
        return ackCnfID;
    }

    public void setAckCnfID(String ackCnfID) {
        this.ackCnfID = ackCnfID;
    }

    public long getAckTime() {
        return ackTime;
    }

    public void setAckTime(long ackTime) {
        this.ackTime = ackTime;
    }

    public long getCnfTime() {
        return cnfTime;
    }

    public void setCnfTime(long cnfTime) {
        this.cnfTime = cnfTime;
    }

    public boolean isAckd() {
        return isAckd;
    }

    public void setAckd(boolean isAckd) {
        this.isAckd = isAckd;
    }

    public boolean isCnfd() {
        return isCnfd;
    }

    public void setCnfd(boolean isCnfd) {
        this.isCnfd = isCnfd;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTaskCreaterId() {
        return taskCreaterId;
    }

    public void setTaskCreaterId(String taskCreaterId) {
        this.taskCreaterId = taskCreaterId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ArrayList<String> getSeenByList() {
        return seenByList;
    }

    public void setSeenByList(ArrayList<String> seenByList) {
        this.seenByList = seenByList;
    }

    public long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(long expireDate) {
        this.expireDate = expireDate;
    }

    public String getGroupObjectId() {
        return groupObjectId;
    }

    public void setGroupObjectId(String groupObjectId) {
        this.groupObjectId = groupObjectId;
    }

    public ArrayList<String> getUnsubscribedBy() {
        return unsubscribedBy;
    }

    public void setUnsubscribedBy(ArrayList<String> unsubscribedBy) {
        this.unsubscribedBy = unsubscribedBy;
    }
}