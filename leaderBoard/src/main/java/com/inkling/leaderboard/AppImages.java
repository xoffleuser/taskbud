package com.inkling.leaderboard;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.Serializable;

@ParseClassName(value = "AppImages")
public class AppImages extends ParseObject implements Serializable {

    protected static final String ClassName = "AppImages";
    private static final long serialVersionUID = -6241390411224337772L;

    private static final String IMAGE_NAME = "image.png";
    private static final String IMAGE_KEY = "Image";

    public static void saveOrUpdateFile(String imageObjectId, final byte[] data, final ParseImageListener listener) {

        System.out.println("saveOrUpdateFile imageObjectId = " + imageObjectId);
        load(imageObjectId, new ParseImageListener() {
            @Override
            public void onImageSaveOrUpdate(final AppImages appImages) {
                if (appImages == null) {
                    createNew(data, listener);
                } else {
                    ParseFile parseFile = (ParseFile) appImages.get(IMAGE_KEY);
                    appImages.remove(IMAGE_KEY);
                    parseFile = new ParseFile(IMAGE_NAME, data);

                    updateImage(appImages, parseFile, listener);
                }
            }

            @Override
            public void onImageDownload(byte[] bytes) {
            }
        });
    }

    private static void updateImage(final AppImages appImages, ParseFile parseFile, final ParseImageListener listener) {
        appImages.put(IMAGE_KEY, parseFile);

        appImages.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    listener.onImageSaveOrUpdate(appImages);
                } else {
                    Util.debug(e);
                    e.printStackTrace();
                }
            }
        });
    }

    private static void createNew(byte[] data, final ParseImageListener listener) {
        ParseFile parseFile = new ParseFile(IMAGE_NAME, data);
        final AppImages appImages = new AppImages();
        appImages.put(IMAGE_KEY, parseFile);

        appImages.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    listener.onImageSaveOrUpdate(appImages);
                } else {
                    Util.debug(e);
                    e.printStackTrace();
                }
            }
        });
    }

    private static void load(String imageObjectId, final ParseImageListener listener) {
        if (imageObjectId == null) {
            listener.onImageSaveOrUpdate(null);
        }

        ParseQuery<AppImages> query = new ParseQuery<AppImages>(AppImages.ClassName);

        query.getInBackground(imageObjectId, new GetCallback<AppImages>() {
            @Override
            public void done(AppImages appImages, ParseException e) {
                if (e == null && appImages != null) {
                    listener.onImageSaveOrUpdate(appImages);
                } else {
                    listener.onImageSaveOrUpdate(null);
                }
            }
        });
    }

    public static void getImage(String objectId, final ParseImageListener listener) {

        ParseQuery<AppImages> query = new ParseQuery<AppImages>(ClassName);

        query.getInBackground(objectId, new GetCallback<AppImages>() {
            @Override
            public void done(AppImages appImages, ParseException e) {
                if (e == null && appImages != null) {
                    ParseFile parseFile = (ParseFile) appImages.get(IMAGE_KEY);
                    parseFile.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] bytes, ParseException e) {
                            if (e == null && bytes != null && bytes.length > 0) {
                                listener.onImageDownload(bytes);
                            } else {
                                Util.debug(e.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }
}