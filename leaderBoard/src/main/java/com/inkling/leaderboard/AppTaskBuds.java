package com.inkling.leaderboard;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ParseClassName(value = "TaskBuds")
public class AppTaskBuds extends ParseObject implements Serializable {

    public static final int LOAD_BY_USERNAME = 1;
    public static final int LOAD_BY_USER_ID = 2;
    public static final int UPDATE_USERNAME = 3;
    public static final int UPDATE_DISPLAY_NAME = 4;

    protected static final String ClassName = "TaskBuds";
    private static final long serialVersionUID = -6241390411224337772L;

    private static final String USER_ID = "UserId"/* facebookId */;
    private static final String DISPLAY_NAME = "DisplayName";
    private static final String TB_USER_NAME = "TbUserName";
    private static final String IMAGE_ID = "imageId";
    private static final String STATUS = "Status";
    private static final String MEMBER_OF_GROUPS = "MemberOfGroup";

    private static void debug(String msg) {
        LeaderBoard.debug("AppGroupMembers: " + msg);
    }

    /**
     * Load the game user object from parse.<br /> <b> Return null if did n't found else user
     * object.</b>
     * @param userIdOrName
     *         Unique facebook id.
     * @param listener
     */
    static void load(final String userIdOrName, int loadBy, final DataFetchListener<AppTaskBuds> listener) {
        debug("Loading new AppGroupMember");

        ParseQuery<AppTaskBuds> query = new ParseQuery<AppTaskBuds>(ClassName);
        switch (loadBy) {
            case LOAD_BY_USERNAME:
                query.whereEqualTo(TB_USER_NAME, userIdOrName);
                break;
            case LOAD_BY_USER_ID:
                query.whereEqualTo(USER_ID, userIdOrName);
                break;
        }

        query.findInBackground(new FindCallback<AppTaskBuds>() {

            public void done(List<AppTaskBuds> objects, ParseException e) {
                debug("Load= Exception : " + e + "   List of Object: " + objects);
                // Nothing found
                if (e != null || objects == null || objects.size() == 0) {
                    listener.onFetchComplete(null, true);
                }
                // Return found user
                else {
                    listener.onFetchComplete(objects.get(0), true);
                }
            }
        });
    }

    /**
     * Create new user at parse.
     * @param taskBudBean
     * @param listener
     */
    static void createNew(TaskBudBean taskBudBean, final DataFetchListener<AppTaskBuds> listener) {
        debug("Creating new AppGroupMembers");

        final AppTaskBuds taskBud = new AppTaskBuds();
        setData(taskBud, taskBudBean);

        taskBud.saveInBackground(new SaveCallback() {

            public void done(ParseException e) {
                debug("Create New user=  Exception :" + e);
                if (e == null) {
                    listener.onFetchComplete(taskBud, false);
                } else {
                    listener.onFetchComplete(null, false);
                }
            }
        });
    }

    static void loadOrCreate(final TaskBudBean taskBudBean, final DataFetchListener<AppTaskBuds> listener) {
        load(taskBudBean.getMemberId(), LOAD_BY_USER_ID, new DataFetchListener<AppTaskBuds>() {

            public void onFetchComplete(AppTaskBuds object, boolean isLoaded) {
                debug("LoadOrCreate=  NeedNew?: " + (object == null));
                if (object == null) {
                    createNew(taskBudBean, listener);
                } else {
                    listener.onFetchComplete(object, isLoaded);
                }
            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> objects) {

            }
        });
    }

    static void update(final TaskBudBean taskBudBean, final DataFetchListener<AppTaskBuds> listener) {
        load(taskBudBean.getMemberId(), LOAD_BY_USER_ID, new DataFetchListener<AppTaskBuds>() {

            public void onFetchComplete(final AppTaskBuds taskBud, final boolean isLoaded) {

                if (taskBud != null) {

                    setData(taskBud, taskBudBean);

                    taskBud.saveInBackground(new SaveCallback() {

                        public void done(ParseException e) {
                            if(listener==null)
                                return;

                            if (e == null) {
                                listener.onFetchComplete(taskBud, true);
                            } else {
                                listener.onFetchComplete(null, isLoaded);
                            }
                        }
                    });
                } else {
                    listener.onFetchComplete(null, isLoaded);
                }
            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> objects) {

            }
        });
    }

    private static void setData(AppTaskBuds taskBud, TaskBudBean taskBudBean) {
        taskBud.setUserId(taskBudBean.getMemberId());
        taskBud.setStatus(taskBudBean.getStatus());
        taskBud.setDisplayName(taskBudBean.getDisplayName());
        taskBud.setImageId(taskBudBean.getImageId());
        taskBud.setTbUserName(taskBudBean.getUserName());
    }


    public static void updateUserName(String userId, final String usersNewName, final DataFetchListener<AppTaskBuds> listener) {
        load(userId, LOAD_BY_USER_ID, new DataFetchListener<AppTaskBuds>() {

            public void onFetchComplete(final AppTaskBuds taskBud, final boolean isLoaded) {

                if (taskBud != null) {
                    taskBud.setTbUserName(usersNewName);

                    taskBud.saveInBackground(new SaveCallback() {

                        public void done(ParseException e) {
                            if (e == null) {
                                listener.onFetchComplete(taskBud, true);
                            } else {
                                listener.onFetchComplete(null, isLoaded);
                            }
                        }
                    });
                } else {
                    listener.onFetchComplete(null, isLoaded);
                }
            }

            @Override
            public void onListFetchComplete(List<AppTaskBuds> objects) {

            }
        });
    }

    static void searchTaskBuds(String text, final DataFetchListener<AppTaskBuds> listener) {
        ParseQuery<AppTaskBuds> query = new ParseQuery<AppTaskBuds>(ClassName);
        query.whereMatches(TB_USER_NAME, text);

        query.findInBackground(new FindCallback<AppTaskBuds>() {

            public void done(List<AppTaskBuds> objects, ParseException e) {
                debug("Load= Exception : " + e + "   List of Object: " + objects);
                if (e != null || objects == null || objects.size() == 0) {
                    listener.onListFetchComplete(null);
                } else {
                    listener.onListFetchComplete(objects);
                }
            }
        });
    }

    /**
     * @return unique user's facebook id.
     */
    public String getUserId() {
        return getString(USER_ID);
    }

    /**
     * Set User id of GameUser but if once set can't be changed.
     * @param userId
     */
    private void setUserId(String userId) {
        if (getUserId() != null || userId == null) {
            debug("Can't Change Id  once set.");
            return;
        }
        put(USER_ID, userId);
    }

    public String getImageId() {
        return getString(IMAGE_ID);
    }

    private void setImageId(String imageId) {
        if (imageId != null) {
            put(IMAGE_ID, imageId);
        }
    }

    public String getStatus() {
        return getString(STATUS);
    }

    private void setStatus(String status) {
        if (status != null) {
            put(STATUS, status);
        }
    }

    /**
     * @return unique user's facebook id.
     */
    public String getDisplayName() {
        if(getString(DISPLAY_NAME)==null)
            return getString("FbUserName");
        return getString(DISPLAY_NAME);
    }

    private void setDisplayName(String userName) {
        if (userName != null) {
            put(DISPLAY_NAME, userName);
        }
    }

    public String getTbUserName() {
        return getString(TB_USER_NAME);
    }

    private void setTbUserName(String tbUserName) {
        if (tbUserName != null) {
            put(TB_USER_NAME, tbUserName);
        }
    }

    public List<String> getMemberOfGroups() {
        return getList(MEMBER_OF_GROUPS);
    }

    private void setMemberOfGroups(ArrayList<String> list) {
        put(MEMBER_OF_GROUPS, list);
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("[").append(getUserId()).append(",").append(getTbUserName()).append("]");
        return buffer.toString();
    }
}