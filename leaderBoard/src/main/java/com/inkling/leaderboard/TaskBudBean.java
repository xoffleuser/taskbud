package com.inkling.leaderboard;

import java.io.Serializable;

/**
 * Created by ravi on 6/7/15.
 */
public class TaskBudBean implements Serializable {

    protected String memberId;
    protected String userName;
    protected String displayName;
    protected String imageId;
    protected String status;
//    private boolean isAdmin;
//    private boolean isMe;

    public TaskBudBean(String memberId, String displayName) {
        this(memberId, displayName, null, null);
    }

    public TaskBudBean(String memberId, String displayName, String status) {
        this(memberId, displayName, status, null);
    }

    public TaskBudBean(String memberId, String displayName, String status, String imageId) {
        this.memberId = memberId;
        this.displayName = displayName;
        this.status = status;
        this.imageId = imageId;
    }

    public TaskBudBean(TaskBudBean taskBudBean) {
        this.memberId = taskBudBean.getMemberId();
        this.displayName = taskBudBean.getDisplayName();
        this.status = taskBudBean.getStatus();
        this.imageId = taskBudBean.getImageId();
        this.userName = taskBudBean.getUserName();
    }

    public TaskBudBean(AppTaskBuds appTaskBud) {
        this.memberId = appTaskBud.getUserId();
        this.displayName = appTaskBud.getDisplayName();
        this.status = appTaskBud.getStatus();
        this.imageId = appTaskBud.getImageId();
        this.userName = appTaskBud.getTbUserName();
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int hashCode() {
        return getMemberId().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o != null && getMemberId().equals(((TaskBudBean) o).getMemberId());
    }
}