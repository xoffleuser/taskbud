package com.inkling.leaderboard;

public class Util {

	private static final String TAG = "<-------DebugTag-------->";
	private static boolean DEBUG;

	static void setDebug(boolean debug) {
		DEBUG = debug;
	}

	static void debug(Object object) {
		if (DEBUG)
			System.out.println(TAG + object);
	}

	public static boolean isNull(String value) {
		return value == null || value.trim().length() == 0;
	}

}
